<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Reports.
 *
 * @author  The scaffold-interface created at 2017-02-16 10:58:56am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Reports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('reports',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->dateTime('date')->nullable();
        
        $table->longText('description');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('project_id')->unsigned()->nullable();
        $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
