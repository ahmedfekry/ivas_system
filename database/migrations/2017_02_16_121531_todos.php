<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Todos.
 *
 * @author  The scaffold-interface created at 2017-02-16 12:15:31pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Todos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('todos',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('title');
        
        $table->double('regular_hours',8,2);
        
        $table->integer('priority');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('user_id')->unsigned()->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        
        $table->integer('project_id')->unsigned()->nullable();
        $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('todos');
    }
}
