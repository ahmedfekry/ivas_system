<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Miniprojects.
 *
 * @author  The scaffold-interface created at 2017-02-16 10:27:29am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Miniprojects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('miniprojects',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('regular_days');
        
        $table->String('name');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('departments_projects_id')->unsigned()->nullable();
        $table->foreign('departments_projects_id')->references('id')->on('departments_projects')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('miniprojects');
    }
}
