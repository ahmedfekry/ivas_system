<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Taskdocuments.
 *
 * @author  The scaffold-interface created at 2017-02-16 12:13:34pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Taskdocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('taskdocuments',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->String('url');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('task_id')->unsigned()->nullable();
        $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('taskdocuments');
    }
}
