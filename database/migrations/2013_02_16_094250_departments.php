<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Departments.
 *
 * @author  The scaffold-interface created at 2017-02-16 09:42:51am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Departments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('departments',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->longText('description');
        
        $table->String('sub_department');
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('departments');
    }
}
