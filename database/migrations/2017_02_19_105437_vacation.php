<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vacations.
 *
 * @author  The scaffold-interface created at 2017-02-19 10:54:37am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vacation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('vacation',function (Blueprint $table){

        $table->increments('id');
        
        $table->date('date');
        
        $table->longText('reason');
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('vacation');
    }
}
