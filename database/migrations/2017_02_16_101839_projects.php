<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Projects.
 *
 * @author  The scaffold-interface created at 2017-02-16 10:18:39am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Projects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('projects',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->longText('description');
        
        $table->dateTime('contract_date');
        
        $table->dateTime('delivary_date');
        
        $table->double('contract_period')->nullable();
        
        $table->integer('status');
        
        $table->dateTime('start_date')->nullable();
        
        $table->integer('priority');
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
