<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEndDateInTasksToNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function($table) {
            $table->dateTime('end_date')->nullable()->change();
            $table->dateTime('actual_start')->nullable()->change();
            $table->dateTime('actual_end')->nullable()->change();
            $table->string('finish_text')->nullable()->change();
            $table->string('finish_file_path')->nullable()->change();
            $table->string('finish_url')->nullable()->change();
            $table->integer('progress_pause')->nullable()->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
