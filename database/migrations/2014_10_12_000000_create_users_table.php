<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('vacation')->nullable();
            $table->integer('emergency')->nullable();
            $table->integer('marriage')->nullable();
            $table->integer('military_service')->nullable();
            $table->integer('meternity')->nullable();
            $table->string('title');
            $table->string('gender');
            $table->string('address');
            $table->date('birthday')->nullable();
            $table->date('hire_date')->nullable();
            $table->text('about');
            $table->float('salary', 15, 8)->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('role');
            $table->string('phone_number');
            $table->string('category')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->integer('parent_id')->unsigned()->nullable(); 
            $table->foreign('parent_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
