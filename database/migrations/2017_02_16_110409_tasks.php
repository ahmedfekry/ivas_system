<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Tasks.
 *
 * @author  The scaffold-interface created at 2017-02-16 11:04:09am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('tasks',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->longText('description');
        
        $table->dateTime('start_date');
        
        $table->integer('regular_hours');
        
        $table->integer('periority');
        
        $table->dateTime('end_date');
        
        $table->dateTime('actual_start');
        
        $table->dateTime('actual_end');
        
        $table->integer('status')->default(0);
        
        $table->longText('finish_text');
        
        $table->String('finish_file_path');
        
        $table->String('finish_url');
        
        $table->integer('progress_pause');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('miniproject_id')->unsigned()->nullable();
        $table->foreign('miniproject_id')->references('id')->on('miniprojects')->onDelete('cascade');
        
        $table->integer('user_id')->unsigned()->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
