<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Documents.
 *
 * @author  The scaffold-interface created at 2017-02-16 10:56:35am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Documents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('documents',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->longText('description');
        
        $table->dateTime('date')->nullable();
        
        $table->String('url');
        
        $table->String('type');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('project_id')->unsigned()->nullable();
        $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
