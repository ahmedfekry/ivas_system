<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Pauses.
 *
 * @author  The scaffold-interface created at 2017-02-16 12:09:04pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Pauses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('pauses',function (Blueprint $table){

        $table->increments('id');
        
        $table->dateTime('start');
        
        $table->dateTime('end');
        
        $table->integer('number_of_hours')->nullable();
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('task_id')->unsigned()->nullable();
        $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('pauses');
    }
}
