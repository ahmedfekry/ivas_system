-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2016 at 09:24 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ivas`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sub_department` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `description`, `sub_department`) VALUES
(1, 'Digital', 'Digital', 'Android/IOS/Web'),
(2, 'IT', 'IT', 'HelpDesk/Network'),
(3, 'HR', 'HR', 'HR');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `description`, `date`, `url`, `type`, `project_id`, `created_at`, `updated_at`) VALUES
(1, 'test project', 'this is a document for project: test project', '2016-12-18 09:56:08', '1482054968_file.docx', 'document', 1, '2016-12-18 07:56:08', '2016-12-18 07:56:08'),
(2, 'Total Project', 'this is a document for project: Total Project', '2016-12-21 12:26:53', '1482323213_1482306472_1480948076_ivas.sql', 'document', 4, '2016-12-21 10:26:53', '2016-12-21 10:26:53');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(11) NOT NULL,
  `timesheet_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` double NOT NULL,
  `end_time` double NOT NULL,
  `type` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_11_21_145722_create_department_table', 1),
(2, '2016_11_21_145722_create_document_table', 1),
(3, '2016_11_21_145722_create_leaves_table', 1),
(4, '2016_11_21_145722_create_overtime_table', 1),
(5, '2016_11_21_145722_create_project_table', 1),
(6, '2016_11_21_145722_create_report_table', 1),
(7, '2016_11_21_145722_create_subproject_table', 1),
(8, '2016_11_21_145722_create_task_table', 1),
(9, '2016_11_21_145722_create_timesegment_table', 1),
(10, '2016_11_21_145722_create_timesheet_table', 1),
(11, '2016_11_21_145722_create_userproject_table', 1),
(12, '2016_11_21_145722_create_users_table', 1),
(13, '2016_11_21_145722_create_usertask_table', 1),
(14, '2016_11_21_145724_add_foreign_keys_to_document_table', 1),
(15, '2016_11_21_145724_add_foreign_keys_to_leaves_table', 1),
(16, '2016_11_21_145724_add_foreign_keys_to_overtime_table', 1),
(17, '2016_11_21_145724_add_foreign_keys_to_report_table', 1),
(18, '2016_11_21_145724_add_foreign_keys_to_subproject_table', 1),
(19, '2016_11_21_145724_add_foreign_keys_to_task_table', 1),
(20, '2016_11_21_145724_add_foreign_keys_to_timesegment_table', 1),
(21, '2016_11_21_145724_add_foreign_keys_to_timesheet_table', 1),
(22, '2016_11_21_145724_add_foreign_keys_to_userproject_table', 1),
(23, '2016_11_21_145724_add_foreign_keys_to_users_table', 1),
(24, '2016_11_21_145724_add_foreign_keys_to_usertask_table', 1),
(25, '2016_11_21_145851_add_time_stamp_to_users', 1),
(26, '2016_11_21_150343_add_role_to_users', 1),
(27, '2016_11_22_115547_remove_columns_from_users', 1),
(28, '2016_11_22_143304_modify_profile_picture_in_user', 1),
(29, '2016_11_22_143534_modify_profile_picture_in_users', 1),
(30, '2016_11_22_145610_modify_salary_in_users', 1),
(31, '2016_11_22_160653_remove_file_path_from_projects', 1),
(32, '2016_11_22_161031_add_time_stamp_to_projects', 1),
(33, '2016_11_22_161337_remove_total_hours_from_projects', 1),
(34, '2016_11_22_180845_remove_start_date_projects', 1),
(35, '2016_11_24_074043_change_status_to_type_in_project', 1),
(36, '2016_11_24_075106_change_type_in_project', 1),
(37, '2016_11_24_124629_add_time_stamp_to_document', 1),
(38, '2016_11_24_134202_change_contract_period_in_project', 1),
(39, '2016_11_28_094116_create_table_mini_project', 1),
(40, '2016_11_28_095326_add_mini_project_id_to_tasks', 1),
(41, '2016_11_28_130518_add_time_stamp_to_miniproject', 1),
(42, '2016_11_28_150602_add_name_to_miniproject', 1),
(43, '2016_11_28_171125_drop_departments_projects_id_from_task', 1),
(44, '2016_11_28_172732_drop_end_date_and_approved_from_task', 1),
(45, '2016_11_28_180207_add_time_stamp_task', 1),
(46, '2016_12_05_122716_add_sub_departments_to_department', 1),
(47, '2016_12_05_123021_add_category_to_employee', 1),
(48, '2016_12_05_123225_set_category_to_nullable', 1),
(49, '2016_12_05_135843_remove_type_from_projects', 1),
(50, '2016_12_06_084802_create_task_document_table', 1),
(51, '2016_12_06_085125_add_forign_key_to_task_document_table', 1),
(52, '2016_12_06_094041_add_time_stamp_task_document_table', 1),
(53, '2016_12_06_094648_remove_file_path_from_task_table', 1),
(54, '2016_12_12_121008_add_status_to_project', 1),
(55, '2016_12_15_090708_todo_table', 1),
(56, '2016_12_15_091341_add_forign_key_user_id_to_todo', 1),
(57, '2016_12_15_091529_add_forign_key_project_id_to_todo', 1),
(58, '2016_12_15_091739_remove_last_name_from_todo', 1),
(59, '2016_12_15_091859_add_priority_to_todo', 1),
(60, '2016_12_18_104106_add_start_date_to_project', 2),
(62, '2016_12_19_141956_add_end_date_to_task', 3),
(63, '2016_12_20_092308_remove_status_from_task', 4),
(65, '2016_12_20_092938_add_actual_start_and_end_and_status_to_usertask', 5),
(66, '2016_12_20_093250_remove_accurate_hours_from_usertask', 6),
(67, '2016_12_20_100558_add_default_value_to_status_is_usertask', 7),
(68, '2016_12_20_154727_add_finish_text_file_to_usertask', 8);

-- --------------------------------------------------------

--
-- Table structure for table `miniproject`
--

CREATE TABLE `miniproject` (
  `id` int(11) NOT NULL,
  `regular_days` int(11) NOT NULL,
  `departments_projects_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `miniproject`
--

INSERT INTO `miniproject` (`id`, `regular_days`, `departments_projects_id`, `created_at`, `updated_at`, `name`) VALUES
(7, 3, 1, '2016-12-19 10:21:33', '2016-12-19 10:21:33', 'Android'),
(8, 4, 1, '2016-12-19 10:21:33', '2016-12-19 10:21:33', 'IOS'),
(9, 2, 2, '2016-12-20 06:42:07', '2016-12-20 06:42:07', 'Android'),
(10, 3, 3, '2016-12-20 06:42:36', '2016-12-20 06:42:36', 'IOS');

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `start_time` double NOT NULL,
  `end_time` double NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `time_segment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contract_date` date NOT NULL,
  `delivary_date` date NOT NULL,
  `contract_period` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `start_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `description`, `contract_date`, `delivary_date`, `contract_period`, `created_at`, `updated_at`, `status`, `start_date`) VALUES
(1, 'test project', 'information', '2013-05-07', '2017-07-13', 4, '2016-12-18 07:56:08', '2016-12-19 10:21:33', 1, '2016-12-19'),
(2, 'new project', 'sdfsdf', '2016-08-24', '2017-11-07', 2, '2016-12-20 06:42:01', '2016-12-20 06:42:07', 1, '2016-12-20'),
(3, 'newennnjknj', 'sdafasdf', '2013-05-07', '2017-09-28', 3, '2016-12-20 06:42:29', '2016-12-20 06:42:36', 1, '2016-12-20'),
(4, 'Total Project', 'ifrom', '2016-12-21', '2017-01-19', 4, '2016-12-21 10:26:53', '2016-12-21 10:28:29', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subproject`
--

CREATE TABLE `subproject` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subproject`
--

INSERT INTO `subproject` (`id`, `project_id`, `department_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `regular_hours` int(11) DEFAULT NULL,
  `periority` int(11) NOT NULL,
  `mini_project_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `name`, `description`, `start_date`, `regular_hours`, `periority`, `mini_project_id`, `created_at`, `updated_at`, `end_date`) VALUES
(1, 'New Task', 'information', '2016-12-20', 4, 1, 7, '2016-12-19 10:37:58', '2016-12-19 12:02:01', '2016-12-22 00:00:00'),
(2, 'asdfsdafdsasd', 'kj', '2016-12-20', 54, 2, 8, '2016-12-19 12:03:41', '2016-12-19 12:04:16', '2016-12-21 00:00:00'),
(3, 'fassdaasddaasd', 'asdasd', '2016-12-21', 4, 1, 8, '2016-12-19 12:30:32', '2016-12-19 13:05:31', '2016-12-21 04:00:00'),
(4, 'asdasdasdasd', 'asd', '2016-12-23', 415, 1, 8, '2016-12-19 12:31:50', '2016-12-19 13:05:34', '2017-01-09 07:00:00'),
(5, 'heeeeeeeey', 'dsaf', '2016-12-22', 21, 1, 8, '2016-12-20 06:15:27', '2016-12-20 06:15:27', '2016-12-22 21:00:00'),
(6, 'titletielellt', 'sdfasd', '2016-12-22', 4, 1, 9, '2016-12-20 07:59:56', '2016-12-20 07:59:56', '2016-12-22 04:00:00'),
(7, 'titletielellt', 'sdfasd', '2016-12-22', 4, 1, 9, '2016-12-20 08:01:16', '2016-12-20 08:01:16', '2016-12-22 04:00:00'),
(8, 'titletielellt', 'sdfasd', '2016-12-22', 4, 1, 9, '2016-12-20 08:02:33', '2016-12-20 08:02:33', '2016-12-22 04:00:00'),
(9, 'titletielellt', 'sdfasd', '2016-12-19', 4, 1, 9, '2016-12-20 08:11:27', '2016-12-20 08:11:27', '2016-12-22 04:00:00'),
(10, 'nennnennnen', 'sadfasdf', '2016-12-19', 2, 1, 8, '2016-12-20 11:56:36', '2016-12-20 11:56:36', '2016-12-21 02:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `task_document`
--

CREATE TABLE `task_document` (
  `id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_document`
--

INSERT INTO `task_document` (`id`, `url`, `task_id`, `created_at`, `updated_at`) VALUES
(1, '1482151078_.gitignore', 1, '2016-12-19 10:37:58', '2016-12-19 10:37:58'),
(2, '1482221727_.gitignore', 5, '2016-12-20 06:15:27', '2016-12-20 06:15:27');

-- --------------------------------------------------------

--
-- Table structure for table `timesegment`
--

CREATE TABLE `timesegment` (
  `id` int(11) NOT NULL,
  `timesheet_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` double NOT NULL,
  `end_time` double NOT NULL,
  `total_hours` double NOT NULL,
  `deduction_hours` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timesheet`
--

CREATE TABLE `timesheet` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `number_of_hours` int(11) NOT NULL,
  `total_salary` double NOT NULL,
  `delivered` tinyint(1) NOT NULL,
  `accurate_num_of_hours` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE `todo` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regular_hours` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`id`, `user_id`, `project_id`, `title`, `regular_hours`, `created_at`, `updated_at`, `priority`) VALUES
(1, 2, 1, 'managerTodo', 1.90, '2016-12-19 10:24:06', '2016-12-19 10:25:30', 3),
(2, 4, 1, 'asd', 0.30, '2016-12-19 13:04:12', '2016-12-19 13:05:49', 3),
(3, 2, 1, 'asd', 0.30, '2016-12-19 13:06:22', '2016-12-19 13:06:27', 3),
(4, 2, 3, 'tototo', 0.80, '2016-12-21 07:11:56', '2016-12-21 07:11:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userproject`
--

CREATE TABLE `userproject` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userproject`
--

INSERT INTO `userproject` (`id`, `project_id`, `user_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 4),
(4, 1, 5),
(5, 1, 6),
(6, 4, 2),
(7, 4, 3),
(8, 4, 4),
(9, 4, 5),
(10, 4, 6),
(11, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vacation` int(11) NOT NULL,
  `emergency` int(11) NOT NULL,
  `marriage` int(11) NOT NULL,
  `military_service` int(11) NOT NULL,
  `meternity` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `salary` double DEFAULT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `department_id`, `phone_number`, `email`, `first_name`, `last_name`, `password`, `vacation`, `emergency`, `marriage`, `military_service`, `meternity`, `title`, `gender`, `address`, `birthday`, `hire_date`, `about`, `salary`, `profile_picture`, `remember_token`, `created_at`, `updated_at`, `role`, `category`) VALUES
(1, 3, '0110xxxxxxxx', 'CEO@ivas.com.eg', 'CEO', 'Ivas', '$2y$10$ljha9Y1MOQbQ4Po/LhGaGuP4gbwx5crUAq.qoPp0fbySBvzgzhtVa', 21, 10, 10, 10, 10, 'General Manager', 'Male', '11 abo el karamat @ ivas', '1970-09-18', '2014-12-18', 'about the general manager', 1000000, NULL, 'Kb8lYuN5ktsYBg2Esz8ChBLgMhHQv8UsuuhbFlo1fYCy7uBuY9xwgB4yBkjY', NULL, '2016-12-22 06:11:39', 'admin', 'general'),
(2, 1, '0110xxxxxxxxx', 'ahmed.taalab@ivas.com.eg', 'Ahmed', 'Taalab', '$2y$10$8559qbbz4bzh8M8FYL1RuO6VnxE4xAr/rZvgy/kXGA3Mrgc3X8RLG', 15, 6, 7, 14, 0, 'Digital Department Manager', 'male', 'ivas location address', '1980-02-01', '2016-02-01', 'Taalab is the manager of the digital department ', 10000, NULL, 'Ztf3ouad3D1sQpHuOJogshtVlt4zmpqHr77Tg6osiKEBdssZReWboX6q8iyB', '2016-12-18 07:08:29', '2016-12-21 13:22:25', 'manager', 'general'),
(3, 1, '0120xxxxxxxxx', 'ahmed.fekry@ivas.com.eg', 'Ahmed', 'Fekry', '$2y$10$pnZhnC7ZSfXutm.ELfJQGuXERmSrU7Vd4x.fmzx1NoJpD17yb/NGO', 15, 6, 7, 14, 0, 'PHP Developer', 'male', 'ivas location address', '1994-09-07', '2016-10-01', 'Ahmed is a php developer in the Digital department at ivas ', 10000, NULL, NULL, '2016-12-18 07:12:05', '2016-12-18 07:12:05', 'employee', 'Web'),
(4, 1, '012101xxxxxxxx', 'ahmed.3aal@ivas.com.eg', 'Ahmed', '3aal', '$2y$10$6dUgLXm66nARDaAbi.BrVeNtA9W/oXgJrCXwOCwD4GL6FxFHVY/R2', 15, 6, 7, 14, 0, 'Android Developer', 'male', 'ivas location address', '1990-02-01', '2015-12-01', 'Ahmed is an Android Developer @ ivas company', 10000, NULL, 'CCxsEz9JvyQE35w7weHHUfwa30CXyOLoNQpy4QhEws4cEWzUgMdKoXE0Xtcn', '2016-12-18 07:22:21', '2016-12-21 13:19:43', 'employee', 'Android'),
(5, 1, '0130xxxxxxxxx', 'sara.khateer@ivas.com.eg', 'Sara', 'Khater', '$2y$10$C0nScP9aBfZoM1OmqAzKrupE5Qe30UeLvS5Q.MscTyXvogMxaDkyW', 15, 6, 7, 14, 0, 'IOS Developer', 'male', 'ivas location address', '1990-02-01', '2012-02-01', 'sara is an ios developer at ivas company', 10000, NULL, NULL, '2016-12-18 07:27:38', '2016-12-18 07:27:38', 'employee', 'IOS'),
(6, 1, '0140xxxxxxxx', 'abdelrahman@ivas.com.eg', 'Abdelrahman', 'haridy', '$2y$10$OmG0m36E3eeSX9aXTn1wiO80KgRRXskgq7MNeDMYW0I5Km2./.mbu', 15, 6, 7, 14, 0, 'Web Designer', 'male', 'ivas location address', '1990-02-01', '2016-02-02', 'abdelrahman is a web designer at ivas company', 10000, NULL, NULL, '2016-12-18 07:29:10', '2016-12-18 07:29:10', 'employee', 'Web'),
(7, 2, '0158xxxxxxx', 'it.manager@ivas.com.eg', 'Test', 'It', '$2y$10$5l5mZMdsLZgcWeicPnmFTOmxROXjo8rNcFoMJr1D0LwSrQDjUztJ.', 15, 6, 7, 14, 0, 'IT Manager', 'male', 'address', '1983-12-29', '2016-12-20', 'Manager', 12341546, NULL, 'pkSFEQgoKmnaqsDSMbAN3JIYDnL4xRA34jpoQOJzZd1nAg6TXsk79A36qdj6', '2016-12-21 10:26:17', '2016-12-21 10:38:22', 'manager', 'general');

-- --------------------------------------------------------

--
-- Table structure for table `usertask`
--

CREATE TABLE `usertask` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `actual_start` datetime DEFAULT NULL,
  `actual_end` datetime DEFAULT NULL,
  `finish_text` text COLLATE utf8_unicode_ci,
  `finish_file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `finish_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usertask`
--

INSERT INTO `usertask` (`id`, `user_id`, `task_id`, `status`, `actual_start`, `actual_end`, `finish_text`, `finish_file_path`, `finish_url`) VALUES
(1, 4, 1, 1, '2016-12-21 08:43:48', NULL, NULL, NULL, NULL),
(2, 4, 2, 2, '2016-12-21 07:49:28', '2016-12-21 08:35:00', 'asdasd', NULL, 'asd'),
(3, 4, 3, 0, NULL, NULL, NULL, NULL, NULL),
(4, 4, 4, 0, NULL, NULL, NULL, NULL, NULL),
(5, 4, 5, 0, NULL, NULL, NULL, NULL, NULL),
(6, 4, 9, 2, '2016-12-20 00:00:00', '2016-12-21 07:47:52', 'Ahmed', '1482306472_1480948076_ivas.sql', 'www.facebook.com'),
(8, 5, 10, 0, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_document__project_id` (`project_id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_leaves__timesheet_id` (`timesheet_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `miniproject`
--
ALTER TABLE `miniproject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_miniproject__departments_projects_id` (`departments_projects_id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_overtime__time_segment_id` (`time_segment_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_report__project_id` (`project_id`);

--
-- Indexes for table `subproject`
--
ALTER TABLE `subproject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_subproject__project_id` (`project_id`),
  ADD KEY `idx_subproject__department_id` (`department_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_task__mini_project_id` (`mini_project_id`);

--
-- Indexes for table `task_document`
--
ALTER TABLE `task_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_task_document__task_id` (`task_id`);

--
-- Indexes for table `timesegment`
--
ALTER TABLE `timesegment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_timesegment__timesheet_id` (`timesheet_id`);

--
-- Indexes for table `timesheet`
--
ALTER TABLE `timesheet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_timesheet__user_id` (`user_id`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_todo__user_id` (`user_id`),
  ADD KEY `idx_todo__project_id` (`project_id`);

--
-- Indexes for table `userproject`
--
ALTER TABLE `userproject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_userproject__project_id` (`project_id`),
  ADD KEY `idx_userproject__user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `idx_user__department_id` (`department_id`);

--
-- Indexes for table `usertask`
--
ALTER TABLE `usertask`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_usertask__user_id` (`user_id`),
  ADD KEY `idx_usertask__task_id` (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `miniproject`
--
ALTER TABLE `miniproject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subproject`
--
ALTER TABLE `subproject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `task_document`
--
ALTER TABLE `task_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `timesegment`
--
ALTER TABLE `timesegment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `timesheet`
--
ALTER TABLE `timesheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `userproject`
--
ALTER TABLE `userproject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `usertask`
--
ALTER TABLE `usertask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `fk_document__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `leaves`
--
ALTER TABLE `leaves`
  ADD CONSTRAINT `fk_leaves__timesheet_id` FOREIGN KEY (`timesheet_id`) REFERENCES `timesheet` (`id`);

--
-- Constraints for table `miniproject`
--
ALTER TABLE `miniproject`
  ADD CONSTRAINT `miniproject_departments_projects_id_foreign` FOREIGN KEY (`departments_projects_id`) REFERENCES `subproject` (`id`);

--
-- Constraints for table `overtime`
--
ALTER TABLE `overtime`
  ADD CONSTRAINT `fk_overtime__time_segment_id` FOREIGN KEY (`time_segment_id`) REFERENCES `timesegment` (`id`);

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `fk_report__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `subproject`
--
ALTER TABLE `subproject`
  ADD CONSTRAINT `fk_subproject__department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `fk_subproject__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_mini_project_id_foreign` FOREIGN KEY (`mini_project_id`) REFERENCES `miniproject` (`id`);

--
-- Constraints for table `task_document`
--
ALTER TABLE `task_document`
  ADD CONSTRAINT `fk_task_document__task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);

--
-- Constraints for table `timesegment`
--
ALTER TABLE `timesegment`
  ADD CONSTRAINT `fk_timesegment__timesheet_id` FOREIGN KEY (`timesheet_id`) REFERENCES `timesheet` (`id`);

--
-- Constraints for table `timesheet`
--
ALTER TABLE `timesheet`
  ADD CONSTRAINT `fk_timesheet__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `todo`
--
ALTER TABLE `todo`
  ADD CONSTRAINT `fk_todo__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `fk_todo__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `userproject`
--
ALTER TABLE `userproject`
  ADD CONSTRAINT `fk_userproject__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `fk_userproject__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_user__department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);

--
-- Constraints for table `usertask`
--
ALTER TABLE `usertask`
  ADD CONSTRAINT `fk_usertask__task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `fk_usertask__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
