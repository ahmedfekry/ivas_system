<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    //
    protected $table = "todos";
    public function user()
    {
    	# code...
    	return $this->belongsTo('App\User');
    }

    public function project()
    {
    	# code...
    	return $this->belongsTo('App\Project');
    }
}
