<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pause extends Model
{
	protected $table = 'pauses';
	public function task()
	{
		return $this->belongsTo('App\Task');
	}

	public function project()
	{
		return $this->belongsTo('App\Project');
	}
}
