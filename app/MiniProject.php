<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiniProject extends Model
{
    //
    protected $table = 'miniprojects';

    public function DepartmentProject()
    {
        return $this->belongsTo('App\DepartmentProject','departments_projects_id');
    }

    public function tasks()
    {
    	# code...
    	return $this->hasMany('App\Task','miniproject_id');
    }
}
