<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentProject extends Model
{
    protected $table = 'departments_projects';

    public function miniprojects()
    {
        return $this->hasMany('App\MiniProject','departments_projects_id');
    }

    public function project()
    {
    	# code...
    	return $this->belongsTo('App\Project');
    }

    public function tasks()
    {
        $this->hasManyThrough('App\Task','App\MiniProject');
    }
}
