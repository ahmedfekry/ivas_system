<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user && $user->role == 'admin') {
            // return $next($request);
            return redirect('admin_dashboard');

        }elseif ($user && $user->role == 'employee') {
            # code...
            return redirect('employee_dashboard'); 
        }elseif ($user && $user->role == 'manager') {
            # code...
            return redirect('manager_dashboard');             
        }

        return redirect('login'); 

        // abort(404,'You shall not pass !!');

    }
}
