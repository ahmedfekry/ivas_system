<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && $user->role == 'admin') {
            # code...
            return $next($request);
            // return ;
        }
        return redirect()->guest('login');
        // abort('404', 'You shall not pass !!');
    }
}
