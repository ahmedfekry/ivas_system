<?php

namespace App\Http\Controllers;

use App\User;
use App\Department;
use App\Project;
use Carbon\Carbon;
use App\Vacation;
use Auth;
use Hash;
use File;
use Validator;


use Illuminate\Http\Request;

class VacationController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }


    public function create()
    {
    	return view('admin.create_vacation');
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'date' => 'required|date|unique:vacation',
            'reason' => 'required',

        ]);

        if ($validator->fails())
        {   
            return redirect('/admin_dashboard/vacation/new')->withErrors($validator)->withInput();
        }

    	$vacation = new Vacation;
    	$date = new Carbon($request->date);
    	$vacation->date = $date->toDateString();
    	$vacation->reason = $request->reason;
    	$vacation->save();
    	// return $vacation;
        $request->session()->flash('success', 'Vacation created successfull');
    	return redirect('/admin_dashboard/vacations');
    }

    public function index()
    {
    	$vacations = Vacation::all();

    	return view('admin.all_vacations',compact('vacations'));
    }

}
