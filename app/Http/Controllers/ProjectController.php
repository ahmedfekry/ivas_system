<?php

namespace App\Http\Controllers;

use App\Project;
use App\Department;
use Carbon\Carbon;
use App\Input;
use App\Document;
use Auth;
use Validator;
use App\User;
use App\DepartmentProject;
use App\MiniProject;
use App\Task;
use App\TaskDocument;
use App\Pause;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProjectController extends Controller
{
    //
    public function create()
    {
        # code...

            $departments = Department::all();
        
        if (Auth::user()->role == 'admin') {

            return view('admin.new_project', compact('departments'));
            # code...
        } elseif(Auth::user()->role == 'manager') {
            # code...  
            $departments = Auth::user()->department()->get();   
            // return $departments ;
            return view('manager.new_project', compact('departments'));

        }
        
    }


    public function store(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
       
            'name' => 'required|max:255|min:2',
            'contract_date' => 'required|date|before:today',
            'delivery_date' => 'required|date|after:today',
            'contract_period' => 'required|numeric',
            'description' => 'required',
            'priority' => 'required|numeric',

        ]);

        if ($validator->fails())
        {   
            if (Auth::user()->role == 'manager') {
                return redirect('/manager_dashboard/project/new')->withErrors($validator)->withInput();
            }elseif (Auth::user()->role == 'admin') {
                return redirect('/admin_dashboard/project/new')->withErrors($validator)->withInput();
            }
        }
    

        $Project = new Project;
        $Project->name = $request->name;
        $Project->contract_date = new Carbon($request->contract_date);
        $Project->delivary_date = new Carbon($request->delivery_date);
        $Project->contract_period = $request->contract_period;
        $Project->description = $request->description;
        $Project->status = 0;
        $Project->priority = $request->priority;
        
        $Project->save();

        if ($request->hasFile('projectDocuments')) {
            foreach ($request->file('projectDocuments') as $file) {

                $path = $file->move('uploads/project/', time().'_'.$file->getClientOriginalName());
                $Document = new Document;
                $Document->name = $request->name;
                $Document->description = 'this is a document for project: '.$request->name;
                $Document->date =  new Carbon("Africa/Cairo");
                $Document->url = time().'_'.$file->getClientOriginalName();
                $Document->type = 'document';
                $Project->documents()->save($Document);

            }
        }


       
        if (Auth::user()->role == 'admin') {

            $Project->departments()->sync($request->department);
            $request->session()->flash('success', 'Project created successfull');
            return redirect('/admin_dashboard/projects');

        } elseif (Auth::user()->role == 'manager') {

            $department = Auth::user()->department;   
            $Project->departments()->sync([  $department->id ]);
            $request->session()->flash('success', 'Project created successfull');
            return redirect('/manager_dashboard/projects');
    
        } 
    }

    public function index()
    {
        # code...
        if (Auth::user()->role == 'admin') {
            $projects = Project::all()->sortByDesc("created_at");

            return view('admin.all_projects', compact('projects'));
            # code...
        }elseif (Auth::user()->role == 'manager') {
            # code...
           $projects =   Department::find(Auth::user()->department_id )->projects()->get()->sortByDesc('created_at');

           // return $projects;
            return view('manager.all_projects', compact('projects'));
           // echo $department->id;
            // return $projects;
        }elseif (Auth::user()->role == 'employee' ) {
            # code...
            $projects =   Department::find(Auth::user()->department_id )->projects()->get()->sortByDesc('created_at');

            return view('employee.all_projects', compact('projects'));
        }
    }


    public function show(Project $project)
    {
        # code...
        if (Auth::user()->role == 'admin') {
            # code...
            $department = $project->departments()->first();
        }else{
            $department = Auth::user()->department()->first();
        }

        $DepartmentProject = DepartmentProject::where([['project_id','=',$project->id],['department_id','=',$department->id]])->first();
        
        $miniprojects = collect([]);

        if ($DepartmentProject != null) {
            $miniprojects = $DepartmentProject->miniprojects()->get();
            # code...
        }

        if (Auth::user()->role == 'admin') {

            return view('admin.view_project', compact(['project','miniprojects']));
            # code...
        } elseif(Auth::user()->role == 'manager') {
            # code...
            // return $miniprojects;
            return view('manager.view_project', compact(['project','miniprojects']));
        }elseif ( Auth::user()->role == 'employee' ) {
            # code...

            return view('employee.view_project', compact(['project','miniprojects']));
        }
        
        // return $project;
    }

    public function timeline(Project $project)
    {
        # code...
        return view('admin.project_timeline', compact('project'));
    }

    public function split(Project $project,Request $request )
    {
        # code...
        // return $request->all();
        $numOfMiniProjects = $request->number;

        $department = Auth::user()->department()->first();
        // $department = Department::find($department->id)->projects()->get();
        $DepartmentProject = DepartmentProject::where([['project_id','=',$project->id],['department_id','=',$department->id]])->first();

        $data = $request->all();
        
        $i = 0;
        // return $request->all();

        if ($project->start_date == null) {
            # code...
            // $delivary_date = $project->delivary_date
            $start_date = new Carbon("Africa/Cairo");
            $project->start_date = $start_date;
            $project->status = 1;
            $project->save();

        }

        $array = array();
        while ($numOfMiniProjects > 0) {
            # code...
            while (true) {
                $i++;

                $idf = "numOfdys".($i);
                if (array_key_exists($idf,$data) ) {
                    # code...
                    $idf = "numOfdys".($i);
                    $select = "select".($i);
                    
                    if (array_key_exists($data[$select], $array)) {
                        
                        // return  array('error' => "Can't");
                        $request->session()->flash('splitFail', 'you can\'t add two Sub-Projects to the same project');
                        return redirect('manager_dashboard/projects#splitProjectFail');
                    }else{
                        
                        $array[$data[$select]] = $data[$idf];
                    }

                    break;
                }else{
                    continue;
                }
            }
            $numOfMiniProjects--;
        }

        $created = new Carbon($project->delivary_date);
        $now = Carbon::now("Africa/Cairo");
        $difference = ($created->diff($now)->days < 1)
            ? 'today'
            : $created->diffInDays($now);

        foreach ($array as $key => $value) {
            if ($array[$key] > $difference) {
                # code...
                $request->session()->flash('splitFail', 'You can\'t add a Sub-Project with Regular days that exceeds the delivary date');
                return redirect('manager_dashboard/projects#splitProjectFail');
            }
        }

        foreach ($array as $key => $value) {
            # code...
            $miniproject = new MiniProject;
            $miniproject->regular_days = $value;
            $miniproject->name = $key;
            $miniproject->departments_projects_id = $DepartmentProject->id;
            $miniproject->save();
        }
        // return $array;
        $request->session()->flash('splitSuccess', 'Sub Projects created successfully');
        return redirect('manager_dashboard/projects#splitProject');
    }


    public function getMiniProjects(Project $project)
    {   
        # code...
        $department = Auth::user()->department;
        $DepartmentProject = DepartmentProject::where([['project_id','=',$project->id],['department_id','=',$department->id]])->first()->miniprojects;

        return $DepartmentProject;
    }

    public function getDocument($type,$path)
    {
        # code...
        $path = "uploads/".$type."/".$path;
        
        return  response()->download($path);

    }

    public function graph()
    {
        $projects = Project::get(['id','name']);
        $departments = Department::get(['id','name']);
        $realResult = array();
        foreach ($projects as $project) {
            $project_departments = $project->departments()->get(['name']);
            $result['project_name'] = $project->name ;
            foreach ($departments as $department) {
                $flag = false;
                foreach ($project_departments as $project_department) {
                    if ($department->name == $project_department->name ) {
                       $flag = true;
                        break;
                    }
                }
                if ($flag == true) {
                    $miniprojects =  DepartmentProject::where([['project_id','=',$project_department->pivot->project_id],['department_id','=',$project_department->pivot->department_id]])->first()->miniprojects;
                   $count = 0;
                   foreach ($miniprojects as $miniproject) {
                            $count += $miniproject->tasks()->get()->count();
                   }
                   $result[$department->name] = $count;
                }else{
                   $result[$department->name] = 0;
                }
            }
            array_push($realResult, $result);
        }
        $result = array('data_per_project'=> $realResult, 'departments' => $departments);
        return $result;
    }

    public function managerGraph()
    {
        $final = array('projects' => array(),'sub_departments' => array());
        $final['sub_departments'] = explode('/', Auth::user()->department->sub_department);
        $projects = Auth::user()->department->projects()->get();

        foreach ($projects as $project) {
            $pro = array('project_name' => $project->name,'total_tasks' => 0,);
            foreach ($final['sub_departments'] as $key ) {
                $pro[$key] = 0;
            }
            $minis = $project->DepartmentProject()->first()->miniprojects()->get();
            if (!$minis->isEmpty()) {
                foreach ($minis as $mini) {
                    $tasks = $mini->tasks()->get();
                    if (!$tasks->isEmpty()) {
                        foreach ($tasks as $task) {
                            if ($task->status == 2) {
                                $pro[$mini->name] += 1;
                            }
                            $pro['total_tasks'] += 1;
                        }
                    }
                }
            }
            array_push($final['projects'], $pro);
        }
        return $final;
    }

    public function pause(Project $project,Request $request)
    {
        if (Auth::user()->role == 'admin') {
            $project->status = 3;
            $project->save();
            $pause = new Pause();
            $date = Carbon::now();
            $pause->start = $date->toDateTimeString();
            $project->pauses()->save($pause);
            return back();
        }elseif (Auth::user()->role == 'manager') {
            if(count($project->DepartmentProject) == 1 && $project->DepartmentProject()->first()->department_id == Auth::user()->department->id){
                $project->status = 3;
                $project->save();
                $pause = new Pause();
                $date = Carbon::now();
                $pause->start = $date->toDateTimeString();
                $project->pauses()->save($pause);
                return back();
            }else{
                 $request->session()->flash('failPause', 'You are no authorized to pause that project only the admin can');
                return back();
            }
        }
    }

    public function resume(Project $project)
    {
        $task = new Task();
        $project->status = 1;
        $project->save();
        $date = Carbon::now();
        $pause = $project->pauses()->orderBy('created_at','desc')->first();
        $pause->end = $date->toDateTimeString();
        $pause->number_of_hours = $task->get_working_hours($pause->start,$date);
        $pause->save();
        return back();   
    }

    public function extend(Project $project,Request $request)
    {
        $validator = Validator::make($request->all(), [
       
            'delivery_date' => 'required|date|after:today',
            'contract_period' => 'required|numeric',

        ]);

        if ($validator->fails())
        {   
            return back()->withErrors($validator)->withInput();
        }

        if (Auth::user()->role == 'admin' || (Auth::user()->role == 'manager' && (count($project->DepartmentProject) == 1 && $project->DepartmentProject()->first()->department_id == Auth::user()->department->id))) {

            $date = new Carbon($request->delivery_date);
            $project->delivary_date = $date->toDateTimeString();
            $project->contract_period = $request->contract_period;
            $project->save();
            $request->session()->flash('extendSuccess', 'Project extended successfully');
            return back();

        }else{

            $request->session()->flash('extendFail', 'You are no authorized to pause that project only the admin can');
                return back();
        }
    }

    public function userproject(Project $project,User $user)
    {
        $tasks = $user->tasks;
        $projecttasks = collect();
        foreach ($tasks as $task) {
            if ($task->miniproject->DepartmentProject->project->id == $project->id) {
                $projecttasks->push($task);
            }
        }
        if (Auth::user()->role == 'admin') {
            return view('admin.userproject', compact('projecttasks'));
        }elseif(Auth::user()->role == 'manager'){
            return view('manager.userproject', compact('projecttasks'));
        }else{
            return view('employee.userproject', compact('projecttasks'));

        }
    }
}
