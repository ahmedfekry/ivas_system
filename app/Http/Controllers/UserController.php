<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{

	// public function __construct()
	// {
		# code...
	// }
    
    public function show(User $user)
    {
    	return view('user.show',compact('user'));
    }

    public function getEmployeeBySub($sub,$department)
    {
    	$users = User::where([['department_id','=',$department],['category','=',$sub]])->get();
    	return $users;
    }
}
