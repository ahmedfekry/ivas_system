<?php
namespace App\Http\Controllers;

use App\Project;
use App\Department;
use Carbon\Carbon;
use App\Input;
use App\Document;
use Auth;
use Validator;
use App\User;
use App\DepartmentProject;
use App\MiniProject;
use App\Task;
use App\TaskDocument;
use App\ToDo;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ToDoController extends Controller
{
    //
	public function store(Request $request)
	{
		# code...
		$validator = Validator::make($request->all(), [
            'title' => 'required',
            'regular_hours' => 'required|numeric',
            'project_id' => 'required|numeric',
            'priority' => 'required|numeric',
        ]);

        if ($validator->fails())
        {   
            return redirect('/employee_dashboard#createToDo')->withErrors($validator)->withInput();
        }

		$todo = new ToDo();
		$project = Project::find($request->project_id);


		$todo->title = $request->title;
		$todo->priority = $request->priority;
		$todo->regular_hours = $request->regular_hours;
		$todo->project_id = $request->project_id;
		$todo->user_id = Auth::user()->id;
		$todo->save();

		$project->todos()->save($todo);
		Auth::user()->todos()->save($todo);

		// return $todo;
		 $request->session()->flash('todoSuccess', 'Todo created successfull');
		 return redirect('/employee_dashboard#createToDo');

	}

	public function done(ToDo $todo)
	{
		# code...
		$todo->priority = 3;
		$todo->save();
		return redirect('/employee_dashboard');
	}
}
