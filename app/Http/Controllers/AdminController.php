<?php

namespace App\Http\Controllers;

use App\User;
use App\Department;
use App\Project;
use Carbon\Carbon;
use Auth;
use Hash;
use File;
use Validator;


use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    
    public function __construct()
    {
        # code...
        $this->middleware('auth');
    }

    public function messages()
    {

        return view('admin.messages');
    }

    public function target()
    {

        return view('admin.target');
    }

    public function create()
    {
        # code...
        $departments = Department::all();

        return view('admin.new_employee', compact('departments'));
    }

    public function admin_dashboard()
    {
        # code...
        
        $projects = Project::all()->sortByDesc("created_at");

        return view('admin.welcome', compact('projects'));
    }

    public function profile()
    {
        # code...
        $user = Auth::user();

        return view('admin.profile', compact('user'));
    }

    public function store(Request $request)
    {
      

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'phone_number' => 'required|min:11|unique:users',
            'gender' => 'required|min:4',
            'password' => 'required|min:8',
            'birthday' => 'required|date|before:yesterday',
            'hire_date' => 'required|date|before:today',
            'salary' => 'required|numeric',
            'role' => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect('/admin_dashboard/employee/new#editPassword')->withErrors($validator)->withInput();
        }

        // return $request->all();

        $data =  $request->all();
        $user = new User;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        $user->title = $data['title'];
        $user->gender = $data['gender'];
        $user->password =  Hash::make($data['password']);
        $user->address = $data['address'];
        $user->birthday =  new Carbon($data['birthday']);
        $user->hire_date =  new Carbon($data['hire_date']);
        $user->about = $data['about'];
        $user->salary = $data['salary'];
        $user->role = $data['role'];


        if ( $data['role'] == 'manager' || $data['role'] == 'employee') {
             $validator = Validator::make($request->all(), [
                'department' => 'required|numeric|min:1'
            ]);
            if ($validator->fails())
            {
                return redirect('/admin_dashboard/employee/new#editPassword')->withErrors($validator)->withInput();
            }
            if ($data['role'] == 'manager') {
                # code...
                $_department = Department::find($data['department']);

                $users = $_department->users()->get();
                
                foreach ($users as $user) {
                    if ($user->role == 'manager') {
                        $request->session()->flash('failedCreateEmployee', 'You cann\'t create two managers for the same department');
                        return redirect('/admin_dashboard/employee/new#editPassword');
                    }
                }
            }
            
            $user->department_id = $data['department'];
            
            if ( $request->exists('selectSubDepartments') ) {
                $user->category = $data['selectSubDepartments'];
            }else{
                $user->category = 'general';
            }
        
        }elseif($data['role'] == 'admin') {
            $_department = Department::where('name','=','Admin')->first();
            if (!$_department) {
                $_department = new Department();
                $_department->name = 'Admin';
                $_department->description = 'Admin';
                $_department->sub_department = 'admin';
                $_department->save();
            }
            $user->department_id = $_department->id;
            $user->category = 'admin';
        }        

        if ($data['gender'] == 'female') {
            $user->meternity = 14;
            $user->military_service = 0;
        }else{
            $user->meternity = 0;
            $user->military_service = 14;
        }
        $user->vacation = 15;
        $user->emergency = 6;
        $user->marriage = 7;
        $user->parent_id = $data['supervisor'];

        $user->save();

        $request->session()->flash('success', 'Employee created successfull');
       return redirect('/admin_dashboard/employees');
    }

    public function employees()
    {
        # code...
        $employees = User::all();
        // return $employees;
        return view('admin.all_employees', compact('employees'));
    }


    public function edit_profile(Request $request)
    {
        # code...
         $user = User::find(Auth::user()->id);

        $bool = 0;
        if ($request->has('first_name') && ($request->first_name != $user->first_name)) {
            $user->first_name = $request->first_name;
            $bool = 1;
        }
        if ($request->has('last_name') && ($request->last_name != $user->last_name)) {
            $user->last_name = $request->last_name;
            $bool = 1;
        }
        if ($request->has('birthday') && ($request->birthday != $user->birthday)) {
            $user->birthday = $request->birthday;
            $bool = 1;
        }
       

        if ($request->has('title') && ($request->title != $user->title)) {
            $user->title = $request->title;
            $bool = 1;
        }

        if ($request->has('phone_number') && ($request->phone_number != $user->phone_number)) {
            $user->phone_number = $request->phone_number;
            $bool = 1;
        }

        if ($request->has('email') && ($request->email != $user->email)) {
            $user->email = $request->email;
            $bool = 1;
        }
        if ($request->has('about') && ($request->about != $user->about)) {
            $user->about = $request->about;
            $bool = 1;
        }
        if ($request->has('gender') && ($request->gender != $user->gender)) {
            $user->gender = $request->gender;
            $bool = 1;
        }

        if ($bool == 1) {
            # code...
            $user->save();
            $request->session()->flash('Success_update', 'Information updated successfully');
            return redirect('/admin_dashboard/profile#edit_profile');
        }else{
            $request->session()->flash('no_update', 'No update');
            return redirect('/admin_dashboard/profile#edit_profile');
        }


    }

     public function update_password(Request $request)
    {
        # code...
          $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8'
        ]);

        if ($validator->fails())
        {
            return redirect('/admin_dashboard/profile#editPassword')->withErrors($validator)->withInput();
        }

        if ($request->new_password != $request->confirm_password) {
            # code...
            $request->session()->flash('failed', 'Passwords does\'nt match');

            return redirect('/admin_dashboard/profile#editPassword');
        } else {
            # code...
            if ( ! Hash::check($request->old_password , Auth::user()->password)) {
                
                 $request->session()->flash('failed', 'The old password is incorrect');

                return redirect('/admin_dashboard/profile#editPassword');
            }else {

                $user = User::find(Auth::user()->id);
                $user->password =  Hash::make( $request->new_password );

                $user->save();

                 $request->session()->flash('success', 'Password updated successfully');

                return redirect('/admin_dashboard/profile#editPassword');
            }
        }
        
    }

    public function image_upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_picture' => 'required',
        ]);

        if ($validator->fails())
        {
            return redirect('/admin_dashboard/profile#edit_image')->withErrors($validator)->withInput();
        }

        $ext =  $request->file('profile_picture')->getClientOriginalExtension();
    
        if ( $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' ) {
            $user = Auth::user();  
            if ($user->profile_picture) {
                File::delete($user->profile_picture);
            } 
            
            $path = $request->file('profile_picture')->move('uploads/user/', time().'_'.$request->file('profile_picture')->getClientOriginalName());
            
            $user->profile_picture = $path;

            $user->save();

            $request->session()->flash('success_image', 'Image updated successfully');
            
            return redirect('/admin_dashboard/profile#edit_image');
        }else{
            
            $request->session()->flash('failed_image', 'file is not an image');
            return redirect('/admin_dashboard/profile#edit_image');
            
        }
    }

    public function show_employee(User $employee)
    {
        $projects =  array();
        $tasks = $employee->tasks;
        foreach ($tasks as $task) {
            $name = $task->miniproject->DepartmentProject->project->name;
            $project = $task->miniproject->DepartmentProject->project;
            $projects[$name] = $project;
        }
        
        if (Auth::user()->role == 'admin') {
            return view('admin.show_employee',compact(['employee','projects']));
        }elseif (Auth::user()->role == 'manager') {
            return view('manager.show_employee', compact(['employee','projects']));
        }
    }

    public function delete_employee(User $employee)
    {
        if (Auth::user()->role == 'admin') {
            $employee->delete();
            return redirect('/admin_dashboard/employees');
        } 
    }
}
