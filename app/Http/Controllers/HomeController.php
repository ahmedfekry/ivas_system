<?php

namespace App\Http\Controllers;

use App\Project;
use App\Department;
use Carbon\Carbon;
use App\Input;
use App\Document;
use Auth;
use Validator;
use App\User;
use App\DepartmentProject;
use App\MiniProject;
use App\Task;
use App\TaskDocument;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('role', ['except' => ['search']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('home');
    }

    

    public function search($string)
    {
        if (Auth::user()->role == 'admin') {

            $projects = Project::where('name', 'LIKE', '%' . $string . '%')->get(['id','name']);
            $users = User::where('first_name', 'LIKE', '%' . $string . '%')->orWhere('last_name', 'LIKE', '%' . $string . '%')->orWhere(DB::raw("CONCAT(`first_name`,' ', `last_name`)"),'LIKE','%' . $string . '%')->get(['id','first_name','last_name']);

            $tasks = Task::where('name', 'LIKE', '%' . $string . '%')->get(['id','name']);
            $result = array('users' => $users ,'projects' => $projects, 'tasks' => $tasks);
        
        }else{

            $projects = Auth::user()->department()->first()->projects()->where('name', 'LIKE', '%' . $string . '%')->get(['project.id','project.name']);
            $users = Auth::user()->department()->first()->users()->where('first_name', 'LIKE', '%' . $string . '%')->get(['users.id','users.first_name','users.last_name']);
            if ($users->isEmpty()) {
                # code...
                $users = Auth::user()->department()->first()->users()->Where('last_name', 'LIKE', '%' . $string . '%')->get(['users.id','users.first_name','users.last_name']);//->orWhere(DB::raw("CONCAT(`first_name`,' ', `last_name`)"),'LIKE','%' . $string . '%')->get(['users.id','users.first_name','users.last_name']);
            }

            if ($users->isEmpty()) {
                # code...
                $users = Auth::user()->department()->first()->users()->Where(DB::raw("CONCAT(`first_name`,' ', `last_name`)"),'LIKE','%' . $string . '%')->get(['users.id','users.first_name','users.last_name']);
            }
            $userss = Auth::user()->department()->first()->users()->get();
            $tasks = collect();
            foreach ($userss as $user) {
                # code...
                $task = $user->tasks()->where('name', 'LIKE', '%' . $string . '%')->get(['task.id','task.name']);
                if (!$task->isEmpty()) {
                    foreach ($task as $tas) {
                        $tasks->push($tas);
                    }
                }
                
            }
            $result = array('users' => $users ,'projects' => $projects, 'tasks' => $tasks);
        }
        return $result;
    }
}
