<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Hash;
use File;
use Validator;

use Illuminate\Http\Request;

class ManagerController extends Controller
{
    //
    public function __construct()
    {
        # code...
        $this->middleware('auth');
    }

     public function manager_dashboard()
    {
        # code...
        // $this->middleware('auth');
        $projects = Auth::user()->department()->first()->projects()->get();
        // return $projects;
        $todos = Auth::user()->todos()->get()->sortByDesc('created_at');
        return view('manager.welcome',compact(['projects','todos']));
        // return $projects;
    }

     public function profile()
    {
        # code...
        $user = Auth::user();

        return view('manager.profile', compact('user'));
    }

     public function edit_profile(Request $request)
    {
        # code...
        $user = User::find(Auth::user()->id);

        $bool = 0;
        if ($request->has('first_name') && ($request->first_name != $user->first_name)) {
            $user->first_name = $request->first_name;
            $bool = 1;
        }
        if ($request->has('last_name') && ($request->last_name != $user->last_name)) {
            $user->last_name = $request->last_name;
            $bool = 1;
        }
        if ($request->has('birthday') && ($request->birthday != $user->birthday)) {
            $user->birthday = $request->birthday;
            $bool = 1;
        }
       

        if ($request->has('title') && ($request->title != $user->title)) {
            $user->title = $request->title;
            $bool = 1;
        }

        if ($request->has('phone_number') && ($request->phone_number != $user->phone_number)) {
            $user->phone_number = $request->phone_number;
            $bool = 1;
        }

        if ($request->has('email') && ($request->email != $user->email)) {
            $user->email = $request->email;
            $bool = 1;
        }
        if ($request->has('about') && ($request->about != $user->about)) {
            $user->about = $request->about;
            $bool = 1;
        }
        if ($request->has('gender') && ($request->gender != $user->gender)) {
            $user->gender = $request->gender;
            $bool = 1;
        }

        if ($bool == 1) {
            # code...
            $user->save();
            $request->session()->flash('Success_update', 'Information updated successfully');
            return redirect('/manager_dashboard/profile#edit_profile');
        }else{
            $request->session()->flash('no_update', 'No update');
            return redirect('/manager_dashboard/profile#edit_profile');
        }
    }
    
    public function update_password(Request $request)
    {
        # code...
        //  $this->validate($request,[
                
        // ]);

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8'
        ]);

        if ($validator->fails())
        {
            return redirect('/manager_dashboard/profile#editPassword')->withErrors($validator)->withInput();
        }
         
        if ($request->new_password != $request->confirm_password) {
            # code...
            $request->session()->flash('failed', 'Passwords does\'nt match');

            return redirect('/manager_dashboard/profile#editPassword');
        } else {
            # code...
            if ( ! Hash::check($request->old_password , Auth::user()->password)) {
                
                 $request->session()->flash('failed', 'The old password is incorrect');

                return redirect('/manager_dashboard/profile#editPassword');
            }else {

                $user = User::find(Auth::user()->id);
                $user->password =  Hash::make( $request->new_password );

                $user->save();

                 $request->session()->flash('success', 'Password updated successfully');

                return redirect('/manager_dashboard/profile#editPassword');
            }
        }
        
    }

    public function image_upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_picture' => 'required',
        ]);

        if ($validator->fails())
        {
            return redirect('/manager_dashboard/profile#edit_image')->withErrors($validator)->withInput();
        }
        
        $ext =  $request->file('profile_picture')->getClientOriginalExtension();
    
        if ( $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' ) {
            $user = Auth::user();  
            if ($user->profile_picture) {
                File::delete($user->profile_picture);
            } 
            
            $path = $request->file('profile_picture')->move('uploads/user/', time().'_'.$request->file('profile_picture')->getClientOriginalName());
            
            $user->profile_picture = $path;

            $user->save();

            $request->session()->flash('success_image', 'Image updated successfully');
            
            return redirect('/manager_dashboard/profile#edit_image');
        }else{
            
            $request->session()->flash('failed_image', 'file is not an image');
            return redirect('/manager_dashboard/profile#edit_image');
            
        }
    }
}
