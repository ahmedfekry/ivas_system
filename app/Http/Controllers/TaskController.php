<?php

namespace App\Http\Controllers;

use App\Project;
use App\Department;
use Carbon\Carbon;
use App\Input;
use App\Document;
use Auth;
use Validator;
use App\User;
use App\DepartmentProject;
use App\MiniProject;
use App\Task;
use App\Pause;
use App\TaskDocument;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
// use Illuminate\Database\Eloquent\Collection;

class TaskController extends Controller
{
    //
    public function create_task()
    {
        # code...
        $projects = Auth::user()->department->projects;
        $users = Auth::user()->department->users;

        // return $users;
        return view('manager.new_task',compact('projects','users') );
    }

    public function new_task(Project $project)
    {
        $users = Auth::user()->department->users;
        
        return view('manager.new_task2',compact('project','users') );
    }

    public function tasks()
    {
        # code...
        if (Auth::user()->role == 'manager') {
            
            $tasks = collect([]);
            $users = Auth::user()->department()->first()->users()->get();

            foreach ($users as $user) {
                # code...
                if (!$user->tasks->isEmpty()) {
                    foreach ($user->tasks as $task) {
                        $tasks->push($task);
                    }
                }
            }
            // return $tasks;
            return view('manager.all_tasks',compact(['tasks']));
        }elseif (Auth::user()->role == 'employee') {
           $tasks = Auth::user()->tasks()->get();
           // return $tasks;
           return view('employee.all_tasks',compact('tasks'));
        }
    }

    public function store_task(Request $request)
    {
        $validator = Validator::make($request->all(), [
               'selectSubProject' => 'required|numeric',
                'selectProject' => 'required|numeric',
                'regular_hours' => 'required|numeric',
                'priority' => 'required',
                'title' => 'required|max:255|min:2',
                'user_id' => 'required|numeric',
        ]);

        if ($validator->fails())
        {   
            // $url = '/project/'.$request->selectProject.'/new_task';
            return back()->withErrors($validator)->withInput();
        }

        $task = new Task;

        $task->name = $request->title;
        if ($request->has('description')) {
            $task->description = $request->description;
        }else{
            $task->description = " ";
        }
        if ($request->has('start_date')) {
            $task->start_date = new Carbon($request->start_date);
        }
        
        $task->regular_hours = $request->regular_hours;

        $task->periority = $request->priority;
        $task->miniproject_id = $request->selectSubProject;

        $user = User::find($request->user_id);

        $user->tasks()->save($task);

        if ($request->hasFile('taskDocument')) {
            // return "asd";
            $path = $request->file('taskDocument')->move('uploads/task/', time().'_'.$request->file('taskDocument')->getClientOriginalName());
            $TaskDocument = new TaskDocument;
            $TaskDocument->url = time().'_'.$request->file('taskDocument')->getClientOriginalName();
            $task->task_documents()->save($TaskDocument);
        }

        

        $request->session()->flash('success', 'Task created successfull');
        return redirect('/manager_dashboard/tasks');
    }

    public function approve(Task $task)
    { 
        $task->actual_start = Carbon::now("Africa/Cairo")->toDateTimeString();
        $task_ = new Task;
        $task->end_date = $task_->approval(Carbon::now("Africa/Cairo"), $task->regular_hours);
        $task->status = 1;
        $task->save();

        return redirect('/employee_dashboard/tasks');
    }


    public function finish(Task $task, Request $request)
    {

        $finish_path = null;
        $url = null;
        $text = null;

        if ($request->has('finishUrl')) {
            $url = $request->finishUrl;
        }

        if ($request->has('finishText')) {
            $text = $request->finishText;
        }

        if ($request->hasFile('finishFileToUpload')) {
            $path = $request->file('finishFileToUpload')->move('uploads/usertask/', time().'_'.$request->file('finishFileToUpload')->getClientOriginalName());
            $finish_path = time().'_'.$request->file('finishFileToUpload')->getClientOriginalName();
        }

        $task->status = 2;
        $task->actual_end = Carbon::now("Africa/Cairo")->toDateTimeString();
        $task->finish_text = $text;
        $task->finish_file_path = $finish_path;
        $task->finish_url = $url;
        $task->save();
        
        return redirect('/employee_dashboard/tasks');

    }
    // 0 -> initial
    // 1 -> start
    // 2 -> finished
    // 3 -> paused
    // 
    public function start_pause(Request $request,Task $task)
    {
        if ($task->status == 1) {
            $now = new Carbon("Africa/Cairo");
            $pause = new Pause();
            $pause->start = $now->toDateTimeString();
            $task->status = 3;
            $task->save();
            $task->pauses()->save($pause);

            $end_date = new Carbon($task->end_date);
            $start_date = new Carbon($task->actual_start);
            $now = Carbon::now('Africa/Cairo');
            $passed_working_hours = $task->get_working_hours($start_date,$now->toDateTimeString());
            $regular_hours = $task->regular_hours;
            $total_pause_time = 0;
            $pauses = $task->pauses;
            if(!$pauses->isEmpty() && count($pauses) > 1){
                foreach ($pauses as $pause) {
                    $total_pause_time += $pause->number_of_hours;
                }
            }
            $progress = round((($total_pause_time + $passed_working_hours)/( $total_pause_time + $regular_hours))*100);
            $task->progress_pause = $progress;
            $task->save();
            $request->session()->flash('pauseSuccess', 'Task paused successfully');
            return redirect('/employee_dashboard/tasks');
        }elseif ($task->status == 3) {
            $request->session()->flash('pauseFail', 'You already paused this task');
            return redirect('/employee_dashboard/tasks');         
        }else{
            $request->session()->flash('pauseFail', 'This Task has not started yet it is either finished or upcomming');
            return redirect('/employee_dashboard/tasks');
        }
    }

    public function end_pause(Request $request,Task $task)
    {

        if ($task->status == 3) {
            $now = new Carbon("Africa/Cairo");
            $pause = $task->pauses()->get()->sortByDesc('created_at')->first();
            if ($pause) {
                if ($pause->end == null && $pause->number_of_hours == null) {
                    $pause->end = $now->toDateTimeString();
                    $pause->number_of_hours = $task->get_working_hours($pause->start,$now);
                    $pause->save();
                    $task->status = 1;
                    $task->save();
                    $request->session()->flash('pauseSuccess', 'Task resumed successfully');
                    return redirect('/employee_dashboard/tasks');
                }
            }
        }else{
            $request->session()->flash('pauseFail', 'This task is not paused');
            return redirect('/employee_dashboard/tasks');

        }
    }


}
