<?php

namespace App\Http\Controllers;

use App\Department;
use App\User;
use Auth;
use Validator;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    //
    protected $table = "department";

     /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
     
    public function index()
    {
    	# code...
    	$departments = Department::all();
    	return view('department.index', compact('departments'));
    }

    public function team()
    {
        
        if (Auth::user()->role == 'manager') {
            $users = Department::find( Auth::user()->department_id )->users()->get();
            return view('manager.all_team', compact('users'));
        }elseif (Auth::user()->role == 'employee') {
            $users = Auth::user()->children;
            return view('employee.all_team', compact('users'));
        }
    }

    public function getSubDepartment(Department $department)
    {
        # code...

        $app = array('subDepartment' => $department->sub_department);
        return $app;
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - department';
        
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'sub' => 'required|array'
        ]);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
        // return $request->all();

        $department = new Department();

        
        $department->name = $request->name;

        
        $department->description = $request->description;

        $subs = '';
        if ($request->sub[0] == "" ) {
            return back();
        } else {
            foreach ($request->sub as $sub_department) {
                $subs .= $sub_department.'/';
            }
            $subs .= 'n';
        }
        $subs = str_replace('/n', '', $subs);

        $department->sub_department = $subs;

        $department->save();
        $request->session()->flash('success','department created successfully');
        return redirect('/departments');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - department';

        $department = Department::findOrfail($id);
        return view('department.show',compact('title','department'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - department';
        if($request->ajax())
        {
            return URL::to('department/'. $id . '/edit');
        }

        
        $department = Department::findOrfail($id);
        return view('department.edit',compact('title','department'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $department = Department::findOrfail($id);
        
        $department->name = $request->name;
        
        $department->description = $request->description;
        
        $department->sub_department = $request->sub_department;
        
        
        $department->save();

        return redirect('department');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('Warning!!','Would you like to remove This?','/department/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::findOrfail($id);
        $department->delete();
        return URL::to('department');
    }

    public function getUsers(Department $department)
    {
        $app = array('users' => $department->users);
        return $app;
    }

    public function getAdmins()
    {   
        $users = User::where('role','=','admin')->get(['id','first_name','last_name']);
        $app = array('users' => $users);
        return $app;
    }
}
