<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $table = "departments";

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function projects()
    {
    	# code...
    	return $this->belongsToMany('App\Project','departments_projects','department_id','project_id');
    }
}
