<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = "projects";
    public function documents()
    {
    	# code...
		return $this->hasMany('App\Document');

    }


    public function departments()
    {
    	# code...
    	return $this->belongsToMany('App\Department','departments_projects','project_id','department_id');
    }

    public function DepartmentProject()
    {
        # code...
        return $this->hasMany('App\DepartmentProject');
    }

    public function todos()
    {
        # code...
        return $this->hasMany('App\Project');
    }

    public function pauses()
    {
        return $this->hasMany('App\Pause');
    }
}
