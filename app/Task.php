<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Task extends Model
{
    //
    protected $table = "tasks";
    public function user()
    {
    	# code...
        return $this->belongsTo('App\User');
    }

    public function task_documents()
    {
    	# code...
    	return $this->hasMany('App\TaskDocument');
    }

    public function miniproject()
    {
        # code...
        return $this->belongsTo('App\MiniProject');
    }

    public function ifHoliday(Carbon $end)
    {
        $end = $end->toDateString();
        $vacation = Vacation::where('date', '=', $end)->first();
        if ($vacation) {
            return true;
        }
        return false;
    }

    public function addWeekdaysP(Carbon $end, $numberOfDays)
    {
        for ($i=0; $i < $numberOfDays; $i++) { 
            $end->addDay(1);
            if (explode(',',$end->toCookieString())[0] == 'Friday') {
                $end = $end->addDay(2);    
            }
            $flag = $this->ifHoliday($end);
            if ($flag) {
                $end = $this->addWeekdaysP($end,1);
            }
        }

        return $end;
    }

    public function addHoursP(Carbon $end, $numb)
    {
        // echo $numb;
        for ($i=0; $i < $numb; $i++) { 
            $end = $end->addHours(1);
            if ($end->hour == 13) {
               $end = $end->addHours(1);
            }
        }
        return $end;
    }
    public function approval(Carbon $carbon,$regular_hours){
        $start_date = $carbon;
        $end_date = $start_date;
        $numberOfDays = 0;
        // if the regular hours is greater than 7 hours 
        // you need to divide the regular hours by 7 to get the number of days then get the modulas
        if ( $regular_hours > 7) {
            $numberOfDays = intval($regular_hours/7);
            $regular_hours = $regular_hours%7;
            // $end_date = Task::addWeekdaysP($end_date,$numberOfDays);
        }
        // add the regular hours to the end date
        $end_date = $this->addHoursP($end_date,$regular_hours);
        $day = explode(',',$end_date->toCookieString())[0];
        $hours = $end_date->hour;
        // check to see if the end date is greater than Thursday at 17:00 or if the end date is greater than 17
        if ($day == 'Thursday' && $hours > 17) {
            $end_date = $end_date->addHours(65);
        }elseif ($day != 'Thursday' && $hours > 17) {
            $end_date = $end_date->addHours(16);
        }
        $flag = $this->ifHoliday($end_date);
        if ($flag) {
            $end_date = $this->addWeekdaysP($end_date,1);
        }
        
        $end_date = $this->addWeekdaysP($end_date,$numberOfDays);
        return $end_date;
    }

    // get begin and end dates of the full period
    // $date_begin = '2017-1-8 09:00:00';
    // $date_end = '2017-1-12 17:00:00';

    // echo get_working_hours($date_begin, $date_end);

    function get_working_hours($from,$to)
    {
        // timestamps
        $from_timestamp = strtotime($from);
        $to_timestamp = strtotime($to);

        // work day seconds
        // $workday_start_hour = 10;
        $workday_end_hour = 7;
        $workday_seconds = $workday_end_hour*3600;

        // work days beetwen dates, minus 1 day
        $from_date = date('Y-m-d',$from_timestamp);
        $to_date = date('Y-m-d',$to_timestamp);
        $workdays_number = count($this->get_workdays($from_date,$to_date))-1;
        // echo $work;
        $workdays_number = $workdays_number<0 ? 0 : $workdays_number;

        // start and end time
        $start_time_in_seconds = date("H",$from_timestamp)*3600+date("i",$from_timestamp)*60;
        $end_time_in_seconds = date("H",$to_timestamp)*3600+date("i",$to_timestamp)*60;

        // final calculations
        $working_hours = ($workdays_number * $workday_seconds + $end_time_in_seconds - $start_time_in_seconds) / 86400 * 24;

        return $working_hours;
    }

    //There are two additional functions. One returns work days array...

    function get_workdays($from,$to) 
    {
        // arrays
        $days_array = array();
        $skipdays = array("Saturday", "Friday");
        $skipdates = $this->get_holidays();

        // other variables
        $i = 0;
        $current = $from;

        if($current == $to) // same dates
        {
            $timestamp = strtotime($from);
            if (!in_array(date("l", $timestamp), $skipdays)&&!in_array(date("Y-m-d", $timestamp), $skipdates)) {
                $days_array[] = date("Y-m-d",$timestamp);
            }
        }
        elseif($current < $to) // different dates
        {
            while ($current < $to) {
                $timestamp = strtotime($from." +".$i." day");
                if (!in_array(date("l", $timestamp), $skipdays)&&!in_array(date("Y-m-d", $timestamp), $skipdates)) {
                    // $days_array[] = date("Y-m-d",$timestamp);
                    array_push($days_array, date("Y-m-d",$timestamp));
                }
                $current = date("Y-m-d",$timestamp);
                $i++;
            }
        }

        return $days_array;
    }

    function get_holidays() 
    {
        // arrays
        $days_array = array();
        $vacations = Vacation::all();
        foreach ($vacations as $vacation) {
            array_push($days_array, $vacation->date);
        }

        return $days_array;

    }


    public function pauses()
    {
        return $this->hasMany('App\Pause');
    }
   
}
