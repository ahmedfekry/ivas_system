<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\UserInterface;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Cmgmyr\Messenger\Traits\Messagable;


class User extends Authenticatable
{
    //
    use Notifiable;
    use Messagable;
	protected $table = "users";

     public function department()
    {
        return $this->belongsTo('App\Department');
    }

    

    public function tasks()
    {
        # code...
        return $this->hasMany('App\Task');
    }

    public function todos()
    {
        # code...
        return $this->hasMany('App\ToDo');

    }

    
    public function parent()
    {
        return $this->belongsTo('App\User', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\User', 'parent_id');
    }

}
