<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskDocument extends Model
{
    //
    protected $table = "taskdocuments";

    public function task()
    {
    	# code...
    	return $this->belongsTo('App\Task');
    }
}
