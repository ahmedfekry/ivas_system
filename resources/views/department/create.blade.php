@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
  
{{-- 
@foreach($departments as $department)
  <li> {{ $department->name }} </li>
@endforeach --}}
	<div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Add Employee</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/admin_dashboard/", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Add Employee</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if ( Session::has('failedCreateEmployee') )
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Failed!</strong> {{ Session::get('failedCreateEmployee')}}
                    </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row" id="new_employee">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> New Department</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                 <form action=" {{ url("/admin_dashboard/department/store") }} " method="POST" class="form-horizontal">
                                 {{ csrf_field() }}
                                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Name</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="Name" value="{{ old('name') }}" class="form-control" name="name" required>
                                          @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                       </div>
                                    </div>
                                    <div id="subcategoylist">
	                                    <div class="form-group{{ $errors->has('sub') ? ' has-error' : '' }}">
	                                       <label class="col-sm-3 col-lg-2 control-label">Sub Department</label>
	                                       <div class="col-sm-2 col-lg-3 controls">
	                                          <input type="text" placeholder="Sub Department" class="form-control" name="sub[]" required>
	                                       </div>
	                                        <div class="col-sm-2 col-lg-2 controls">
	                                          <a href="#"><i class="fa fa-plus"></i></a>
	                                        </div>
	                                    </div>
                                    </div>
                                    
                                   <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">About</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <textarea class="form-control" rows="3" value="{{ old('description') }}" name="description"></textarea>
                                         @if ($errors->has('about'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                          @endif
                                      </div>
                                   </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                           <button type="button" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
            
        <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->
               

@endsection

@section('adminCustomScript')
        <script type="text/javascript" charset="utf-8" async defer>
            $('#departmentSide').addClass('active');
            $('.fa-plus').click(function() {
            	$('#subcategoylist').append('\
            			<div class="form-group added">\
                           <label class="col-sm-3 col-lg-2 control-label">Sub Department</label>\
                           <div class="col-sm-2 col-lg-3 controls">\
                              <input type="text" placeholder="Sub Department" class="form-control" name="sub[]" required>\
                           </div>\
                           <div class="col-sm-2 col-lg-2 controls">\
                              <a href="#"><i class="fa fa-times"></i></a>\
                            </div>\
                        </div>\
            		');
            });

            $('#subcategoylist').on('click','.fa-times',function() {
                event.preventDefault();
                $(this).closest('.form-group').remove();
            });
        </script>
        <!--page specific plugin scripts-->        
        <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-inputmask/bootstrap-inputmask.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-tags-input/jquery.tagsinput.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-pwstrength/jquery.pwstrength.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js') !!}"></script>
        <script src="{!! asset('assets/dropzone/downloads/dropzone.min.j') !!}s"></script>
        <script src="{!! asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
        <script src="{!! asset('assets/clockface/js/clockface.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}"></script> 
        <script src="{!! asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}"></script>
        <script src="{!! asset('assets/ckeditor/ckeditor.js') !!}"></script> 

@endsection