@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
    
            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Dashboard</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-4">
                         <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-tasks"></i> Projects In Progress</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="tasks-in-progress">
                                    @foreach($projects as $project)
                                        <?php 
                                            $delivary_date = new Carbon\Carbon($project->delivary_date);
                                            $start_date = new Carbon\Carbon($project->start_date);
                                            $now = Carbon\Carbon::now();
                                        ?>

                                        @if($project->status == 1)
                                            @if( $delivary_date >= $now )
                                                <li>
                                                    <p>
                                                        {{ $project->name }}
                                                        <span>{{  ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%</span>
                                                    </p>
                                                    <div class="progress progress-mini">
                                                        <div class="progress-bar progress-bar-success" style="width:{{  ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%"></div>
                                                    </div>
                                                </li>
                                            @elseif( $delivary_date < $now )
                                                <li>
                                                    <p>
                                                        {{ $project->name }}
                                                        <span>{{  ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%</span>
                                                    </p>
                                                    <div class="progress progress-mini">
                                                        <div class="progress-bar progress-bar-danger" style="width:{{ ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%"></div>
                                                    </div>
                                                </li>
                                            @endif
                                        @endif

                                    @endforeach
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 row">
                        <div class="col-md-12">
                            <a href="{{ url('/admin_dashboard/project/new') }}" class="tile tile-green">
                                <div class="img">
                                    <i class="fa fa-copy"></i>
                                </div>
                                <div class="content">
                                    <p class="big">+New</p>
                                    <p class="title">Create Project</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-12">
                            <a class="tile tile-red" href="https://outlook.office.com/">
                                <div class="img img-center">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <p class="title text-center">Messages</p>
                            </a>
                        </div>
                    </div>
                    
                    <div class="row col-md-5">
                        <div class="col-md-12">
                            <a href="#" class="tile">
                                <p class="title">Check All Messages</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-12">
                            <a href="{{ url('/admin_dashboard/employees') }}" class="tile tile-magenta">
                                <p class="title">Employees</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-group"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    
                </div>        
                
                <div class="row">
                    <!-- Projects Status-->
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Projects Status</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div id="chart_div" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- //Projects Status-->
                </div>
                
                
                <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->
             
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->
        {{-- <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script> --}}
        {{-- <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script> --}}

@endsection


@section('adminCustomScript')
        
        <!--page specific plugin scripts-->
        <script src="{!! asset('assets/flot/jquery.flot.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.resize.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.pie.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.stack.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.crosshair.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.tooltip.min.js') !!}"></script>
        <script src="{!! asset('assets/sparkline/jquery.sparkline.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-ui/jquery-ui.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-knob/jquery.knob.js') !!}"></script>
        <script src="{!! asset('https://www.gstatic.com/charts/loader.js') !!}"></script>
        <script>
            $(document).ready(function () {
                $('#dashboardSide').addClass('active');
                $('body').addClass('AdminDachboard');
            });
                
        </script>

        
@endsection
