@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
<div id="main-content">
     <div class="page-title">
            <div>
                <h1><i class="fa fa-file-o"></i> Emplyee Info</h1>
            </div>
        </div>
        <!-- END Page Title -->

        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href=" {{ url("/admin_dashboard/", $args = []) }} ">Home</a>
                    <span class="divider"><i class="fa fa-angle-right"></i></span>
                </li>
                <li class="active">Emplyee Info</li>
            </ul>
        </div>
        <!-- END Breadcrumb -->

        <!-- BEGIN Main Content -->
        <div class="emplyee_info row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="fa fa-file"></i> Profile Info</h3>
                        <div class="box-tool">
                            <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                            <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div class="box-content main_info">
                        <div class="row">
                            <div class="col-md-3 profile_img">
                                @if($employee->profile_picture == '')
                                    {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                @else
                                    {{HTML::image( $employee->profile_picture , ' Panny\'s Avatar', ['class' => 'img-responsive img-thumbnail'])}}
                                @endif
                                <br/><br/>
                            </div>
                            <div class="col-md-9 user-profile-info">
                                <p><span>Email:</span> <a href="#"> {{ $employee->email }} </a></p>
                                <p><span>First Name:</span> {{ $employee->first_name }} </p>
                                <p><span>Last Name:</span> {{ $employee->last_name }} </p>
                                <p><span>Birthday:</span> {{ $employee->birthday }}</p>
                                <p><span>Date of Hire:</span> {{ $employee->hire_date }} </p>
                                <p><span>Gender:</span> {{ $employee->gender }}</p>
                                <p><span>Position:</span> {{ $employee->title }}</p>
                                <p><span>Mobile Number:</span> {{ $employee->phone_number }}</p>
                                <p><span>About:</span>{{ $employee->about }} </p>
                                <p><span>Department:</span><a href="{{url('admin_dashboard/department/'.$employee->department->id)}}" title="">{{ $employee->department->name }}</a></p>
                                @if($employee->parent)
                                    <p><span>Supervisor:</span> <a href=" {{ url("/admin_dashboard/employee/".$employee->parent->id, $args = []) }} "> {{ $employee->parent->first_name." ".$employee->parent->last_name }} </a> </p>
                                @endif
                                @if(!$employee->children()->get()->isEmpty())
                                    <p><span>Supervisee:</span>
                                    @foreach($employee->children as $child)
                                        <a href=" {{ url("/admin_dashboard/employee/".$child->id, $args = []) }} "> {{ $child->first_name." ".$child->last_name }} </a>  - 
                                    @endforeach
                                    </p>
                                @endif

                                @if(count($projects) > 0)
                                    <p><span>Projects:</span>
                                    @foreach($projects as $project)
                                        <p> <a href="{{url('admin_dashboard/project/'.$project->id.'/user/'.$employee->id)}}" title=""> {{$project->name}}</a></p>
                                    @endforeach
                                @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- Fixed Chat -->
        <div class="row">
            <div class="box fixed-chat">
                <div class="box-title">
                    <h3><i class="fa fa-comments"></i> Chat</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                        <li>
                            {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                            <div>
                                <div>
                                    <h5>Penny</h5>
                                    <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                </div>
                                <p>hey Sarah</p>
                                <p>how R U?</p>
                            </div>
                        </li>
                        <li>
                            {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                            <div>
                                <div>
                                    <h5>Sarah</h5>
                                    <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                </div>
                                <p>Hi Penny</p>
                                <p>Thanks, how are you ?</p>
                            </div>
                        </li>
                        <li>
                            {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                            <div>
                                <div>
                                    <h5>Penny</h5>
                                    <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                </div>
                                <p>ey, I'm good</p>
                                <p>what's up?</p>
                                <p>what's your plan for dinner?</p>
                            </div>
                        </li>
                        <li>
                            {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                            <div>
                                <div>
                                    <h5>Sarah</h5>
                                    <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                </div>
                                <p>Not much</p>
                                <p>I haven't any plan, why ?</p>
                            </div>
                        </li>
                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                    <div class="messages-input-form">
                        <form method="POST" action="#">
                            <div class="input">
                                <input type="text" name="text" placeholder="Write here..." class="form-control">
                            </div>
                            <div class="buttons">
                                <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Fixed Chat -->
    
    
       </div>
<script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
<script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
<script type="text/javascript" charset="utf-8" async defer>
        $('#employeeSide').addClass('active');
</script>
                
@endsection
