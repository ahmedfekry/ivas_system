@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
	<!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                            
                        <h1><i class="fa fa-list"></i> All Projects </h1>
                        <h4>View all Projects Status</h4>
                    </div>
                </div>
                <!-- END Page Title -->
                
                <!-- END Breadcrumb -->
                @if ( Session::has('success') )                
                {{-- <h1><i class="fa fa-list"></i>  </h1> --}}
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{ Session::get('success')}}
                    </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-list"></i> Projects List</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <br/><br/>
                                <div class="clearfix"></div>
                                <div class="table-responsive" style="border:0">
                                    <table class="table table-advance" id="table1">
                                        <thead>
                                            <tr>
                                                <th style="width:18px"><input type="checkbox" /></th>
                                                <th>Name</th>
                                                <th>Contract Date</th>
                                                <th>Start Date</th>
                                                <th>Project Priority</th>
                                                <th>Project Status</th>
                                                <th>Delivery Date</th>
                                                <th>Finishing Time</th>
                                                <th>Finishing Tasks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($projects as $project)
                                            <?php
                                                $finished = 0;
                                                $unfinished = 0;
                                                if ($project->status != 0) {
                                                    $subs = $project->DepartmentProject()->get();
                                                    foreach ($subs as $sub) {
                                                        # code...
                                                        $minis = $sub->miniprojects()->get();
                                                        foreach ($minis as $minid) {
                                                            # code...
                                                            $tasks = $minid->tasks()->get();
                                                            foreach ($tasks as $task) {
                                                                # code...
                                                                    if ($task->status == 2) {
                                                                        $finished++;
                                                                        
                                                                    }
                                                                $unfinished++;
                                                            }
                                                        }
                                                    }
                                                }
                                            ?>
                                             
                                            <?php 

                                                $delivary_date = new Carbon\Carbon($project->delivary_date);
                                                $start_date = new Carbon\Carbon($project->start_date);
                                                $now = Carbon\Carbon::now();
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" /></td>
                                                <td><a href="{{ url('admin_dashboard/project/'.$project->id) }}">{{ $project->name }}</a></td>
                                                <td> {{ $project->contract_date }} </td>
                                                @if($project->start_date != null)
                                                    <td> {{ $project->start_date }} </td>
                                                @else
                                                    <td> Initial Project </td>
                                                @endif
                                                 @if($project->priority == 0)
                                                <td>Low</td>
                                                @elseif($project->priority == 1)
                                                <td>Medium</td>
                                                @elseif($project->priority == 2)
                                                <td>High</td>
                                                @endif
                                                @if($project->status == 1)
                                                    <td>In Progress</td>
                                                @elseif($project->status == 3)
                                                    <td>Paused</td>
                                                @endif
                                                <td> {{ $project->delivary_date }} </td>
                                                @if( $project->status == 0 )
                                                    <td>
                                                        Initial Project
                                                    </td>
                                                    <td class="center">Initial Project</td>
                                                @elseif( $project->status == 2 )    
                                                    <td>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Finish</span>
                                                            <span class="pull-right">100%</span>
                                                        </div>

                                                        <div class="progress progress-mini">
                                                            <div style="width:100%" class="progress-bar"></div>
                                                        </div>
                                                    </td>
                                                    <td class="finishing-tasks"><span class="done-tasks">{{ $finished }}</span> / <span class="total-tasks">{{ $unfinished }}</span></td>
                                                @elseif( $project->status == 1 || $project->status == 3 )
                                                    @if( $delivary_date < $now )
                                                        <td>
                                                            <div class="clearfix">
                                                                <span class="pull-left">Delayed</span>
                                                                <span class="pull-right">{{ ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) - 100 }} %</span>
                                                            </div>

                                                            <div class="progress progress-mini">
                                                                <div style="width:{{ ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%" class="progress-bar progress-bar-danger"></div>
                                                            </div>
                                                        </td>
                                                    @elseif( $delivary_date > $now )
                                                        <td>
                                                            <div class="clearfix">
                                                                <span class="pull-left">In Progress</span>
                                                                <span class="pull-right">{{  ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%</span>
                                                            </div>

                                                            <div class="progress progress-mini">
                                                                <div style="width:{{  ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%" class="progress-bar progress-bar-success"></div>
                                                            </div>
                                                        </td>
                                                    @endif
                                                    <td class="finishing-tasks"><span class="done-tasks">{{ $finished }}</span> / <span class="total-tasks">{{ $unfinished }}</span></td>
                                                @endif

                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->
                
                <!-- END Main Content -->
                
               

                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
            <!-- END Content -->

@endsection

@section('adminCustomScript')
    <script type="text/javascript" charset="utf-8" async defer>
            $('#projectSide').addClass('active');
    </script>

    <!--page specific plugin scripts-->
    <script src="{!! asset('assets/data-tables/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/data-tables/bootstrap3/dataTables.bootstrap.js') !!}"></script>

@endsection
