@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
	<div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Project Status</h1>
                        <h4>View all information for Project</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{url('/')}}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Project info</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if ( Session::has('extendSuccess') )
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> {{ Session::get('extendSuccess')}}
                    </div>
                @endif
                @if ( Session::has('extendFail') )
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Error!</strong> {{ Session::get('extendFail')}}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-th-list"></i>Project info</h3>
                                <div class="box-tool">
                                    <a class="btn btn-primary" data-toggle="modal" style="color: white;" data-target="#modalExtend">Extend</a>
                                    @if($project->status == 1)
                                        <a href="{{url('admin_dashboard/project/pause/'.$project->id)}}" class="btn btn-primary" style="color: white;"  title="">Pause</a>
                                    @elseif($project->status == 3)
                                        <a href="{{url('admin_dashboard/project/resume/'.$project->id)}}" class="btn btn-primary" style="color: white;"  title="">Resume</a>
                                    @endif
                                    <a class="show-tooltip" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" href="#" title="Finish"><i class="fa fa-check green"></i></a>
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                             <div class="box-content">
                                <div class="project-box-content">
                                    <br/>
                                    <h4>{{ $project->name }}</h4>
                                    <dl>
                                        <dt>Contract Date</dt>
                                        <dd> {{ $project->contract_date }}</dd>
                                        <br/>
                                        <dt>Delivery Date</dt>
                                        <dd> {{ $project->delivary_date }} </dd>
                                        <br/>
                                        <dt>Contract period</dt>
                                        <dd> {{ $project->contract_period }} </dd>
                                        <br/>
                                        <dt>Departments</dt>
                                        <dd>
                                            @foreach ($project->departments as $department)
                                                {{ $department->name }} - 
                                            @endforeach 
                                            
                                        </dd>
                                        <br/>
                                        <dt>Sub Projects</dt>
                                        <dd>
                                            @if(!$miniprojects->isEmpty())
                                                @foreach ($miniprojects as $miniproject)
                                                    {{ $miniproject->name }} - 
                                                @endforeach 
                                            @else
                                                <h4 style="color: red;"> There is no sub projects yet </h4>
                                            @endif
                                        </dd>
                                        <br/>
                                        <dt>Status</dt>
                                        <dd>
                                            @if($project->status == 0)
                                                Initial
                                            @elseif($project->status == 1)
                                                In Progress
                                            @elseif($project->status == 2)
                                                Delayed
                                            @elseif($project->status == 3)
                                                Paused     
                                            @endif
                                        </dd>
                                        <br/>
                                        <dt>Files</dt>
                                        <?php $documents = $project->documents()->get(); ?>
                                        @foreach($documents as $document)
                                            <?php   
                                                    $name = explode("_", $document->url);
                                             ?>
                                            <dd><a href="{{ url("/admin_dashboard/API/getDocument/project/$document->url", $args = []) }}"><i class="fa fa-file"></i>{{$name[1]}}</a></dd>
                                        @endforeach
                                        <br/>
                                        <dt>More info</dt>
                                        <dd> {{$project->description }} </dd>
                                        <br/>
                                        <dt>Number of Pauses</dt>
                                        <dd> {{count($project->pauses)}} </dd>
                                        @foreach($project->pauses as $pause)
                                            <hr>
                                            <dd>From :  {{$pause->start}} </dd>
                                            <dd>To :  {{$pause->end}} </dd>
                                            <dd>Number Of Hours :  {{$pause->number_of_hours}} </dd>
                                            
                                        @endforeach
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        
        
        <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalExtend" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal" action="{{url('admin_dashboard/project/'.$project->id.'/extend')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                               <label class="col-sm-4 col-lg-3 control-label">Delivery Date</label>
                               <div class="col-sm-7 col-lg-6 controls">
                                  <div class="input-group date date-picker" data-date="Date()" data-date-format="dd-mm-yyyy" data-date-viewmode="days">
                                    <span class="input-group-addon" ><i  class="fa fa-calendar"></i></span>
                                    <input class="form-control" size="16" value="{!!$project->delivary_date !!}" type="text" name="delivery_date" required>
                                 </div>
                               </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-3 control-label">Contract Period</label>
                                <div class="col-sm-7 col-lg-6 controls">
                                <input type="number" min='0'  class="form-control" value="{!! $project->contract_period !!}" name="contract_period">
                                </div>
                                <label class=" control-label">Year(s)</label>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

@endsection

@section('adminCustomScript')

    <script type="text/javascript" charset="utf-8" async defer>
            $('#projectSide').addClass('active');

    </script>


    <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>

@endsection