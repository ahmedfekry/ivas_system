@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
<div id="main-content">
	@foreach($projecttasks as $task)
		<h1>Task Name: {{$task->name}}</h1>
		<h1>Project Name: {{$task->miniproject->DepartmentProject->project->name}}</h1>
	<hr>		
	@endforeach
</div>
@endsection
@section('managerCustomScript')
    <script type="text/javascript" charset="utf-8" async defer>
            $('#teamSide').addClass('active');
    </script>
    
@endsection