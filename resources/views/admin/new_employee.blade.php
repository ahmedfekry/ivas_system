@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
  
{{-- 
@foreach($departments as $department)
  <li> {{ $department->name }} </li>
@endforeach --}}
	<div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Add Employee</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/admin_dashboard/", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Add Employee</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if ( Session::has('failedCreateEmployee') )
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Failed!</strong> {{ Session::get('failedCreateEmployee')}}
                    </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row" id="new_employee">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> New Employee</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                 <form action=" {{ url("/admin_dashboard/employee/store") }} " method="POST" class="form-horizontal" >
                                 {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Email</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="email" type="email" value="{{ old('email') }}" placeholder="Email" class="form-control" name="email" required> 
                                       @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                       </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">First Name</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="First Name" value="{{ old('first_name') }}" class="form-control" name="first_name" required>
                                          @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                       </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">last Name</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="last Name" value="{{ old('last_name') }}" class="form-control" name="last_name" required>
                                          @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                          @endif
                                       </div>
                                    </div>
                                     
                                     <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Gender</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <label class="radio-inline">
                                              <input type="radio" name="gender" value="male" checked="" required> Male
                                          </label>
                                          <label class="radio-inline">
                                              <input type="radio" name="gender" value="female"> female
                                          </label> 
                                       </div>
                                    </div>
                                     <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Title</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="Job Title" value="{{ old('title') }}" class="form-control" name="title" required>
                                          @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                          @endif
                                       </div>
                                    </div>
                                     <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Mobile Number</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="Mobile Number" value="{{ old('phone_number') }}" class="form-control" name="phone_number" required>
                                       @if ($errors->has('phone_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                       </div>
                                      </div>

                                      <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Birthday</label>
                                       <div class="col-sm-4 col-lg-3 controls">
                                          <div class="input-group date date-picker" data-date="Date()" data-date-format="dd-mm-yyyy" data-date-viewmode="days">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control " size="16" value="{{ old('birthday') }}" type="text" value="12-09-2016" name="birthday" >
                                            @if ($errors->has('birthday'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('birthday') }}</strong>
                                              </span>
                                            @endif
                                         </div>
                                       </div>
                                    </div>
                                     <div class="form-group{{ $errors->has('hire_date') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Date of Hire</label>
                                       <div class="col-sm-4 col-lg-3 controls">
                                          <div class="input-group date date-picker" data-date="Date()" data-date-format="dd-mm-yyyy" data-date-viewmode="days">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control " size="16" value="{{ old('hire_date') }}" type="text" value="12-09-2016" name="hire_date" >
                                            @if ($errors->has('hire_date'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('hire_date') }}</strong>
                                              </span>
                                            @endif
                                         </div>
                                       </div>
                                    </div>
                                    
                                    <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Role</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <select class="form-control chosen" value="{{ old('role') }}" data-placeholder="Choose a Role" tabindex="1" name="role" id="empRole"  required>
                                            <option value=""> </option>
                                            <option value="admin">Admin</option>
                                            <option value="manager">Manager</option>
                                            <option value="employee">Employee</option>
                                         </select>
                                         @if ($errors->has('role'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('role') }}</strong>
                                            </span>
                                          @endif
                                      </div>
                                   </div>

                                    <div id="departmentDiv" class="form-group hidden {{ $errors->has('department') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Department</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <select class="form-control chosen " value="{{ old('department') }}" data-placeholder="Choose a Department" tabindex="1" name="department" id="selectDepartment" required>
                                            <option value=""> </option>
                                            @foreach($departments as $department)
                                              {{-- <li> {{ $department->name }} </li> --}}
                                              <option value="{{ $department->id }}">{{ $department->name }}</option>
                                            @endforeach
                                         </select>
                                         @if ($errors->has('department'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('department') }}</strong>
                                            </span>
                                          @endif
                                      </div>
                                   </div>
                                  <div id="subdepartmentDiv" class="form-group hidden">
                                      <label class="col-sm-3 col-lg-2 control-label">Sub Department</label>
                                      <div class="col-sm-9 col-lg-10 controls" id="selectSubDepartment" >
                                         
                                      </div>
                                   </div>

                                   <div id="supervisorDiv" class="form-group{{ $errors->has('supervisor') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Supervisor</label>
                                      <div class="col-sm-9 col-lg-10">
                                         <select class="form-control " name="supervisor"  id="selectSupervisor" required>
                                            
                                         </select>
                                         @if ($errors->has('supervisor'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('supervisor') }}</strong>
                                            </span>
                                          @endif
                                      </div>
                                   </div>

                                   <div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">About</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <textarea class="form-control" rows="3" value="{{ old('about') }}" name="about"></textarea>
                                         @if ($errors->has('about'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('about') }}</strong>
                                            </span>
                                          @endif
                                      </div>
                                   </div>

                                   <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
                                     <label class="col-sm-3 col-lg-2 control-label">Salary</label>
                                     <div class="col-sm-9 col-lg-10 controls">
                                        <input type="text"  class="form-control" value="{{ old('salary') }}" name="salary" required>
                                        @if ($errors->has('salary'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('salary') }}</strong>
                                            </span>
                                          @endif
                                     </div>
                                    </div>

                                   <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                     <label class="col-sm-3 col-lg-2 control-label">Address</label>
                                     <div class="col-sm-9 col-lg-10 controls">
                                        <input type="text"  class="form-control" value="{{ old('address') }}" name="address" id="address" value="" autocomplete="off">
                                        @if ($errors->has('address'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('address') }}</strong>
                                          </span>
                                        @endif
                                     </div>
                                    </div>

                                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Password Strength</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <input type="password" class="form-control"  id="password" name="password" value="" autocomplete="off" required/>
                                         @if ($errors->has('password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                        @endif
                                         <span class="help-inline">Password should have capital letter and numbers and symbols</span>
                                         <div class="pwindicator" id="pwindicator-block">
                                            <div class="bar"></div>
                                            <div class="label"></div>
                                         </div>
                                      </div>
                                   </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                           <button type="button" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
            
        <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->

@endsection

@section('adminCustomScript')
        <script type="text/javascript" charset="utf-8" async defer>
            $('#employeeSide').addClass('active');

            $('#empRole').change(function() {
                var role = $('#empRole').val();
                if (role == 'manager' || role == 'admin') {
                  if (role == 'manager') {
                    $('#departmentDiv').removeClass('hidden');
                    $('#subdepartmentDiv').removeClass('hidden');
                    $("#selectDepartment").prop('required', true);

                    $('#selectSubDepartment').html('');
                    $('#selectSubDepartment').append('<label class="radio-inline">\
                                          <input type="radio" name="selectSubDepartments" value="Manager" required disabled checked />Manager </label>' );
                  } else {
                    $('#departmentDiv').addClass('hidden');
                    $("#selectDepartment").prop('required', false);
                    $('#subdepartmentDiv').addClass('hidden');
                    $('#selectSubDepartment').html('');

                  }

                  $('#supervisorDiv').removeClass('hidden');

                  url = '../API/getAdmins';
                  $.ajax({
                      dataType:'json',
                      type: 'GET',
                      url: url,
                      success: function(data) {
                        if (!data.users) {
                            alert('not found');
                        }else{
                            console.log(data.users);
                            $('#selectSupervisor').html('');
                            for (var i = 0; i <= data.users.length - 1 ; i++) {
                              $('#selectSupervisor').append($('<option>', {
                                  value: data.users[i].id,
                                  text: data.users[i].first_name+" "+data.users[i].last_name
                              }));
                              
                            }
                        }
                      }
                  });
               }else{
                  $('#departmentDiv').removeClass('hidden');
                  $('#subdepartmentDiv').addClass('hidden');
                  $("#selectDepartment").prop('required', true);
               }

            });

            $('#selectDepartment').change(function() {
                /* Act on the event */
                // alert('s');
                $('#subdepartmentDiv').removeClass('hidden')
                var role = $('#empRole').val();
                if (role == 'employee') {
                  $('#selectSubDepartment').html('');
                  var department = $('#selectDepartment').val();
                  var url = '../API/getSubDepartments/'+department;
                  $.ajax({
                      dataType:'json',
                      type: 'GET',
                      url: url,
                      success: function(data) {
                        if (!data.subDepartment) {
                            $('#selectSubDepartment').append('<h4 style="color: red;"> We can not find a sub departments </h4>');
                        }else{
                            var sub = data.subDepartment.split('/');
                            for (var i = 0; i <= sub.length - 1 ; i++) {
                              $('#selectSubDepartment').append('<label class="radio-inline">\
                                      <input type="radio" name="selectSubDepartments" value="'+sub[i]+'" />'+sub[i]+'\
                                   </label>' );
                            }
                        }
                      }
                  });
                  $('#supervisorDiv').removeClass('hidden');
                  url = '../API/getUsers/'+department;
                  $.ajax({
                      dataType:'json',
                      type: 'GET',
                      url: url,
                      success: function(data) {
                        if (!data.users) {
                            alert('not found');
                        }else{
                            console.log(data.users);
                            $('#selectSupervisor').html('');                            
                            for (var i = 0; i <= data.users.length - 1 ; i++) {
                              $('#selectSupervisor').append($('<option>', {
                                  value: data.users[i].id,
                                  text: data.users[i].first_name+" "+data.users[i].last_name
                              }));
                              
                            }
                        }
                      }
                  }); 
              }
            });
        </script>
        <!--page specific plugin scripts-->        
        <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-inputmask/bootstrap-inputmask.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-tags-input/jquery.tagsinput.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-pwstrength/jquery.pwstrength.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js') !!}"></script>
        <script src="{!! asset('assets/dropzone/downloads/dropzone.min.j') !!}s"></script>
        <script src="{!! asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
        <script src="{!! asset('assets/clockface/js/clockface.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}"></script> 
        <script src="{!! asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}"></script>
        <script src="{!! asset('assets/ckeditor/ckeditor.js') !!}"></script> 

@endsection