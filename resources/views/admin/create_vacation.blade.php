@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
	 <div id="main-content">
	<div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Create New Vacation </h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/admin_dashboard/", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">New Vacation</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
               
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> New Vacation</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                 <form action=" {{ url("/admin_dashboard/vacation/store") }} " method="POST"  class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    
                                     <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Date</label>
                                       <div class="col-sm-4 col-lg-3 controls">
                                          <div class="input-group date date-picker" data-date="Date()" data-date-format="yyyy-mm-dd" data-date-viewmode="days">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control " value="{{ old('date') }}" size="16" type="text" name="date" required>
                                         </div>
                                       </div>
                                       @if ($errors->has('date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label"> Reason </label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input class="form-control" rows="3" value="{{ old('reason') }}" name="reason" required>
                                       </div>
                                       @if ($errors->has('reason'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('reason') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                           <button type="button" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
         <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->
                
                <!-- END Main Content -->
                

                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
		</div>

    
@endsection

@section('adminCustomScript')
    <script type="text/javascript" charset="utf-8" async defer>
            $('#vacationSide').addClass('active');
    </script>
    <!--page specific plugin scripts-->
    <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-inputmask/bootstrap-inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-tags-input/jquery.tagsinput.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-pwstrength/jquery.pwstrength.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js') !!}"></script>
    <script src="{!! asset('assets/dropzone/downloads/dropzone.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
    <script src="{!! asset('assets/clockface/js/clockface.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}"></script> 
    <script src="{!! asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}"></script>
    <script src="{!! asset('assets/ckeditor/ckeditor.js') !!}"></script> 
@endsection