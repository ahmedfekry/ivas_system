@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
	
            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-user"></i> Profile</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/admin_dashboard/", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Profile</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Profile Info</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-md-3">
                                        {{-- <img class="img-responsive img-thumbnail" src="img/demo/profile-picture.jpg" alt="profile picture" /> --}}
                                        @if(Auth::user()->profile_picture == '')
                                            {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                        @else
                                            {{HTML::image( Auth::user()->profile_picture , ' Panny\'s Avatar', ['class' => 'img-responsive img-thumbnail'])}}
                                        @endif
                                        <br/><br/>
                                    </div>
                                    <div class="col-md-9 user-profile-info">
                                        <p><span>Email:</span> {{ $user->email }} </p>
                                        <p><span>First Name:</span> {{ $user->first_name }}</p>
                                        <p><span>Last Name:</span> {{ $user->last_name }} </p>
                                        <p><span>Birthday:</span> {{ $user->birthday }} </p>
                                        <p><span>Date of Hire:</span> {{ $user->hire_date }} </p>
                                        <p><span>Gender:</span> {{ $user->gender }}</p>
                                        <p><span>address:</span> {{ $user->address }}</p>
                                        <p><span>Mobile Number:</span> {{ $user->phone_number }} </p>
                                        <p><span>Title:</span> {{ $user->title }}</p>
                                        <p><span>About:</span> {{ $user->about }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- sheet emplyee salay -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-table"></i> Salary Sheet</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="btn-toolbar pull-right">
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Refresh" href="#"><i class="fa fa-repeat"></i></a>
                                    </div>
                                </div>
                                <br/><br/>
<div class="table-responsive">
    <table class="table table-advance">
        <thead>
            <tr>
                <th><a href="#">Date</a></th>
                <th><a href="#">Start Time</a></th>
                <th><a href="#">End Time</a></th>
                <th><a href="#">OverTime Hrs</a></th>
                <th><a href="#">Regular Hrs</a></th>
                <th><a href="#">Absence</a></th>
                <th><a href="#">Total </a></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>1/9/2016</td>
                <td>8:50</td>
                <td>5:10</td>
                <td>2:00</td>
                <td>8:20</td>
                <td>0</td>
                <td> </td>
            </tr>
            <tr>
                <td>Salary</td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td>1000 L.E </td>
            </tr>
            
        </tbody>
    </table>
</div>

                                <p class="text-right">
                                    1-12 of 46
                                    <a class="btn btn-circle disabled" href="#"><i class="fa fa-angle-left"></i></a>
                                    <a class="btn btn-circle" href="#"><i class="fa fa-angle-right"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //sheet emplyee salay -->

                @if ( Session::has('Success_update') )                
                {{-- <h1><i class="fa fa-list"></i>  </h1> --}}
                    <div class="alert alert-success alert-dismissible" id="edit_profile">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{ Session::get('Success_update')}}
                    </div>
                @elseif( Session::has('no_update') )
                    <div class="alert alert-info alert-dismissible" id="edit_profile">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Info!</strong> {{ Session::get('no_update')}}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Edit Profile</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="{{url('/admin_dashboard/profile/edit')}}" method="POST" class="form-horizontal">
                                 {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">First Name</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="{{ $user->first_name }}" class="form-control" name="first_name" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Last Name</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="{{ $user->last_name }}" class="form-control" name="last_name" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Birthday</label>
                                        <div class="col-sm-9 col-lg-10 controls" >
                                            <input class="date-picker form-control" data-date-format="yyyy-mm-dd"  size="16" type="text" value="{{ $user->birthday }}" name="birthday" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Gender</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="male" checked /> Male
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="female" /> Female
                                            </label> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Occupation</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="{{ $user->title }}" class="form-control" name="title" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Mobile Number</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="{{ $user->phone_number }}" class="form-control" name="phone_number" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Email</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="{{ $user->email }}" class="form-control" name="email" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">About</label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <textarea class="form-control" rows="6" name="about" >{{ $user->about }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn">Cancel</button>
                                        </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="edit_image">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Edit Profile Picture</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            @if(Session::has('success_image'))
                                
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Success!</strong> {{ Session::get('success_image')}}
                                </div>

                            @elseif(Session::has('failed_image'))
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Failed!</strong> {{ Session::get('failed_image')}}
                                </div>
                            @endif
                            <div class="box-content"> 
                            <form action="{{ url("/admin_dashboard/profile/image_upload") }}" method="POST"  class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                      <label class="col-sm-3 col-md-4 control-label">Image Upload</label>
                                      <div class="col-sm-9 col-md-8 controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                               {{-- <img src="" alt="" /> --}}
                                               @if(Auth::user()->profile_picture == '')
                                                    {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                                @else
                                                    {{HTML::image( Auth::user()->profile_picture , ' Panny\'s Avatar', ['class' => 'img-responsive img-thumbnail'])}}
                                                @endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                               <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" accept="image/*" class="default" name="profile_picture" /></span>
                                               <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                         </div>
                                         <span class="label label-important">NOTE!</span>
                                         <span>Attached image img-thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only</span>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn">Cancel</button>
                                        </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6" id="editPassword">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Change Password</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            @if(Session::has('success'))
                                
                                <div class="alert alert-success alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Success!</strong> {{ Session::get('success')}}
                                </div>

                            @elseif(Session::has('failed'))
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Fialed!</strong> {{ Session::get('failed')}}
                                </div>
                            @endif
                            <div class="box-content" id="editPassword">
                                <form action=" {{ url("/admin_dashboard/profile/update_password") }} " method="POST" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-sm-4 col-md-5 control-label">Current password</label>
                                        <div class="col-sm-8 col-md-7 controls">
                                            <input type="password" class="form-control" name="old_password" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-md-5 control-label">New password</label>
                                        <div class="col-sm-8 col-md-7 controls">
                                            <input type="password" class="form-control" name="new_password" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-md-5 control-label">Re-type new password</label>
                                        <div class="col-sm-8 col-md-7 controls">
                                            <input type="password" class="form-control" name="confirm_password" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-5">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn">Cancel</button>
                                        </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->
 
                @if(count($errors))
                    <div class="alert alert-danger" >
                    <button class="close" data-dismiss="alert">×</button>
                    <h4>Error!</h4> 
                  @foreach($errors->all() as $error)
                 
                      <p>{{$error}}</p> 

                  @endforeach
                   </div>
                @endif

            </div>
            <!-- END Content -->
            <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- END Container -->

@endsection


@section('adminCustomScript')
        
         <script type="text/javascript" charset="utf-8" async defer>
                $('#profileSide').addClass('active');
        </script>
        <!--page specific plugin scripts-->
        <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
        <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-ui/jquery-ui.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-knob/jquery.knob.js') !!}"></script>
@endsection

