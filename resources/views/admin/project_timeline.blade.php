@extends('layouts.admin')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('adminContent')
	

            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Project Status</h1>
                        <h4>View all information for Project</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Timeline</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-area-chart"></i> {{$project->name}} </h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="timeline">
                                    <li>
                                        <span class="tl-icon"><i class="fa fa-check-square-o"></i></span>
                                        <span class="tl-time">10:30</span>
                                        <span class="tl-title">Deliver Project</span>
                                        <span class="tl-arrow"></span>
                                        <div class="tl-content">
                                            <p class="tl-date tl-date-small">
                                                <span class="tl-numb-day">05</span>
                                                <span class="tl-text-day">Thursday</span>
                                                <span class="tl-month">September 2016</span>
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span class="tl-icon"><i class="fa fa-eye"></i></span>
                                        <span class="tl-time">18:24</span>
                                        <span class="tl-title">Test Project</span>
                                        <span class="tl-arrow"></span>
                                        <div class="tl-content">
                                            <p class="tl-date">
                                                <span class="tl-numb-day">03</span>
                                                <span class="tl-text-day">Tuesday</span>
                                                <span class="tl-month">September 2016</span>
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span class="tl-icon"><i class="fa fa-code-fork"></i></span>
                                        <span class="tl-time">21:00</span>
                                        <span class="tl-title">BackEnd Devlopment</span>
                                        <span class="tl-arrow"></span>
                                        <div class="tl-content">
                                            <p class="tl-date">
                                                <span class="tl-numb-day">01</span>
                                                <span class="tl-text-day">Sunday</span>
                                                <span class="tl-month">September 2016</span>
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </li>

                                    <li class="project-pause">
                                        <span class="tl-icon"><i class="fa fa-code"></i></span>
                                        <span class="tl-time">15:45</span>
                                        <span class="tl-title">FrontEnd Devlopment</span>
                                        <span class="tl-arrow"></span>
                                        <div class="tl-content">
                                            <p class="tl-date">
                                                <span class="tl-numb-day">21</span>
                                                <span class="tl-text-day">Wednesday</span>
                                                <span class="tl-month">August 2016</span>
                                            </p>
                                            <p>
                                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span class="tl-icon"><i class="fa fa-magic"></i></span>
                                        <span class="tl-time">12:35</span>
                                        <span class="tl-title">Project analysis & UI/UX Design</span>
                                        <span class="tl-arrow"></span>
                                        <div class="tl-content">
                                            <p>
                                                Ut enim ad minim veniam, quis nostrud exercitation
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span class="tl-icon"><i class="fa fa-check-circle"></i></span>
                                        <span class="tl-time">19:15</span>
                                        <span class="tl-title">Contract project.</span>
                                        <span class="tl-arrow"></span>
                                        <div class="tl-content">
                                            <p class="tl-date">
                                                <span class="tl-numb-day">14</span>
                                                <span class="tl-text-day">Wednesday</span>
                                                <span class="tl-month">August 2016</span>
                                            </p>
                                            <table class="table fill-head">
                                                <tbody>
                                                    <tr>
                                                        <td>Name of Project</td>
                                                        <td>Medhat Saleh Website</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Delivery date</td>
                                                        <td>20/9/2016</td>
                                                    </tr>
                                                    <tr>
                                                        <td>More</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic .</td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </li>

                                    <li class="clearfix"></li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar1.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}
                                    {{HTML::image('img/demo/avatar/avatar3.jpg') }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Fixed Chat -->
                
                
            </div>
            
                          <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        <script type="text/javascript" charset="utf-8" async defer>
                $('#projectSide').addClass('active');
        </script>
        
            
            
            
            
           
@endsection