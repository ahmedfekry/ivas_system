<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('pageTitle')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ivas') }}</title>

    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">

    <!--page specific css styles-->

    <!--flaty css styles-->
    <link rel="stylesheet" href="{{asset('css/flaty.css')}}">
    <link rel="stylesheet" href="{{asset('css/flaty-responsive.css')}}">

    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">

    <link href="{{asset('/css/app.css')}}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="login-page">
   
        @yield('content')

    <!-- Scripts -->
    <script src="{{asset('/js/app.js')}}"></script>
</body>
</html>
