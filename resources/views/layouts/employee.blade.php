<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        {{-- <title>Ivas System - Home</title> --}}
        @yield('pageTitle')
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->



        {{ HTML::style('assets/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('assets/font-awesome/css/font-awesome.min.css') }}

        {{ HTML::style('assets/chosen-bootstrap/chosen.min.css') }}
        {{ HTML::style('assets/bootstrap-datepicker/css/datepicker.css') }}
        {{ HTML::style('assets/bootstrap-fileupload/bootstrap-fileupload.css') }}
        {{ HTML::style('assets/jquery-ui/jquery-ui.min.css') }}

        {{ HTML::style('assets/jquery-ui/jquery-ui.min.css') }}

        {{ HTML::style('css/flaty.css') }}
        {{ HTML::style('css/flaty-responsive.css') }}
        {{ HTML::style('css/style.css') }}
        <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
        
        {{-- <link rel="shortcut icon" href="img/favicon.png"> --}}
    </head>
<body>
        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <small>
                    <i class="fa fa-desktop"></i>
                    IVAS System
                </small>
            </a>

            <!-- BEGIN Navbar Buttons -->
            <ul class="nav flaty-nav pull-right">
                <!-- BEGIN Button Tasks -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-tasks"></i>
                        <span class="badge badge-warning"></span>
                    </a>

                    <!-- BEGIN Tasks Dropdown -->
                    {{-- <ul class="dropdown-navbar dropdown-menu">
                        <li class="nav-header">
                            <i class="fa fa-check"></i>
                            4 Tasks to complete
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Software Update</span>
                                    <span class="pull-right">75%</span>
                                </div>

                                <div class="progress progress-mini">
                                    <div style="width:75%" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Transfer To New Server</span>
                                    <span class="pull-right">45%</span>
                                </div>

                                <div class="progress progress-mini">
                                    <div style="width:45%" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Bug Fixes</span>
                                    <span class="pull-right">20%</span>
                                </div>

                                <div class="progress progress-mini">
                                    <div style="width:20%" class="progress-bar"></div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <div class="clearfix">
                                    <span class="pull-left">Writing Documentation</span>
                                    <span class="pull-right">85%</span>
                                </div>

                                <div class="progress progress-mini progress-striped active">
                                    <div style="width:85%" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>

                        <li class="more">
                            <a href="#">See tasks with details</a>
                        </li>
                    </ul> --}}
                    <!-- END Tasks Dropdown -->
                </li>
                <!-- END Button Tasks -->

                <!-- BEGIN Button Notifications -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-important"></span>
                    </a>

                    <!-- BEGIN Notifications Dropdown -->
                   {{--  <ul class="dropdown-navbar dropdown-menu">
                        <li class="nav-header">
                            <i class="fa fa-warning"></i>
                            5 Notifications
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-comment orange"></i>
                                <p>New Comments</p>
                                <span class="badge badge-warning">4</span>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-twitter blue"></i>
                                <p>New Twitter followers</p>
                                <span class="badge badge-info">7</span>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                        @if(Auth::user()->profile_picture == '')
                            {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                        @else
                            {{HTML::image( Auth::user()->profile_picture , ' Panny\'s Avatar', ['class' => 'nav-user-photo'])}}
                        @endif
                                <p>David would like to become moderator.</p>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-bug pink"></i>
                                <p>New bug in program!</p>
                            </a>
                        </li>

                        <li class="notify">
                            <a href="#">
                                <i class="fa fa-shopping-cart green"></i>
                                <p>You have some new orders</p>
                                <span class="badge badge-success">+10</span>
                            </a>
                        </li>

                        <li class="more">
                            <a href="#">See all notifications</a>
                        </li>
                    </ul> --}}
                    <!-- END Notifications Dropdown -->
                </li>
                <!-- END Button Notifications -->

                <!-- BEGIN Button Messages -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-envelope"></i>
                        <span class="badge badge-success"></span>
                    </a>

                    <!-- BEGIN Messages Dropdown -->
                    {{-- <ul class="dropdown-navbar dropdown-menu">
                        <li class="nav-header">
                            <i class="fa fa-comments"></i>
                            3 Messages
                        </li>

                        <li class="msg">
                            <a href="#">
                                @if(Auth::user()->profile_picture == '')
                                    {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                @else
                                    {{HTML::image( Auth::user()->profile_picture , ' Panny\'s Avatar', ['class' => 'nav-user-photo'])}}
                                @endif
                                    
                                <div>
                                    <span class="msg-title">Sarah</span>
                                    <span class="msg-time">
                                        <i class="fa fa-clock-o"></i>
                                        <span>a moment ago</span>
                                    </span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </a>
                        </li>

                        <li class="msg">
                            <a href="#">

                                    {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }}
                                <div>
                                    <span class="msg-title">Emma</span>
                                    <span class="msg-time">
                                        <i class="fa fa-clock-o"></i>
                                        <span>2 Days ago</span>
                                    </span>
                                </div>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ...</p>
                            </a>
                        </li>

                        <li class="msg">
                            <a href="#">

                                    {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }}
                                <div>
                                    <span class="msg-title">John</span>
                                    <span class="msg-time">
                                        <i class="fa fa-clock-o"></i>
                                        <span>8:24 PM</span>
                                    </span>
                                </div>
                                <p>Duis aute irure dolor in reprehenderit in ...</p>
                            </a>
                        </li>

                        <li class="more">
                            <a href="#">See all messages</a>
                        </li>
                    </ul> --}}
                    <!-- END Notifications Dropdown -->
                </li>
                <!-- END Button Messages -->

                <!-- BEGIN Button User -->
                <li class="user-profile">
                    <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        {{-- <img class="nav-user-photo" src="img/demo/avatar/avatar2.jpg" alt="Penny's Photo" /> --}}
                        @if(Auth::user()->profile_picture == '')
                            {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'nav-user-photo']) }}
                        @else
                            {{HTML::image( Auth::user()->profile_picture , ' Panny\'s Avatar', ['class' => 'nav-user-photo'])}}
                        @endif
                        {{-- {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }} --}}
                        <span id="user_info">
                            {{ Auth::user()->first_name }}
                        </span>
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <!-- BEGIN User Dropdown -->
                    <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                        <li class="nav-header">
                            <i class="fa fa-clock-o"></i>
                            Logined From 20:45
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-cog"></i>
                                Account Settings
                            </a>
                        </li>

                        <li>
                            <a href="{{ url("/employee_dashboard/profile", $args = []) }}">
                                <i class="fa fa-user"></i>
                                Edit Profile
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-question"></i>
                                Help
                            </a>
                        </li>

                        <li class="divider visible-xs"></li>

                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-tasks"></i>
                                Tasks
                                <span class="badge badge-warning">4</span>
                            </a>
                        </li>
                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-bell"></i>
                                Notifications
                                <span class="badge badge-important">8</span>
                            </a>
                        </li>
                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                Messages
                                <span class="badge badge-success">5</span>
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                    <!-- BEGIN User Dropdown -->
                </li>
                <!-- END Button User -->
            </ul>
            <!-- END Navbar Buttons -->
        </div>
        <!-- END Navbar -->

        <!-- BEGIN Container -->
        <div class="container" id="main-container">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="navbar-collapse collapse">
                <!-- BEGIN Navlist -->
                <ul class="nav nav-list">
                    <!-- BEGIN Search Form -->
                    <li>
                        <form target="#" method="GET" class="search-form">
                            <span class="search-pan">
                                <button type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input type="text" name="search" placeholder="Search ..." autocomplete="off" id="searchInput" />
                            </span>
                        </form>
                    </li>
                    <div class="list-group" style="padding: 0px 5px 0px 5px;" id="searchGroup">
                     
                    </div>
                    <!-- END Search Form -->
                    <li id="dashboardSide">
                        <a href="{{ url("/employee_dashboard/") }}">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li id="profileSide">
                        <a href=" {{ url("/employee_dashboard/profile") }} ">
                            <i class="fa fa-user"></i>
                            <span>Profile</span>
                        </a>
                    </li>

                    <li id="taskSide">
                        <a href=" {{ url("/employee_dashboard/tasks", $args = []) }} " class="dropdown-toggle">
                            <i class="fa fa-th"></i>
                            <span>Tasks</span>
                        </a>
                    </li>

                    <li id="messagesSide">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-edit"></i>
                            <span>Messages</span>
                            <b class="arrow fa fa-angle-right"></b>
                        </a>

                        <!-- BEGIN Submenu -->
                        <ul class="submenu">
                            <li><a href="#">All Messages</a></li>
                            <li><a href="#">New Message</a></li>
                        </ul>
                        <!-- END Submenu -->
                    </li>

                    <li id="projectSide">
                        <a href=" {{ url("/employee_dashboard/projects", $args = []) }} " class="dropdown-toggle">
                            <i class="fa fa-list"></i>
                            <span>Projects</span>
                        </a>
                    </li>

                    <li id="teamSide">
                        <a href="{{ url("/employee_dashboard/department/team") }}" class="dropdown-toggle">
                            <i class="fa fa-globe"></i>
                            <span>Team</span>
                        </a>
                    </li>

                    <li id="calenderSide">
                        <a href="#">
                            <i class="fa fa-calendar"></i>
                            <span>Calendar</span>
                        </a>
                    </li>

                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-lg">
                    <i class="fa fa-angle-double-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->
             <!-- BEGIN Content -->
            <div id="main-content">


            	@yield('employeeContent')
               
                
                <!-- Fixed Chat -->
                <div class="row">
                    <div class="box fixed-chat">
                        <div class="box-title">
                            <h3><i class="fa fa-comments"></i> Chat</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="messages messages-chat messages-stripped messages-zigzag slimScroll" style="height: 250px; overflow: hidden; width: auto;">
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}

                                    {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 minutes ago</span>
                                        </div>
                                        <p>hey Sarah</p>
                                        <p>how R U?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}

                                    {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 1 minutes ago</span>
                                        </div>
                                        <p>Hi Penny</p>
                                        <p>Thanks, how are you ?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar1.jpg" alt=""> --}}

                                    {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }}
                                    <div>
                                        <div>
                                            <h5>Penny</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 47 seconds ago</span>
                                        </div>
                                        <p>ey, I'm good</p>
                                        <p>what's up?</p>
                                        <p>what's your plan for dinner?</p>
                                    </div>
                                </li>
                                <li>
                                    {{-- <img src="img/demo/avatar/avatar3.jpg" alt=""> --}}

                                    {{ HTML::image(Auth::user()->profile_picture, Auth::user()->first_name, ['class' => 'nav-user-photo']) }}
                                    <div>
                                        <div>
                                            <h5>Sarah</h5>
                                            <span class="time"><i class="fa fa-clock-o"></i> 12 seconds ago</span>
                                        </div>
                                        <p>Not much</p>
                                        <p>I haven't any plan, why ?</p>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                            <div class="messages-input-form">
                                <form method="POST" action="#">
                                    <div class="input">
                                        <input type="text" name="text" placeholder="Write here..." class="form-control">
                                    </div>
                                    <div class="buttons">
                                        <a class="show-tooltip" href="#" title="" data-original-title="Take Picture"><i class="fa fa-camera"></i></a>
                                        <a class="show-tooltip" href="#" title="" data-original-title="Attach File"><i class="fa fa-paperclip"></i></a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fixed Chat -->
                
                <!-- END Main Content -->


                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->

        <!--basic scripts-->
        <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        <script src="{!! asset('assets/bootstrap/js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-slimscroll/jquery.slimscroll.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-cookie/jquery.cookie.js') !!}"></script>

        @yield('employeeCustomScript')
        <!--flaty scripts-->
        <script src="{!! asset('js/flaty.js') !!}"></script>
        <script src="{!! asset('js/flaty-demo-codes.js') !!}"></script>
        <script>
            $('#searchInput').keyup(function () {
                if ($(this).val()!='') { 
                    var pathname = window.location.pathname;
                    // console.log(pathname);
                    var res = pathname.split("/");
                    // console.log(res.length);
                    var url = '';
                    // if (true) {}
                    if (res.length == 2 ) {
                        var url = '';
                    }else if(res.length == 3){
                        var url = '../';
                    }else if (res.length == 4) {
                        var url = '../../';
                    }
                    $.ajax({
                            url: url+'employee_dashboard/search/'+$(this).val(),
                            type: 'get',
                            success: function (data) {
                                // console.log(data);
                                    $('#searchGroup').empty();
                                if (data.users.length > 0) {
                                    $('#searchGroup').append('\
                                        <a href="#" class="list-group-item list-group-item-action active">\
                                            <h5 class="list-group-item-heading">USERS</h5>\
                                        </a>');
                                    for (var i = 0; i < data.users.length; i++) {
                                        $('#searchGroup').append('\
                                            <a href="'+url+'employee_dashboard/employee/'+data.users[i].id+'" class="list-group-item list-group-item-action ">\
                                                <h6 class="list-group-item-heading">'+data.users[i].first_name+" "+data.users[i].last_name+'</h6>\
                                            </a>');
                                    }
                                }

                                if (data.projects.length > 0) {
                                    $('#searchGroup').append('\
                                        <a href="#" class="list-group-item list-group-item-action active">\
                                            <h5 class="list-group-item-heading">PROJECTS</h5>\
                                        </a>');
                                    for (var i = 0; i < data.projects.length; i++) {
                                         $('#searchGroup').append('\
                                                <a href="'+url+'employee_dashboard/project/'+data.projects[i].id+'" class="list-group-item list-group-item-action ">\
                                                    <h6 class="list-group-item-heading">'+data.projects[i].name+'</h6>\
                                                </a>');
                                    }
                                }

                                // if (data.tasks.length > 0) {
                                //     $('#searchGroup').append('\
                                //         <a href="#" class="list-group-item list-group-item-action active">\
                                //             <h5 class="list-group-item-heading">TASKS</h5>\
                                //         </a>');
                                //     for (var i = 0; i < data.tasks.length; i++) {
                                //          $('#searchGroup').append('\
                                //                 <a href="#" class="list-group-item list-group-item-action ">\
                                //                     <h6 class="list-group-item-heading">'+data.tasks[i].name+'</h6>\
                                //                 </a>');
                                //     }
                                // }

                            }
                        });
                    
                  }else{
                    $('#searchGroup').empty();
                  }
            });
        </script>
</body>
</html>