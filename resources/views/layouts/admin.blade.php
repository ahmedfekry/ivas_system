<!DOCTYPE html>
<html>
<head>
	{{-- <title>Admin view </title> --}}
	 <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        {{-- <title>Ivas System - Add Employee</title> --}}
        @yield('pageTitle')
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        
        {{-- {{ HTML::style('assets/css/bootstrap-datepicker.css') }} --}}
        {{ HTML::style('assets/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('assets/font-awesome/css/font-awesome.min.css') }}
        {{ HTML::style('assets/chosen-bootstrap/chosen.min.css') }}
        {{ HTML::style('assets/jquery-tags-input/jquery.tagsinput.css') }}
        {{ HTML::style('assets/jquery-pwstrength/jquery.pwstrength.css') }}
        {{ HTML::style('assets/bootstrap-fileupload/bootstrap-fileupload.css') }}
        {{ HTML::style('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.css') }}
        {{ HTML::style('assets/dropzone/downloads/css/dropzone.css') }}
        {{ HTML::style('assets/bootstrap-colorpicker/css/colorpicker.css') }}
        {{ HTML::style('assets/bootstrap-timepicker/compiled/timepicker.css') }}
        {{ HTML::style('assets/clockface/css/clockface.css') }}
        {{ HTML::style('assets/bootstrap-datepicker/css/datepicker.css') }}
        {{ HTML::style('assets/bootstrap-daterangepicker/daterangepicker.css') }}
        {{ HTML::style('assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css') }}
        {{ HTML::style('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}
        {{ HTML::style('assets/jquery-ui/jquery-ui.min.css') }}
        {{ HTML::style('assets/jquery-ui/jquery-ui.min.css') }}
 
	    {{ HTML::style('css/flaty.css') }}
        {{ HTML::style('css/flaty-responsive.css') }}
        {{ HTML::style('css/style.css') }}

        <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
        

</head>
<body class="admin">
          <div id="navbar" class="navbar navbar-fixed">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href=" {{url('/admin_dashboard/') }}">
                <small>
                    <i class="fa fa-desktop"></i>
                    IVAS System
                </small>
            </a>

            <!-- BEGIN Navbar Buttons -->
            <ul class="nav flaty-nav pull-right">
                <!-- BEGIN Button Tasks -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-tasks"></i>
                        <span class="badge badge-warning"></span>
                    </a>
                </li>
                <!-- END Button Tasks -->

                <!-- BEGIN Button Notifications -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-important"></span>
                    </a>

                </li>
                <!-- END Button Notifications -->

                <!-- BEGIN Button Messages -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-envelope"></i>
                        <span class="badge badge-success"></span>
                    </a>

     
                </li>
                <!-- END Button Messages -->

                <!-- BEGIN Button User -->
                <li class="user-profile">
                    <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        {{-- <img class="nav-user-photo" src="img/demo/avatar/avatar2.jpg" alt="Penny's Photo" /> --}}
                        @if(Auth::user()->profile_picture == '')
                            {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class' => 'nav-user-photo']) }}
                        @else
                            {{HTML::image( Auth::user()->profile_picture , ' Panny\'s Avatar', ['class' => 'nav-user-photo'])}}
                        @endif
                        
                                
                        <span id="user_info">
                            {{ Auth::user()->first_name }}
                        </span>
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <!-- BEGIN User Dropdown -->
                    <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                        <li class="nav-header">
                            <i class="fa fa-clock-o"></i>
                            Logined From 20:45
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-cog"></i>
                                Account Settings
                            </a>
                        </li>

                        <li>
                            <a href="{{ url("/admin_dashboard/profile", $args = []) }}">
                                <i class="fa fa-user"></i>
                                Edit Profile
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-question"></i>
                                Help
                            </a>
                        </li>

                        <li class="divider visible-xs"></li>

                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-tasks"></i>
                                Tasks
                                <span class="badge badge-warning">4</span>
                            </a>
                        </li>
                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-bell"></i>
                                Notifications
                                <span class="badge badge-important">8</span>
                            </a>
                        </li>
                        <li class="visible-xs">
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                Messages
                                <span class="badge badge-success">5</span>
                            </a>
                        </li>

                        <li class="divider"></li>

            
                        <li>
                            <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
             
                    </ul>
                    <!-- BEGIN User Dropdown -->
                </li>
                <!-- END Button User -->
            </ul>
            <!-- END Navbar Buttons -->
        </div>
        <!-- END Navbar -->
               <!-- BEGIN Container -->
        <div class="container" id="main-container">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="navbar-collapse collapse sidebar-fixed">
               <!-- BEGIN Navlist -->

                <ul class="nav nav-list">
                    <!-- BEGIN Search Form -->
                    <li >
                        <form target="#" method="GET" class="search-form">
                            <span class="search-pan">
                                <button type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input type="text" name="search" placeholder="Search ..." autocomplete="off" id="searchInput" />
                            </span>
                        </form>
                     
                    </li>
                    <div class="list-group" style="padding: 0px 5px 0px 5px;" id="searchGroup">
                     
                    </div>
                    <!-- END Search Form -->
                    <li id="dashboardSide">
                        <a href=" {{ url('/admin_dashboard/') }} ">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li id="profileSide">
                        <a href=" {{ url('/admin_dashboard/profile') }} ">
                            <i class="fa fa-user"></i>
                            <span>Profile</span>
                        </a>
                    </li>

                     <li id="departmentSide">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-group"></i>
                            <span>Department</span>
                            <b class="arrow fa fa-angle-right"></b>
                        </a>

                        <!-- BEGIN Submenu -->
                        <ul class="submenu">
                            <li><a href="{{ url('/admin_dashboard/departments') }}">All Departments</a></li>
                            <li class="active"><a href="{{ url('/admin_dashboard/department/new') }} ">Add Department</a></li>
                        </ul>
                        <!-- END Submenu -->
                    </li>

                    <li id="employeeSide">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-group"></i>
                            <span>Employee</span>
                            <b class="arrow fa fa-angle-right"></b>
                        </a>

                        <!-- BEGIN Submenu -->
                        <ul class="submenu">
                            <li><a href="{{ url('/admin_dashboard/employees') }}">All Employees</a></li>
                            <li class="active"><a href="{{ url('/admin_dashboard/employee/new') }} ">Add Employee</a></li>
                        </ul>
                        <!-- END Submenu -->
                    </li>
                    
                    <li id="messageSide">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-edit"></i>
                            <span>Messages</span>
                            <b class="arrow fa fa-angle-right"></b>
                        </a>

                        <!-- BEGIN Submenu -->
                        <ul class="submenu">
                            <li><a href="#">All Messages</a></li>
                            <li><a href="#">New Message</a></li>
                        </ul>
                        <!-- END Submenu -->
                    </li>

                    <li id="projectSide">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-list"></i>
                            <span>Projects</span>
                            <b class="arrow fa fa-angle-right"></b>
                        </a>

                        <!-- BEGIN Submenu -->
                        <ul class="submenu">
                            <li><a href=" {{ url('/admin_dashboard/projects') }} ">All Projects</a></li>
                            <li><a href="{{ url('/admin_dashboard/project/new') }}">Create Project</a></li>
                            {{-- <li><a href="Admin_project_Timeline.html">Project Timeline</a></li> --}}
                        </ul>
                        <!-- END Submenu -->
                    </li>

                     <li id="vacationSide">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-list"></i>
                            <span>Vacations</span>
                            <b class="arrow fa fa-angle-right"></b>
                        </a>

                        <!-- BEGIN Submenu -->
                        <ul class="submenu">
                            <li><a href=" {{ url('/admin_dashboard/vacations') }} ">All Vacations</a></li>
                            <li><a href="{{ url('/admin_dashboard/vacation/new') }}">Create Vacation</a></li>
                            {{-- <li><a href="Admin_project_Timeline.html">Project Timeline</a></li> --}}
                        </ul>
                        <!-- END Submenu -->
                    </li>

                    <li id="calenderSide">
                        <a href="#">
                            <i class="fa fa-calendar"></i>
                            <span>Calendar</span>
                        </a>
                    </li>

                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-lg">
                    <i class="fa fa-angle-double-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->
            
        
        @yield('adminContent')

        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class = 'AjaxisModal'>
            </div>
        </div>
        <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        <script src="{!! asset('assets/bootstrap/js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-slimscroll/jquery.slimscroll.min.js') !!}"></script>
        <script src="{!! asset('assets/jquery-cookie/jquery.cookie.js') !!}"></script>
        
        <script src = "{{URL::asset('js/AjaxisBootstrap.js') }}"></script>
        @yield('adminCustomScript')

        <!--flaty scripts-->
        <script src="{!! asset('js/flaty.js') !!}"></script>
        <script src="{!! asset('js/flaty-demo-codes.js') !!}"></script>

        <script>
            $('#searchInput').keyup(function () {
                if ($(this).val()!='') { 
                    var pathname = window.location.pathname;
                    // console.log(pathname);
                    var res = pathname.split("/");
                    console.log(res.length);
                    var url = '';
                    // if (true) {}
                    var url = {{ base_path()}};
                    // if (res.length == 2 ) {
                    //     var url = '';
                    // }else if(res.length == 3){
                    //     var url = '../';
                    // }else if (res.length == 4) {
                    //     var url = '../../';
                    // }
                    $.ajax({
                            url: url+'admin_dashboard/search/'+$(this).val(),
                            type: 'get',
                            success: function (data) {
                                // console.log(data);
                                    $('#searchGroup').empty();
                                if (data.users.length > 0) {
                                    $('#searchGroup').append('\
                                        <a href="#" class="list-group-item list-group-item-action active">\
                                            <h5 class="list-group-item-heading">USERS</h5>\
                                        </a>');
                                    for (var i = 0; i < data.users.length; i++) {
                                        $('#searchGroup').append('\
                                            <a href="'+url+'admin_dashboard/employee/'+data.users[i].id+'" class="list-group-item list-group-item-action ">\
                                                <h6 class="list-group-item-heading">'+data.users[i].first_name+" "+data.users[i].last_name+'</h6>\
                                            </a>');
                                    }
                                }

                                if (data.projects.length > 0) {
                                    $('#searchGroup').append('\
                                        <a href="#" class="list-group-item list-group-item-action active">\
                                            <h5 class="list-group-item-heading">PROJECTS</h5>\
                                        </a>');
                                    for (var i = 0; i < data.projects.length; i++) {
                                         $('#searchGroup').append('\
                                                <a href="'+url+'admin_dashboard/project/'+data.projects[i].id+'" class="list-group-item list-group-item-action ">\
                                                    <h6 class="list-group-item-heading">'+data.projects[i].name+'</h6>\
                                                </a>');
                                    }
                                }

                                // if (data.tasks.length > 0) {
                                //     $('#searchGroup').append('\
                                //         <a href="#" class="list-group-item list-group-item-action active">\
                                //             <h5 class="list-group-item-heading">TASKS</h5>\
                                //         </a>');
                                //     for (var i = 0; i < data.tasks.length; i++) {
                                //          $('#searchGroup').append('\
                                //                 <a href="#" class="list-group-item list-group-item-action ">\
                                //                     <h6 class="list-group-item-heading">'+data.tasks[i].name+'</h6>\
                                //                 </a>');
                                //     }
                                // }

                            }
                        });
                    
                  }else{
                    $('#searchGroup').empty();
                  }
            });
        </script>
</body>
</html>

