@extends('layouts.employee')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('employeeContent')

                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Project Status</h1>
                        <h4>View all information for Project</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Project info</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-th-list"></i>Project info</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                             <div class="box-content">
                                <div class="project-box-content">
                                    <br/>
                                    <h4>{{ $project->name }}</h4>
                                    <dl>
                                        <dt>Contract Date</dt>
                                        <dd> {{ $project->contract_date }}</dd>
                                        <br/>
                                        <dt>Delivery Date</dt>
                                        <dd> {{ $project->delivary_date }} </dd>
                                        <br/>
                                        <dt>Contract period</dt>
                                        <dd> {{ $project->contract_period }} </dd>
                                        <br/>
                                        <dt>Departments</dt>
                                        <dd>
                                            @foreach ($project->departments as $department)
                                                {{ $department->name }} - 
                                            @endforeach 
                                            
                                        </dd>
                                        <br/>
                                        <dt>Sub Projects</dt>
                                        <dd>
                                            @if(!$miniprojects->isEmpty())
                                                @foreach ($miniprojects as $miniproject)
                                                    {{ $miniproject->name }} - 
                                                @endforeach 
                                            @else
                                                <h4 style="color: red;"> You need to assign a sub projects to that project </h4>
                                            @endif
                                        </dd>
                                        <br/>
                                        <dt>Files</dt>
                                        <?php $documents = $project->documents()->get(); ?>
                                        @foreach($documents as $document)
                                            <?php   
                                                    $name = explode("_", $document->url);
                                             ?>
                                        <dd><a href="{{ url("/employee_dashboard/API/getDocument/project/$document->url", $args = []) }}"><i class="fa fa-file"></i>{{$name[1]}}</a></dd>
                                        @endforeach
                                        <br/>
                                        <dt>More info</dt>
                                        <dd> {{$project->description }} </dd>
                                        
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                {{-- <div class="row">
                    <!-- Projects Status-->
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Project Status</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div id="chart_div" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- //Projects Status-->
                </div>
                --}}
@endsection

@section('employeeCustomScript')
@endsection