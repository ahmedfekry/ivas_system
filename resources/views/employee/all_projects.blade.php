@extends('layouts.employee')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('employeeContent')
	{{-- 
	<script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        <script src="{!! asset('assets/bootstrap/js/bootstrap.min.js') !!}"></script>
         --}}
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-list"></i> All Project</h1>
                        <h4>View all Projects Status</h4>
                    </div>
                </div>
                <!-- END Page Title -->
                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">All Project</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-list"></i> Projects List</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <br/><br/>
                                <div class="clearfix"></div>
                                <div class="table-responsive" style="border:0">
                                    <table class="table table-advance" id="table1">
                                        <thead>
                                            <tr>
                                                <th style="width:18px"><input type="checkbox" /></th>
                                                <th>Name</th>
                                                <th>Contract Date</th>
                                                <th>Delivery Date</th>
                                                <th>Finishing Time</th>
                                                <th>Finishing Tasks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach( $projects as $project )
                                                 
                                                <?php 
                                                    $delivary_date = new Carbon\Carbon($project->delivary_date);
                                                    $start_date = new Carbon\Carbon($project->start_date);
                                                    $now = Carbon\Carbon::now();
                                                ?>

                                                <?php  $mini =  App\DepartmentProject::where([['project_id','=',$project->pivot->project_id],['department_id','=',$project->pivot->department_id]])->first(); ?>
	                                            @if( !$mini->miniprojects()->get()->isEmpty() )
                                                    <?php
                                                        $finished = 0;
                                                        $unfinished = 0;
                                                        if ($project->status != 0) {
                                                            $subs = $project->DepartmentProject()->get();
                                                            foreach ($subs as $sub) {
                                                                # code...
                                                                $minis = $sub->miniprojects()->get();
                                                                foreach ($minis as $minid) {
                                                                    # code...
                                                                    $tasks = $minid->tasks()->get();
                                                                    foreach ($tasks as $task) {
                                                                        # code...
                                                                       if ($task->status == 2) {
                                                                            $finished++;

                                                                        }
                                                                        
                                                                        $unfinished++;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                    @if($project->status == 2)
                                                        <tr>
                                                            <td><input type="checkbox" /></td>
                                                            <td><a href=" {{ url("/employee_dashboard/project/".$project->id) }} "> {{ $project->name }} </a></td>
                                                            <td> {{ $project->contract_date }} </td>
                                                            <td> {{ $project->delivary_date }} </td>
                                                            <td>
                                                                <div class="clearfix">
                                                                    <span class="pull-left">Finish</span>
                                                                    <span class="pull-right">100%</span>
                                                                </div>

                                                                <div class="progress progress-mini">
                                                                    <div style="width:100%" class="progress-bar"></div>
                                                                </div>
                                                            </td>
                                                            <td class="finishing-tasks"><span class="done-tasks">{{$finished}}</span> / <span class="total-tasks">{{$unfinished}}</span></td>
                                                        </tr>
                                                    @else
                                                        @if($delivary_date >= $now)
        		                                     		<tr>
        		                                                <td><input type="checkbox" /></td>
        		                                                <td><a href=" {{ url("/employee_dashboard/project/".$project->id) }} "> {{ $project->name }} </a></td>
        		                                                <td> {{ $project->contract_date }} </td>
        		                                                <td> {{ $project->delivary_date }} </td>
        		                                                <td>
        		                                                    <div class="clearfix">
        		                                                        <span class="pull-left">In Progress</span>
        		                                                        <span class="pull-right">{{  ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%</span>
        		                                                    </div>

        		                                                    <div class="progress progress-mini">
        		                                                        <div style="width:{{  ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%" class="progress-bar progress-bar-success"></div>
        		                                                    </div>
        		                                                </td>
                                                                <td class="finishing-tasks"><span class="done-tasks">{{$finished}}</span> / <span class="total-tasks">{{$unfinished}}</span></td>
        		                                            </tr>
                                                        @elseif($delivary_date < $now)
                                                            <tr>
                                                                <td><input type="checkbox" /></td>
                                                                <td><a href=" {{ url("/employee_dashboard/project/".$project->id) }} "> {{ $project->name }} </a></td>
                                                                <td> {{ $project->contract_date }} </td>
                                                                <td> {{ $project->delivary_date }} </td>
                                                                <td>
                                                                    <div class="clearfix">
                                                                        <span class="pull-left">Delay</span>
                                                                        <span class="pull-right">{{ ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%</span>
                                                                    </div>

                                                                    <div class="progress progress-mini">
                                                                        <div style="width:{{ ceil(($now->diffInDays($start_date) / $delivary_date->diffInDays($start_date)) * 100) }}%" class="progress-bar progress-bar-danger"></div>
                                                                    </div>
                                                                </td>
                                                                <td class="finishing-tasks"><span class="done-tasks">{{$finished}}</span> / <span class="total-tasks">{{$unfinished}}</span></td>
                                                            </tr>
                                                        @endif
    	                                            @endif
                                                @endif
                                            @endforeach
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                                
                                <!-- Modal -->
                       
                                
                            </div>
                        </div>
                    </div>
                </div>
 

@endsection

@section('employeeCustomScript')
    
        <script type="text/javascript" charset="utf-8" async defer>
                $('#projectSide').addClass('active');
        </script>

        <!--page specific plugin scripts-->
        <script src="{!! asset('assets/data-tables/jquery.dataTables.js') !!}"></script>
        <script src="{!! asset('assets/data-tables/bootstrap3/dataTables.bootstrap.js') !!}"></script>
@endsection
