@extends('layouts.employee')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('employeeContent')
 <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Dashboard</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                        </li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
<!--
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Weekly Changes</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="weekly-changes">
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-up light-green"></i>
                                            <span class="light-green">5</span>
                                            New Tasks
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-minus light-blue"></i>
                                            <span class="light-blue">53</span>
                                            finished Tasks
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-down light-red"></i>
                                            <span class="light-red">17</span>
                                            Absence
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-up light-green"></i>
                                            <span class="light-green">75</span>
                                            items
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-down light-red"></i>
                                            <span class="light-red">74</span>
                                            items
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
-->
                    <div class="col-md-5">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Tasks Status</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="circle-stats-item blue">
                                    <i class="fa fa-bars"></i>
                                    <span class="percent">%</span>
                                    <input value="23" data-fgcolor="#87ceeb" data-min="0" data-min="100" />
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-7">
                        <a href="#" class="tile col-md-12">
                            <p class="title">Check All Messages</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <div class="img img-bottom">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                        </a>
                        <a href="#" class="col-md-12 tile tile-magenta">
                            <div class="img img-center">
                                <i class="fa fa-desktop"></i>
                            </div>
                            <p class="title text-center">Tasks</p>
                        </a>
                    </div>
                    
                    
                </div>        
                 @if ( Session::has('todoSuccess') )                
                {{-- <h1><i class="fa fa-list"></i>  </h1> --}}
                    <div class="alert alert-success alert-dismissible col-md-7">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{ Session::get('todoSuccess')}}
                    </div>
                @endif
                @if(count($errors))
                    <div class="alert alert-danger col-md-7">
                    <button class="close" data-dismiss="alert">×</button>
                    <h4>Error!</h4> 
                  @foreach($errors->all() as $error)
                 
                      <p>{{$error}}</p> 

                  @endforeach
                   </div>
                @endif
                <div class="row">
                    <div class="col-md-7">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-check"></i> Todo List <a class="new_todo" href="#" data-toggle="modal" data-target="#newToDo"><i class="fa fa-plus"></i> plus</a></h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="newToDo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">New Todo</h4>
                                      </div>
                                      <div class="modal-body">
                                        <form action=" {{ url("/employee_dashboard/todo/new") }} " method="POST" class="form-horizontal">
                                         {{ csrf_field() }}
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Title</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                  <input class="form-control" type="text" placeholder="title" name="title" required>
                                                </div>
                                              </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 col-lg-2 control-label">Priority</label>
                                              <div class="col-sm-9 col-lg-10 controls">
                                                 <label class="radio-inline">
                                                    <input type="radio" name="priority" value="0" required checked /> low
                                                 </label>
                                                  <label class="radio-inline">
                                                    <input type="radio" name="priority" value="1" /> Medium
                                                 </label>
                                                 <label class="radio-inline">
                                                    <input type="radio" name="priority" value="2" /> High
                                                 </label> 
                                              </div>
                                           </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Project</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                  <select class="form-control"  name="project_id" required >
                                                        <option>Select Project ... </option>
                                                        @foreach($projects as $project)
                                                            @if($project->status == 1)
                                                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Regular Hrs</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                  <input class="form-control" type="number" step="0.1" placeholder="00.00" name="regular_hours" required>
                                                </div>
                                              </div>
                                            
                                            <div class="form-group submit-btn">
                                                  <input type="submit" name="submit" class="btn btn-primary " value="Submit">
                                              </div>


                                        </form>
                                      </div>
<!--
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        {{-- <button type="sub" class="btn btn-primary">Save changes</button> --}}
                                      </div>
-->
                                    </div>
                                  </div>
                                </div>
                                
                            </div>

                            <div class="box-content">
                                <ul class="todo-list">

                                    <?php $todos = $todos->sortBy('priority');?>
                                    @foreach($todos as $todo)
                                        <li>
                                            <div class="todo-desc">
                                                <p><a href="#" data-toggle="modal" data-target="#todo_myModal{{$todo->id}}"> {{ $todo->title }} </a></p>
                                            </div>
                                            <div class="todo-actions">
                                                @if( $todo->priority == 0 )
                                                    <span class="label label-success">low</span>
                                                @elseif($todo->priority == 1)
                                                    <span class="label label-warning">Medium</span>
                                                @elseif($todo->priority == 2)
                                                    <span class="label label-important">High</span>
                                                @else
                                                    <span class="label label-primary">Done</span>
                                                @endif

                                                @if($todo->priority != 3)
                                                    <a class="show-tooltip" href="{{ url("/employee_dashboard/todo/done/$todo->id", $args = []) }}" title="It's done" data-original-title="It's done"><i class="fa fa-check"></i></a>
                                                @endif
                                                {{-- <a class="show-tooltip" href="#" title="" data-original-title="Remove"><i class="fa fa-times"></i></a> --}}
                                            </div>
                                        </li>
                                    @endforeach
                                
                                    
                                  
                                </ul>
                                @foreach($todos as $todo)
                                <!-- Modal -->
                                    <div class="modal fade" id="todo_myModal{{$todo->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">ToDo info</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="todo-info">
                                                <p><span>Title:</span> {{$todo->title}} </p>
                                                {{-- <p><span>Priority:</span> low </p> --}}
                                                @if( $todo->priority == 0 )
                                                    <p><span class="label label-success">Priority:</span> low</p>
                                                @elseif($todo->priority == 1)
                                                    <p><span>Priority:</span> Medium</p>
                                                @elseif($todo->priority == 2)
                                                    <p><span>Priority:</span> High</p>
                                                @else
                                                    <p><span>Priority:</span> Done</p>
                                                @endif
                                                <p><span>project:</span> {{$todo->project->name}} </p>
                                                <p><span>Regular Hrs:</span> {{$todo->regular_hours}} </p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-tasks"></i> Tasks In Progress</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="tasks-in-progress">
                                    @foreach($tasks as $task)
                                        @if($task->status == 1)
                                            <?php 
                                                $end_date = new Carbon\Carbon($task->end_date);
                                                // echo $end_date . " - ";
                                                $start_date = new Carbon\Carbon($task->actual_start);
                                                // echo $start_date . " - ";
                                                $now = Carbon\Carbon::now();
                                            ?>
                                            @if($end_date >= $now)
                                                <li>
                                                    <p>
                                                        {{ $task->name }}
                                                        <span>{{  ceil(($now->diffInHours($start_date) / $end_date->diffInHours($start_date)) * 100) }}%</span>
                                                    </p>
                                                    <div class="progress progress-mini">
                                                        <div class="progress-bar progress-bar-success" style="width:{{  ceil(($now->diffInHours($start_date) / $end_date->diffInHours($start_date)) * 100) }}%"></div>
                                                    </div>
                                                </li>
                                            @elseif($end_date < $now)
                                                <li>
                                                    <p>
                                                        {{ $task->name }}
                                                        <span>{{  ceil(($now->diffInHours($start_date) / $end_date->diffInHours($start_date)) * 100) }}%</span>
                                                    </p>
                                                    <div class="progress progress-mini">
                                                        <div class="progress-bar progress-bar-danger" style="width:{{  ceil(($now->diffInHours($start_date) / $end_date->diffInHours($start_date)) * 100) }}%"></div>
                                                    </div>
                                                </li>
                                            @endif
                                        @endif
                                    @endforeach
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

@endsection

@section('employeeCustomScript')

    
    <script type="text/javascript" charset="utf-8" async defer>
            $('#dashboardSide').addClass('active');
    </script>

    <!--page specific plugin scripts-->
    <script src="{!! asset('assets/flot/jquery.flot.js') !!}"></script>
    <script src="{!! asset('assets/flot/jquery.flot.resize.js') !!}"></script>
    <script src="{!! asset('assets/flot/jquery.flot.pie.js') !!}"></script>
    <script src="{!! asset('assets/flot/jquery.flot.stack.js') !!}"></script>
    <script src="{!! asset('assets/flot/jquery.flot.crosshair.js') !!}"></script>
    <script src="{!! asset('assets/flot/jquery.flot.tooltip.min.js') !!}"></script>
    <script src="{!! asset('assets/sparkline/jquery.sparkline.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-knob/jquery.knob.js') !!}"></script>

@endsection
