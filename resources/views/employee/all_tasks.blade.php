@extends('layouts.employee')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('employeeContent')
	  <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> List of Tasks</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">List of Tasks</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if ( Session::has('pauseSuccess') )                
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{ Session::get('pauseSuccess')}}
                    </div>
                @endif

                @if ( Session::has('pauseFail') )
                    <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Failed!</strong> {{ Session::get('pauseFail')}}
                    </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> List of Tasks </h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                           
                                <div class="btn-toolbar">
                                    <div class="btn-group" >
                                        <select class="form-control" id="filter">
                                            <option>Search by Status ...</option>
                                            <option value="initial">Initial</option>}
                                            <option value="in progress">In Progress</option>}
                                            <option value="finished">Finished</option>}
                                            <option value="delayed">Delayed</option>}
                                            <option value="paused">Paused</option>}
                                            option
                                        </select>
                                    </div>
                                </div>
                                <ul class="messages slimScroll nav nav-tabs" role="tablist" id="tasklist" style="height: 300px">
                                    <?php $tasks = $tasks->sortBy('status'); ?>
                                	@foreach($tasks as $task)
	                                	@if( $task->status == 0 )
		                                    <li role="presentation" class="active" >
		                                        <div style="margin-left: auto;">
		                                            <div>
		                                                <h5><a href="#task{{ $task->id }}" aria-controls="task{{$task->id}}" role="tab" data-toggle="tab">{{ $task->name }}
		                                                    </a></h5>
		                                                <span class="time"><a href="#" data-toggle="tooltip" data-placement="bottom" title="start date : {{ $task->start_date }} , regular hours: {{ $task->regular_hours }}"><span class="badge badge-success">Up Coming <i class="fa fa-star"></i></span></a></span>
		                                            </div>
		                                            
		                                                <div class="clearfix">
		                                                    <span class="pull-left">Up Coming</span>
		                                                    <span class="pull-right">0%</span>
		                                                </div>

		                                                <div class="progress progress-mini">
		                                                    <div style="width:0%" class="progress-bar"></div>
		                                                </div>
		                                            
		                                            <div class="messages-actions">
		                                                <a class="show-tooltip" href=" {{ url("/employee_dashboard/task/$task->id/approve") }} " title="Approve"><i class="fa fa-check green"></i></a>
		                                                <a class="show-tooltip" href="#" title="Disapprove"><i class="fa fa-times orange"></i></a>
		                                                <a class="show-tooltip" href="#" title="Remove"><i class="fa fa-trash-o red"></i></a>
		                                            </div>
		                                        </div>
		                                    </li>
	                                    @elseif( $task->status == 1 )
                                            <?php 
                                                $end_date = new Carbon\Carbon($task->end_date);
                                                $start_date = new Carbon\Carbon($task->actual_start);
                                                $now = Carbon\Carbon::now('Africa/Cairo');
                                                // echo $now->toDateTimeString();
                                                $passed_working_hours = $task->get_working_hours($start_date,$now->toDateTimeString());
                                                $regular_hours = $task->regular_hours;
                                                $total_pause_time = 0;
                                                $pauses = $task->pauses;
                                                if(!$pauses->isEmpty()){
                                                    foreach ($pauses as $pause) {
                                                        $total_pause_time += $pause->number_of_hours;
                                                    }
                                                }
                                                // echo $passed_working_hours;
                                                $progress = round((($total_pause_time + $passed_working_hours)/( $total_pause_time + $regular_hours))*100);
                                            ?>
                                            @if($end_date >= $now)
    		                                    <li role="presentation">
    		                                        <div style="margin-left: auto;">
    		                                            <div>
    		                                                <h5><a href="#task{{$task->id}}" aria-controls="task{{$task->id}}" role="tab" data-toggle="tab">{{ $task->name }} 
    		                                                    </a></h5>
    		                                                <span class="time"><i class="fa fa-clock-o"></i> {{ $task->regular_hours }} </span>
    		                                            </div>
    		                                            <p>
    		                                                <div class="clearfix">
    		                                                    <span class="pull-left">In Progress</span>
    		                                                    <span class="pull-right">{{ $progress }}%</span>
    		                                                </div>

    		                                                <div class="progress progress-mini">
    		                                                    <div style="width:{{ $progress }}%" class="progress-bar progress-bar-success"></div>
    		                                                </div>
    		                                            </p>
    		                                        </div>
    		                                    </li>
                                            @elseif($end_date < $now)
                                                <li role="presentation">
                                                    <div style="margin-left: auto;">
                                                        <div>
                                                            <h5><a href="#task{{$task->id}}" aria-controls="task{{$task->id}}" role="tab" data-toggle="tab">{{ $task->name }} 
                                                                </a></h5>
                                                            <span class="time"><i class="fa fa-clock-o"></i> {{ $task->regular_hours }} </span>
                                                        </div>
                                                        <p>
                                                            <div class="clearfix">
                                                                <span class="pull-left">Delay</span>
                                                                <span class="pull-right">{{ $progress }}%</span>
                                                            </div>

                                                            <div class="progress progress-mini">
                                                                <div style="width:{{ $progress }}%" class="progress-bar progress-bar-danger"></div>
                                                            </div>
                                                        </p>
                                                    </div>
                                                </li>
                                            @endif
                                        @elseif( $task->status == 2 )
                                            <li role="presentation">
                                                <div style="margin-left: auto;">
                                                    <div>
                                                        <h5><a href="#task{{$task->id}}" aria-controls="task{{$task->id}}" role="tab" data-toggle="tab">{{ $task->name }} 
                                                            </a></h5>
                                                        <span class="time"><i class="fa fa-clock-o"></i> {{ $task->regular_hours }} </span>
                                                    </div>
                                                    <p>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Finished</span>
                                                            <span class="pull-right">100%</span>
                                                        </div>

                                                        <div class="progress progress-mini">
                                                            <div style="width:100%" class="progress-bar"></div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </li>
                                        @elseif( $task->status == 3 )
                                        <?php 
                                                $end_date = new Carbon\Carbon($task->end_date);
                                                $start_date = new Carbon\Carbon($task->actual_start);
                                                $now = Carbon\Carbon::now('Africa/Cairo');
                                                // echo $now->toDateTimeString();
                                                $passed_working_hours = $task->get_working_hours($start_date,$now->toDateTimeString());
                                                $regular_hours = $task->regular_hours;
                                                $total_pause_time = 0;
                                                $pauses = $task->pauses;
                                                if(!$pauses->isEmpty()){
                                                    foreach ($pauses as $pause) {
                                                        $total_pause_time += $pause->number_of_hours;
                                                    }
                                                }
                                                // echo $passed_working_hours;
                                                $progress = round((($total_pause_time + $passed_working_hours)/( $total_pause_time + $regular_hours))*100);
                                            ?>
                                            <li role="presentation">
                                                <div style="margin-left: auto;">
                                                    <div>
                                                        <h5><a href="#task{{$task->id}}" aria-controls="task{{$task->id}}" role="tab" data-toggle="tab">{{ $task->name }} 
                                                            </a></h5>
                                                        <span class="time"><i class="fa fa-clock-o"></i> {{ $task->regular_hours }} </span>
                                                    </div>
                                                    <p>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Paused</span>
                                                            <span class="pull-right">{{ $task->progress_pause }}%</span>
                                                        </div>

                                                        <div class="progress progress-mini">
                                                            <div style="width:{{ $task->progress_pause }}%" class="progress-bar"></div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </li>
	                                    @endif
                                    @endforeach
               
                                    
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> Task Info</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content tab-content">
                            <?php $tasks = $tasks->sortBy('status'); ?>
                            <?php $z=0; ?>
                                @foreach($tasks as $task)
                                    @if($z == 0)
                                        @if( $task->status == 1 )
                                            <div class="user-profile-info tab-pane active" role="tabpanel" id="task{{$task->id}}">
                                                <p><span>Task Title :</span> {{ $task->name }} </p>
                                                <p><span>Status :</span> This Task is in progress </p>
                                                <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                                <p><span>Start Date :</span> {{ $task->start_date  }} </p>
                                                <p><span>Actual Start Date :</span> {{ $task->actual_start  }} </p>
                                                <p><span>End Date :</span> {{ $task->end_date  }} </p>
                                                <p><span>About:</span> {{ $task->description }} </p>
                                                
                                                <?php $documents = $task->task_documents()->get(); ?>
                                                @foreach( $documents as $document)
                                                     <?php   
                                                            $name = explode("_", $document->url);
                                                     ?>
                                                    <p><span>Attach File :</span> <a href=" {{ url("/employee_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endforeach
                                                <div class="form-group finish-task">
                                                    <a type="submit" href="{{ url("/employee_dashboard/task/start_pause/".$task->id) }}" class="btn btn-primary">Pause</a>
                                               
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#submit_finish_task{{$task->id}}">Finish</button>
                                               </div>
                                            </div>
                                        @elseif($task->status == 3 )
                                            <div class="user-profile-info tab-pane active" role="tabpanel" id="task{{$task->id}}">
                                                <p><span>Task Title :</span> {{ $task->name }} </p>
                                                <p><span>Status :</span> This Task Paused </p>
                                                <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                                <p><span>Start Date :</span> {{ $task->start_date  }} </p>
                                                <p><span>Actual Start Date :</span> {{ $task->actual_start  }} </p>
                                                <p><span>End Date :</span> {{ $task->end_date  }} </p>
                                                <p><span>About:</span> {{ $task->description }} </p>
                                                
                                                <?php $documents = $task->task_documents()->get(); ?>
                                                @foreach( $documents as $document)
                                                     <?php   
                                                            $name = explode("_", $document->url);
                                                     ?>
                                                    <p><span>Attach File :</span> <a href=" {{ url("/employee_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endforeach
                                               <div class="form-group finish-task">
                                                    <a href="{{ url("/employee_dashboard/task/end_pause/".$task->id) }}" class="btn btn-primary">Resume</a>
                                               </div>
                                            </div>
                                        @else
                                            <div class="user-profile-info tab-pane active" role="tabpanel" id="task{{$task->id}}">
                                                <p><span>Task Title :</span> {{ $task->name }} </p>
                                                @if($task->status == 0)
                                                    <p><span>Status :</span> This Task Initial </p>
                                                @else
                                                    <p><span>Status :</span> This Task Finished </p>
                                                @endif
                                                <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                                <p><span>Start Date :</span> {{ $task->start_date  }} </p>
                                                <p><span>About:</span> {{ $task->description }} </p>
                                                
                                                <?php $documents = $task->task_documents()->get(); ?>
                                                @foreach( $documents as $document)
                                                     <?php   
                                                            $name = explode("_", $document->url);
                                                     ?>
                                                    <p><span>Attach File :</span> <a href=" {{ url("/employee_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endforeach

                                            </div>
                                        @endif
                                        <?php $z++; ?>
                                    @else   
                                        @if( $task->status == 1 )
                                            <div class="user-profile-info tab-pane" role="tabpanel" id="task{{$task->id}}">
                                                <p><span>Task Title :</span> {{ $task->name }} </p>
                                                <p><span>Status :</span> This Task is in progress </p>
                                                <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                                <p><span>Start Date :</span> {{ $task->start_date  }} </p>
                                                <p><span>Actual Start Date :</span> {{ $task->actual_start  }} </p>
                                                <p><span>End Date :</span> {{ $task->end_date  }} </p>
                                                <p><span>About:</span> {{ $task->description }} </p>
                                                {{-- <p><span>Attach File :</span> <a href="#" target="_blank"><i class="fa fa-file-o"></i> File name </a></p> --}}
                                                <?php $documents = $task->task_documents()->get(); ?>
                                                @foreach( $documents as $document)
                                                     <?php   
                                                            $name = explode("_", $document->url);
                                                     ?>
                                                    <p><span>Attach File :</span> <a href=" {{ url("/employee_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endforeach
                                                <div class="form-group finish-task">
                                                    <a type="submit" href="{{ url("/employee_dashboard/task/start_pause/".$task->id) }}" class="btn btn-primary">Pause</a>
                                                    
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#submit_finish_task{{$task->id}}">Finish</button>
                                               </div>
                                            </div>
                                        @elseif($task->status == 3)
                                             <div class="user-profile-info tab-pane" role="tabpanel" id="task{{$task->id}}">
                                                <p><span>Task Title :</span> {{ $task->name }} </p>
                                                <p><span>Status :</span> This Task Paused </p>
                                                <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                                <p><span>Start Date :</span> {{ $task->start_date  }} </p>
                                                <p><span>Actual Start Date :</span> {{ $task->actual_start  }} </p>
                                                <p><span>End Date :</span> {{ $task->end_date  }} </p>
                                                <p><span>About:</span> {{ $task->description }} </p>
                                                {{-- <p><span>Attach File :</span> <a href="#" target="_blank"><i class="fa fa-file-o"></i> File name </a></p> --}}
                                                <?php $documents = $task->task_documents()->get(); ?>
                                                @foreach( $documents as $document)
                                                     <?php   
                                                            $name = explode("_", $document->url);
                                                     ?>
                                                    <p><span>Attach File :</span> <a href=" {{ url("/employee_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endforeach
                                                <a href="{{ url("/employee_dashboard/task/end_pause/".$task->id) }}" class="btn btn-primary">Resume</a>
                                            </div>
                                        @else
                                            <div class="user-profile-info tab-pane" role="tabpanel" id="task{{$task->id}}">
                                                <p><span>Task Title :</span> {{ $task->name }} </p>
                                                @if($task->status == 0)
                                                    <p><span>Status :</span> This Task Initial </p>
                                                @else
                                                    <p><span>Status :</span> This Task Finished </p>
                                                @endif
                                                <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                                <p><span>Start Date :</span> {{ $task->start_date  }} </p>
                                                <p><span>About:</span> {{ $task->description }} </p>
                                                {{-- <p><span>Attach File :</span> <a href="#" target="_blank"><i class="fa fa-file-o"></i> File name </a></p> --}}
                                                <?php $documents = $task->task_documents()->get(); ?>
                                                @foreach( $documents as $document)
                                                     <?php   
                                                            $name = explode("_", $document->url);
                                                     ?>
                                                    <p><span>Attach File :</span> <a href=" {{ url("/employee_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endforeach
                                                
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                                <?php $tasks = $tasks->sortBy('status'); ?>
                                <?php $c=0; ?>
                                @foreach($tasks as $task)
                                    @if($c == 0)
                                        @if( $task->status == 1 )
                                            <div class="modal fade submit_finish_task" id="submit_finish_task{{$task->id}}" tabindex="-1" role="dialog">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Finish Task</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action=" {{ url("/employee_dashboard/task/$task->id/finish", $args = []) }} " method="POST" class="form-horizontal" autocomplete="off" enctype="multipart/form-data" >
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-lg-2 control-label">Add file</label>
                                                            <div class="col-sm-9 col-lg-10 controls">
                                                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileupload-new">Select file</span>
                                                                    <span class="fileupload-exists">Change</span>
                                                                    <input type="file" class="file-input" name="finishFileToUpload" />
                                                                </span>
                                                                <span class="fileupload-preview"></span>
                                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                                             </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-lg-2 control-label">URl</label>
                                                            <div class="col-sm-9 col-lg-10 controls">
                                                              <input class="form-control" type="text" placeholder="URl" name="finishUrl">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-lg-2 control-label">Info</label>
                                                            <div class="col-sm-9 col-lg-10 controls">
                                                              <textarea class="form-control" name="finishText" required></textarea>
                                                            </div>
                                                        </div>


                                                        <div class="form-group finish-task">
                                                          <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                                                        </div>
                                                    </form>
                                                  </div>
                                                </div><!-- /.modal-content -->
                                              </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                        @else
                                        @endif
                                        <?php $c++; ?>
                                    @else
                                        @if( $task->status == 1 )
                                            <div class="modal fade submit_finish_task" id="submit_finish_task{{$task->id}}" tabindex="-1" role="dialog">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Finish Task</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action=" {{ url("/employee_dashboard/task/$task->id/finish", $args = []) }} " method="POST" class="form-horizontal" autocomplete="off" enctype="multipart/form-data" >
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-lg-2 control-label">Add file</label>
                                                            <div class="col-sm-9 col-lg-10 controls">
                                                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileupload-new">Select file</span>
                                                                    <span class="fileupload-exists">Change</span>
                                                                    <input type="file" class="file-input" name="finishFileToUpload" />
                                                                </span>
                                                                <span class="fileupload-preview"></span>
                                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                                             </div>
                                                            </div>
                                                          </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-lg-2 control-label">URl</label>
                                                            <div class="col-sm-9 col-lg-10 controls">
                                                              <input class="form-control" type="text" placeholder="URl" name="finishUrl">
                                                            </div>
                                                          </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-lg-2 control-label">Info</label>
                                                            <div class="col-sm-9 col-lg-10 controls">
                                                              <textarea class="form-control" name="finishText" required></textarea>
                                                            </div>
                                                          </div>


                                                        <div class="form-group finish-task">
                                                              <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                                                          </div>
                                                    </form>
                                                  </div>
                                                </div><!-- /.modal-content -->
                                              </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                        @else
                                        @endif
                                    @endif
                                @endforeach

                                
                            </div>
                        </div>
                    </div>
                </div>

@endsection

@section('employeeCustomScript')
    <script>
         $('#filter').change(function () {
            var searchText = $(this).val();
            if (searchText != 'Search by Status ...') {
                searchText = $.trim(searchText).replace(/ +/g, ' ').toLowerCase();
                // var li = $('#tasklist > li');
                $('#tasklist > li').each(function(){
                    var currentLiText = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
                        showCurrentLi = currentLiText.indexOf(searchText) !== -1;
                    $(this).toggle(showCurrentLi);
                });
                
            }else{
                $('#tasklist > li').show();
            }

        });
    </script>
    <script type="text/javascript" charset="utf-8" async defer>
            $('#taskSide').addClass('active');
    </script>

    
@endsection
