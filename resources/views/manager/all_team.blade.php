@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')
		
		 <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> All Team</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/manager_dashboard", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">All Employees</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-group"></i>Employees</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-responsive nav nav-tabs all-team-tabs" role="tablist">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Position</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php $i = 1; ?>
                                        @foreach( $users as $user )
                                            @if( $i == 1)
    	                                        <tr role="presentation">
    	                                            <td>{{ $i++ }}</td>
    	                                            <td><a href="#nameOf{{$user->id}}" aria-controls="home" role="tab" data-toggle="tab" class="profile-tab-info"> {{ $user->first_name.' '.$user->last_name }} </a></td>
    	                                            <td> {{ $user->category }} </td>
    	                                            <td> {{ $user->title }} </td>
    	                                            <td><a href="mailto:abdulrahman.elsayed@ivas.com.eg"> {{ $user->email }} </a></td>
    	                                            <td><i class="fa fa-wechat"></i></td>
    	                                        </tr>
                                            @else
                                                 <tr role="presentation" >
                                                    <td>{{ $i++ }}</td>
                                                    <td><a href="#nameOf{{$user->id}}" aria-controls="home" role="tab" data-toggle="tab" class="profile-tab-info"> {{ $user->first_name.' '.$user->last_name }} </a></td>
                                                    <td> {{ $user->category }} </td>
                                                    <td> {{ $user->title }} </td>
                                                    <td><a href="mailto:abdulrahman.elsayed@ivas.com.eg"> {{ $user->email }} </a></td>
                                                </tr>
                                            @endif
	                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Profile Info</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content main_info tab-content">
								<?php $z = 1; ?>
                                @foreach( $users as $user )
	                                <div class="row tab-pane fade in {{($z++ == 1) ? 'active' : ''}}" role="tabpanel" id="nameOf{{$user->id}}">
	                                    <div class="col-md-3 profile_img">
                                            @if($user->profile_picture == '')

                                                {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                            @else
                                                  {{ HTML::image($user->profile_picture, 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                            @endif
	                                        <br/><br/>
	                                    </div>
	                                    <div class="col-md-9 user-profile-info">
	                                         <p><span>Email:</span> <a href="mailto:#"> {{ $user->email }}</a></p>
	                                        <p><span>First Name:</span> {{ $user->first_name }} </p>
	                                        <p><span>Last Name:</span> {{ $user->last_name }} </p>
	                                        <p><span>Birthday:</span> {{ $user->birthday }} </p>
	                                        <p><span>Date of Hire:</span> {{ $user->hire_date }} </p>
	                                        <p><span>Gender:</span> {{ $user->gender }} </p>
	                                        <p><span>Position:</span> {{ $user->title }} </p>
	                                        <p><span>Mobile Number:</span> {{ $user->phone_number }} </p>
	                                        <p><span>About:</span> {{ $user->about }} </p>
                                            @if($user->parent)
                                                <p><span>Supervisor:</span> <a href="#"> {{ $user->parent->first_name." ".$user->parent->last_name }} </a> </p>
                                            @endif
                                            @if(!$user->children()->get()->isEmpty())
                                                <p><span>Supervisee:</span>
                                                @foreach($user->children as $child)
                                                    <a href="#"> {{ $child->first_name." ".$child->last_name }} </a>  - 
                                                @endforeach
                                                </p>
                                            @endif
                                            @if(!$user->tasks()->get()->isEmpty())
                                                <?php   
                                                    $projects =  array();
                                                    $tasks = $user->tasks;
                                                    foreach ($tasks as $task) {
                                                        $name = $task->miniproject->DepartmentProject->project->name;
                                                        $project = $task->miniproject->DepartmentProject->project;
                                                        $projects[$name] = $project;
                                                    }

                                                ?>
                                                <p><span>Projects:</span>
                                                @foreach($projects as $project)
                                                    <h5><a href="{{url('manager_dashboard/project/'.$project->id.'/user/'.$user->id)}}" title="">{{$project->name}}</a></h5>
                                                @endforeach
                                            @endif
	                                    </div>
	                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
        <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        
        
	
@endsection


@section('managerCustomScript')
    <script type="text/javascript" charset="utf-8" async defer>
            $('#teamSide').addClass('active');
    </script>
    
@endsection