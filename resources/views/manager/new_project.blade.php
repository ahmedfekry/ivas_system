@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')
	 <div id="main-content">
	             <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Create New Project </h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/manager_dashboard", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">New Project</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                {{-- @if(count($errors))
                    <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">×</button>
                    <h4>Error!</h4> 
                  @foreach($errors->all() as $error)
                 
                      <p>{{$error}}</p> 

                  @endforeach
                   </div>
                @endif --}}
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> New Project</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                 <form action=" {{ url("/manager_dashboard/project/store", $args = []) }} " method="POST"  class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Name</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="Name of project" class="form-control" name="name" value="{{ old('name') }}" required>
                                          <span class="help-inline">Name of Project</span>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                       </div>
                                    </div>
                                     <div class="form-group{{ $errors->has('contract_date') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Contract Date</label>
                                       <div class="col-sm-4 col-lg-3 controls">
                                          <div class="input-group date date-picker" data-date="Date()" data-date-format="dd-mm-yyyy" data-date-viewmode="days">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control " size="16" type="text" name="contract_date" value="{{ old('contract_date') }}" required>
                                         </div>
                                       </div>
                                       @if ($errors->has('contract_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('contract_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                     <div class="form-group{{ $errors->has('delivery_date') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Delivery Date</label>
                                       <div class="col-sm-4 col-lg-3 controls">
                                          <div class="input-group date date-picker" data-date="Date()" data-date-format="dd-mm-yyyy" data-date-viewmode="days">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control " size="16" type="text" value="{{ old('delivery_date') }}" name="delivery_date" required>
                                         </div>
                                       </div>
                                        @if ($errors->has('delivery_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('delivery_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    

                                    <div class="form-group{{ $errors->has('contract_period') ? ' has-error' : '' }}">
                                     <label class="col-sm-3 col-lg-2 control-label">Contract Period</label>
                                     <div class="col-sm-4 col-lg-3 controls">
                                        <input type="number" min='0' class="form-control" value="{{ old('contract_period') }}" name="contract_period">
                                     </div>
                                     <label class=" control-label">Year(s)</label>
                                     @if ($errors->has('contract_period'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('contract_period') }}</strong>
                                          </span>
                                      @endif
                                    </div>
                                    
                                     <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Departments </label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                         @foreach($departments as $department)
                                            <label class="checkbox-inline">
                                              <input type="checkbox" name="department[]" value="{{ $department->id }}" required disabled checked> {{ $department->name }}
                                            </label>
                                         @endforeach
                                      </div>
                                    </div>
                          
                                     <div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Priority</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <label class="radio-inline">
                                            <input type="radio" name="priority" value="0" required  {{ (old('priority') == 0 ? "checked":"") }}/> low
                                         </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="priority" value="1" {{ (old('priority') == 1 ? "checked":"") }} /> Medium
                                         </label>
                                         <label class="radio-inline">
                                            <input type="radio" name="priority" value="2" {{ (old('priority') == 2 ? "checked":"") }} /> High
                                         </label> 
                                      @if ($errors->has('priority'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('priority') }}</strong>
                                          </span>
                                      @endif
                                      </div>
                                    </div>
                                    
                                     <div class="form-group{{ $errors->has('projectDocuments[]') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Add file</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="file-input" value="{{ old('projectDocuments') }}"  name="projectDocuments[]" multiple/>
                                            </span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="projectDocuments" style="float: none"></a>
                                         </div>
                                      </div>
                                      @if ($errors->has('projectDocuments'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('projectDocuments') }}</strong>
                                          </span>
                                      @endif
                                   </div>
                                     
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label"> more info </label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <textarea class="form-control" rows="3" value="{{ old('description') }}" name="description" required></textarea>
                                       </div>
                                       @if ($errors->has('description'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('description') }}</strong>
                                          </span>
                                      @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                           <button type="button" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
		</div>
                      <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        
@endsection

@section('managerCustomScript')
  
  <script type="text/javascript" charset="utf-8" async defer>
          $('#projectSide').addClass('active');
  </script>

  <!--page specific plugin scripts-->
  <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-inputmask/bootstrap-inputmask.min.js') !!}"></script>
  <script src="{!! asset('assets/jquery-tags-input/jquery.tagsinput.min.js') !!}"></script>
  <script src="{!! asset('assets/jquery-pwstrength/jquery.pwstrength.min.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js') !!}"></script>
  <script src="{!! asset('assets/dropzone/downloads/dropzone.min.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
  <script src="{!! asset('assets/clockface/js/clockface.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') !!}"></script>
  <script src="{!! asset('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}"></script> 
  <script src="{!! asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}"></script>
  <script src="{!! asset('assets/ckeditor/ckeditor.js') !!}"></script> 

@endsection