@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')
	 <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> List of Tasks</h1>
                    </div>
                </div>
                
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/manager_dashboard", $args = []) }}">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">List of Tasks</li>
                    </ul>
                </div>
                
                @if ( Session::has('success') )                
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{ Session::get('success')}}
                    </div>
                @endif
                
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> List of Tasks <a class="new_one" href=" {{ url("/manager_dashboard/task/new") }} " >new</a></h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content slimScroll-manger">
                                <div class="btn-toolbar pull-right">
                                    <div class="btn-group" style="">
                                        <select class="form-control" id="filter">
                                            <option>Search by Status ...</option>
                                            <option value="initial">Initial</option>}
                                            <option value="in progress">In Progress</option>}
                                            <option value="finished">Finished</option>}
                                            <option value="delayed">Delayed</option>}
                                            <option value="paused">Paused</option>}
                                            option
                                        </select>
                                    </div>
                                </div>

                                <div class="btn-toolbar pull-right" style="margin-right: 5px;">
                                    <div class="btn-group">
                                        <select class="form-control" id="selectSubDep">
                                            <option> Search by category ... </option>
                                            <?php 
                                                $department = Auth::user()->department()->first();
                                                $subs = explode('/', $department->sub_department);
                                            ?>
                                             @foreach($subs as $sub)
                                                <option value="{{ $sub }}"> {{  $sub }} </option>
                                             @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <input type="text" id="fekry" class="form-control" placeholder="search" />
                                    </div>
                                </div>
                                <br>
                                <ul class="messages slimScroll nav nav-tabs" role="tablist" id="tasklist" style="height: 600px">
                                        <?php $tasks = $tasks->sortByDesc('created_at');?>
                                        
                                    @foreach($tasks as  $task)
                                        <li role="presentation">
                                            {{-- <img src="img/demo/avatar/avatar2.jpg" alt=""> --}}
                                            <?php $user = $task->user; ?>
                                            @if( $user->profile_picture == '' )
                                                {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                            @else
                                                {{HTML::image( $user->profile_picture , "")}}
                                            @endif
                                            <div>
                                                <div>
                                                    <h5><a href="#task{{$task->id}}" aria-controls="task{{ $task->id }}" role="tab" data-toggle="tab"> {{$task->name}}
                                                    </a> TO
                                                            <a href="{{ url("/manager_dashboard/employee/$user->id", $args = []) }}"> {{ $user->first_name.' '.$user->last_name }} </a> - 
                                                            <h4 class="hidden"> {{ $user->category }} </h4>
                                                    </h5>
                                                    <span class="time"><i class="fa fa-clock-o"></i> {{ $task->regular_hours }} Hrs </span>
                                                </div>
                                                 <?php 
                                                    $end_date = new Carbon\Carbon($task->end_date);
                                                    $start_date = new Carbon\Carbon($task->actual_start);
                                                    $now = Carbon\Carbon::now('Africa/Cairo');
                                                    $passed_working_hours = $task->get_working_hours($start_date,$now->toDateTimeString());
                                                    $regular_hours = $task->regular_hours;
                                                    $total_pause_time = 0;
                                                    $pauses = $task->pauses;
                                                    if(!$pauses->isEmpty()){
                                                        foreach ($pauses as $pause) {
                                                            $total_pause_time += $pause->number_of_hours;
                                                        }
                                                    }
                                                    $progress = round((($total_pause_time + $passed_working_hours)/( $total_pause_time + $regular_hours))*100);
                                                ?>

                                                @if( $task->status == 0 )
                                                    <p>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Initial</span>
                                                            <span class="pull-right">0%</span>
                                                        </div>

                                                        <div class="progress progress-mini">
                                                            <div style="width:0%" class="progress-bar progress-bar-success"></div>
                                                        </div>
                                                    </p>
                                                @elseif( $task->status == 1 )
                                                    @if( $end_date >= $now )
                                                        <p>
                                                            <div class="clearfix">
                                                                <span class="pull-left">In Progress</span>
                                                                <span class="pull-right">{{ $progress }}%</span>
                                                            </div>

                                                            <div class="progress progress-mini">
                                                                <div style="width:{{ $progress }}%" class="progress-bar progress-bar-success"></div>
                                                            </div>
                                                        </p>
                                                    @elseif( $end_date < $now )
                                                        <p>
                                                            <div class="clearfix">
                                                                <span class="pull-left">Delayed</span>
                                                                <span class="pull-right">{{  $progress }}%</span>
                                                            </div>

                                                            <div class="progress progress-mini">
                                                                <div style="width:{{  $progress }}%" class="progress-bar progress-bar-danger"></div>
                                                            </div>
                                                        </p>
                                                    @endif
                                                @elseif( $task->status == 2 )
                                                    <p>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Finished</span>
                                                            <span class="pull-right">100%</span>
                                                        </div>

                                                        <div class="progress progress-mini">
                                                            <div style="width:100%" class="progress-bar"></div>
                                                        </div>
                                                    </p>
                                                @elseif( $task->status == 3)
                                                    <p>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Paused</span>
                                                            <span class="pull-right">{{ $task->progress_pause }}%</span>
                                                        </div>

                                                        <div class="progress progress-mini">
                                                            <div style="width:{{ $task->progress_pause }}%" class="progress-bar progress-bar-success"></div>
                                                        </div>
                                                    </p>
                                                @endif
                                                
                                            </div>
                                        </li>
                                    @endforeach  
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> Task Info</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content tab-content task-info-manger">
                            <?php $i = 0;?>
                                <?php $tasks = $tasks->sortByDesc('created_at');?>
                                @foreach($tasks as $task)
                                    <?php $user = $task->user;?>
                                        <div class="user-profile-info tab-pane {{ ($i == 0) ? 'active' : '' }}" role="tabpanel" id="task{{$task->id}}">
                                            <p><span>Task Title :</span> {{ $task->name }} </p>
                                            <p><span>To :</span> 
                                                    <a href="{{ url("/manager_dashboard/employee/$user->id", $args = []) }}" target="_blank">{{ $user->first_name.' '.$user->last_name }} </a> 
                                            </p>
                                            <p><span>Start Date :</span> {{ $task->start_date }} </p>
                                            <p><span>Regular Hrs :</span> {{ $task->regular_hours }} </p>
                                            <p><span>About:</span>{{ $task->description }}</p>
                                            <?php $documents = $task->task_documents()->get(); ?>
                                            @foreach( $documents as $document)
                                                 <?php   
                                                        $name = explode("_", $document->url);
                                                 ?>
                                                <p><span>Attach File :</span> <a href=" {{ url("/manager_dashboard/API/getDocument/task/$document->url", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                            @endforeach
                                            @if($task->status == 0)
                                                <p><span> Task Has not been approved yet</span></p>
                                            @elseif($task->status == 1)
                                                <p><span> Task is approved and in progress</span></p>
                                                <p><span>Actual Start:</span>{{ $task->actual_start }}</p>
                                                @if(!$task->pauses()->get()->isEmpty())
                                                    <p><span> This task has been paused {{ count($task->pauses) }}</span></p>
                                                    @foreach($task->pauses as $pause)
                                                        <p><span> From: {{ $pause->start }}</span></p>
                                                        <p><span> To: {{ $pause->end }}</span></p>
                                                        <p><span> Number of work hours: {{ $pause->number_of_hours }}</span></p>
                                                        <hr>
                                                    @endforeach
                                                @endif
                                            @elseif($task->status == 2)
                                                <p><span> Task is finished </span></p>
                                                <p><span>Actual Start:</span>{{ $task->actual_start }}</p>
                                                <p><span>Actual End:</span>{{ $task->actual_end }}</p>
                                                <p><span>Total Hours:</span>{{ $task->get_working_hours($task->actual_start,$task->actual_end) }}</p>
                                                <p><span>End Text:</span>{{ $task->finish_text }}</p>

                                                @if($task->finish_url != null)
                                                    <p><span>End Url:</span>{{ $task->finish_url }}</p>
                                                @endif

                                                @if($task->finish_file_path != null)
                                                    <?php   
                                                        $name = explode("_", $task->finish_file_path);
                                                        $fill = $task->finish_file_path;
                                                    ?> 
                                                    <p><span>Finish File :</span> <a href=" {{ url("/manager_dashboard/API/getDocument/usertask/$fill", $args = []) }} " target="_blank"><i class="fa fa-file-o"></i> {{$name[1]}}</a></p>
                                                @endif
                                                @if(!$task->pauses()->get()->isEmpty())
                                                    <p><span> This task has been paused {{ count($task->pauses) }} time(s)</span></p>
                                                    {{-- @foreach($task->pauses as $pause) --}}
                                                        {{-- <p><span> From: {{ $pause->start }}</span></p>
                                                        <p><span> To: {{ $pause->end }}</span></p>
                                                        <p><span> Number of work hours: {{ $pause->number_of_hours }}</span></p>
                                                        <hr> --}}
                                                        <div class="box">
                                                         <div class="box-content">
                                                            <br/><br/>
                                                            <div class="clearfix"></div>
                                                            <div class="table-responsive" style="border:0">
                                                                <table class="table table-advance" id="table1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>From</th>
                                                                            <th>to</th>
                                                                            <th>Number of hours</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php $i=1;?>
                                                                    @foreach($task->pauses as $pause)
                                                                        <tr>
                                                                            <td> {{$i++}} </td>
                                                                            <td>{{ $pause->start }}</td>
                                                                            <td>{{ $pause->end }}</td>
                                                                            <td>{{ $pause->number_of_hours }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @elseif($task->status == 3)
                                                <?php 
                                                    $pause = $task->pauses()->get()->sortByDesc('created_at')->first();
                                                ?>
                                                <p><span> Task is Paused </span></p>
                                                <p><span>Actual Start:</span>{{ $task->actual_start }}</p>
                                                <p><span> Task is Paused Since: {{ $pause->start }} </span></p>

                                            @endif
                                        </div>
                                        <?php $i++; ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

        
@endsection

@section('managerCustomScript')
        <script src="{!! asset('assets/data-tables/jquery.dataTables.js') !!}"></script>
        <script src="{!! asset('assets/data-tables/bootstrap3/dataTables.bootstrap.js') !!}"></script>
        <script type="text/javascript" charset="utf-8" async defer>
                $('#tasksSide').addClass('active');
                $(function(){
                    $('#fekry').keyup(function(){
                        var searchText = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
                        $('#tasklist > li').each(function(){
                            var currentLiText = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
                                showCurrentLi = currentLiText.indexOf(searchText) !== -1;
                            $(this).toggle(showCurrentLi);
                        });     
                    });


                    $('#selectSubDep').change(function () {
                        var searchText = $(this).val();
                        if (searchText != 'Search by category ...') {
                            searchText = $.trim(searchText).replace(/ +/g, ' ').toLowerCase();

                            $('#tasklist > li').each(function(){
                                var currentLiText = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
                                    showCurrentLi = currentLiText.indexOf(searchText) !== -1;
                                $(this).toggle(showCurrentLi);
                            });
                            
                        }

                    });

                     $('#filter').change(function () {
                        var searchText = $(this).val();
                        if (searchText != 'Search by Status ...') {
                            searchText = $.trim(searchText).replace(/ +/g, ' ').toLowerCase();
                            // var li = $('#tasklist > li');
                            $('#tasklist > li').each(function(){
                                var currentLiText = $(this).text().replace(/\s+/g, ' ').toLowerCase(),
                                    showCurrentLi = currentLiText.indexOf(searchText) !== -1;
                                $(this).toggle(showCurrentLi);
                            });
                            
                        }else{
                            $('#tasklist > li').show();

                        }

                    });
                });
        </script>
@endsection