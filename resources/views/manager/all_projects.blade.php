@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')
    <!-- BEGIN Page Title -->
   
<div class="page-title">
    <div>
        <h1><i class="fa fa-list"></i> All Project</h1>
        <h4>View all Projects Status</h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url("/manager_dashboard", $args = []) }}">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active">All Project</li>
    </ul>
</div>
<!-- END Breadcrumb -->


@if ( Session::has('splitFail') )                
    <div class="alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> {{ Session::get('splitFail')}}
    </div>
@endif


@if ( Session::has('splitSuccess') )               
    <div class="alert alert-success alert-dismissible" id="splitProjectFail">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Success!</strong> {{ Session::get('splitSuccess')}}
    </div>
@endif

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-list"></i> Projects List</h3>
                <div class="box-tool">
                    <a href="{{url('manager_dashboard/task/new')}}" class="btn btn-primary" style="color: white;"  title="">Assing Task</a>
                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content">
                <br/><br/>
                <div class="clearfix"></div>
                <div class="table-responsive" style="border:0">
                    <table class="table table-advance" id="table1">
                        <thead>
                            <tr>
                                <th style="width:18px"><input type="checkbox" /></th>
                                <th>Name</th>
                                <th>Contract Date</th>
                                <th>Start Date</th>
                                <th>Project priority</th>
                                <th>Project Status</th>
                                <th>Delivery Date</th>
                                <th>Finishing Time</th>
                                <th>Finishing Tasks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $projects as $project )
                                 
                                    <?php
                                        $delivary_date = new Carbon\Carbon($project->delivary_date);
                                        $start_date = new Carbon\Carbon($project->start_date);
                                        $now = Carbon\Carbon::now();

                                        $subprojects =  App\DepartmentProject::where([['project_id','=',$project->pivot->project_id],['department_id','=',$project->pivot->department_id]])->first();
                                    ?>
                                @if($subprojects->miniprojects()->get()->isEmpty())
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#upComing{{$project->id}}"><span class="badge badge-success">Up Coming <i class="fa fa-star"></i></span></a><a href=" {{ url("/manager_dashboard/project/".$project->id) }} "> {{ $project->name }} </a>
                                        </td>
                                        <td>{{ $project->contract_date }} </td>
                                        @if($project->start_date != null)
                                            <td> {{ $project->start_date }} </td>
                                        @else
                                            <td> Initial Project </td>
                                        @endif
                                        @if($project->priority == 0)
                                            <td>Low</td>
                                        @elseif($project->priority == 1)
                                            <td>Medium</td>
                                        @elseif($project->priority == 2)
                                            <td>High</td>
                                        @endif
                                        <td>Initial Project</td>
                                        <td>{{ $project->delivary_date }}</td>
                                        @if($project->status == 0)
                                            <td>
                                                Initial Project
                                            </td>
                                        @endif
                                        <td>Initail Project</td>
                                    </tr>

                                @elseif( !empty($subprojects->miniprojects()->get()) )
                                    <?php
                                        $finished = 0;
                                        $unfinished = 0;
                                        if ($project->status != 0) {
                                            $subs = $project->DepartmentProject()->get();
                                            foreach ($subs as $sub) {
                                                # code...
                                                $minis = $sub->miniprojects()->get();
                                                foreach ($minis as $minid) {
                                                    # code...
                                                    $tasks = $minid->tasks()->get();
                                                    foreach ($tasks as $task) {
                                                        # code...
                                                            if ($task->status == 2) {
                                                                $finished++;
                                                                
                                                            }
                                                        $unfinished++;
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td><a href=" {{ url("/manager_dashboard/project/".$project->id) }} "> {{ $project->name }} </a></td>
                                        <td> {{ $project->contract_date }} </td>
                                        @if($project->start_date != null)
                                            <td> {{ $project->start_date }} </td>
                                        @else
                                            <td> Initial Project </td>
                                        @endif
                                        @if($project->priority == 0)
                                            td>Low</td>
                                        @elseif($project->priority == 1)
                                            <td>Medium</td>
                                        @elseif($project->priority == 2)
                                            <td>High</td>
                                        @endif
                                        @if($project->status == 1)
                                            <td>In Progress</td>
                                        @elseif($project->status == 3)
                                            <td>Paused</td>
                                        @endif
                                        <td> {{ $project->delivary_date }} </td>

                                        @if($project->status == 1 || $project->status == 3)

                                            @if($delivary_date < $now)
                                                <td>
                                                    <div class="clearfix">
                                                        <span class="pull-left">Delay</span>
                                                        <span class="pull-right">{{ ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%</span>
                                                    </div>

                                                    <div class="progress progress-mini">
                                                        <div style="width:{{ ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%" class="progress-bar progress-bar-danger"></div>
                                                    </div>
                                                </td>
                                            @elseif($delivary_date > $now)
                                                <td>
                                                    <div class="clearfix">
                                                        <span class="pull-left">in Progress</span>
                                                        <span class="pull-right">{{ ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%</span>
                                                    </div>

                                                    <div class="progress progress-mini progress-striped active">
                                                        <div style="width:{{ ceil(($now->diffInHours($start_date) / $delivary_date->diffInHours($start_date)) * 100) }}%" class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                </td>

                                            @endif
                                        @elseif($project->status == 2)
                                            <td>Finished</td>
                                            <td>
                                                <div class="clearfix">
                                                    <span class="pull-left">Finish</span>
                                                    <span class="pull-right">100%</span>
                                                </div>

                                                <div class="progress progress-mini">
                                                    <div style="width:100%" class="progress-bar"></div>
                                                </div>
                                            </td>
                                        @endif

                                        <td class="finishing-tasks"><span class="done-tasks">{{ $finished }}</span> / <span class="total-tasks">{{ $unfinished }}</span></td>
                                    </tr>
                                @endif
                            @endforeach
                            
                          
                        </tbody>
                    </table>
                </div>
                <?php $department = Auth::user()->department()->first() ; $subs = explode("/",$department->sub_department); ?>
                <!-- Modal -->
            @foreach( $projects as $project )

                  <div class="modal fade" id="upComing{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Upcoming Project</h4>
                          </div>
                          <div class="modal-body">
                            <form action="{{ url("/manager_dashboard/project/$project->id/split", $args = []) }}" method="POST"  class="form-horizontal" enctype="multipart/form-data">
                                 {{ csrf_field() }}
                                <div class="form-group">
                                  <div class="col-sm-5 col-lg-5 controls">
                                     <select data-placeholder="Type of Task" class="form-control" tabindex="6" name="select1" required>
                                        <option value=""> </option>
                                        @foreach($subs as $sub)
                                            <option>{{$sub}}</option>
                                        @endforeach
                                     </select>
                                  </div>
                                    <div class="col-sm-5 col-lg-5 controls">
                                      <input class="form-control" type="number" placeholder="Regular Days" name="numOfdys1" required>
                                    </div>
                                    <div class="col-sm-2 col-lg-2 controls">
                                      <a href="#"><i class="fa fa-plus"></i></a>
                                    </div>
                                    <br/>
                                    <br/><br/>
                               </div>
                                
                                <input type="hidden" name="number" value="1" id="numberOF{{$project->id}}" >
                                
                                <input type="submit" class="btn btn-primary ">
                                {{-- <br> --}}
                                
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
                    <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>

                    <script  type="text/javascript" charset="utf-8" async defer>
                         $('#upComing{{$project->id}} .fa-plus').click( function(){
                            var numberOF = $('#numberOF{{$project->id}}').val();
                            numberOF++;
                            $('#upComing{{$project->id}} .form-group').append('<div class="col-sm-5 col-lg-5 controls formGroup{{$project->id}}'+numberOF+'">\
                        <select data-placeholder="Type of Task" class="form-control chosen" tabindex="6" name="select'+(numberOF)+'" required>\
                            <option value=""> </option>\
                            @foreach($subs as $sub)\                    <option>{{$sub}}</option>\
                            @endforeach\
                        </select>\
                    </div>\
                    <div class="col-sm-5 col-lg-5 controls formGroup{{$project->id}}'+numberOF+'">\
                        <input class="form-control" type="number" placeholder="Regular Days" name="numOfdys'+(numberOF)+'" required>\
                        <br/>\
                    </div>\
                    <div class="col-sm-2 col-lg-2 controls formGroup{{$project->id}}'+numberOF+'">\
                      <a href="#"><i class="fa fa-times" onclick="remove({{$project->id}}'+numberOF+')"></i></a>\
                    </div>\
                    ');
                            
                            $('#numberOF{{$project->id}}').val(numberOF);
                        });

                         function remove(num) {
                             // body...
                            // alert(num);
                            var numberOF = $('#numberOF{{$project->id}}').val();

                            numberOF--;
                            var id = '.formGroup'+num;
                            $(id).remove();
                            {{-- $('#numberOF{{$project->id}}').val(numberOF);                                                         --}}
                            document.getElementById("numberOF{{$project->id}}").value = numberOF;
                            // alert(document.getElementById("numberOF{{$project->id}}").value);
                         }
                       
                     </script>


                    @endforeach
                
            </div>
        </div>
    </div>
</div>


        

@endsection

@section('managerCustomScript')
       
    <script type="text/javascript" charset="utf-8" async defer>
            $('#projectSide').addClass('active');
    </script>
     <!--page specific plugin scripts-->
    <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-inputmask/bootstrap-inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-tags-input/jquery.tagsinput.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-pwstrength/jquery.pwstrength.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js') !!}"></script>
    <script src="{!! asset('assets/dropzone/downloads/dropzone.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
    <script src="{!! asset('assets/clockface/js/clockface.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}"></script> 
    <script src="{!! asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}"></script>
    <script src="{!! asset('assets/ckeditor/ckeditor.js') !!}"></script> 

@endsection