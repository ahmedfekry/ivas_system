@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')

     <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Dashboard</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-home"></i>
                            <a href="{{ url("/manager_dashboard", $args = []) }}">Home</a>
                        </li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                <?php 
                    $numOfnwPrjts = 0;
                ?>
                @foreach($projects as $project)
                    <?php 
                         $subprojects =  App\DepartmentProject::where([['project_id','=',$project->pivot->project_id],['department_id','=',$project->pivot->department_id]])->first();
                    ?>
                    @if($subprojects->miniprojects()->get()->isEmpty())
                        <?php
                            $numOfnwPrjts+=1;
                        ?>
                    @endif
                @endforeach
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <a class="tile tile-red new_pro" href=" {{ url("/manager_dashboard/projects") }} ">
                            <div class="img img-center">
                                <i class="fa fa-briefcase"></i>
                                <span class="badge badge-success">{{$numOfnwPrjts}}</span>
                            </div>
                            <p class="title text-center">New Projects</p>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <a href=" {{ url("/manager_dashboard/tasks") }} " class="tile">
                            <p class="title">Check All Tasks</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <div class="img img-bottom">
                                <i class="fa fa-bars"></i>
                            </div>
                        </a>
                    </div>
                    <a href="{{ url("/manager_dashboard/projects") }}" class="col-md-4 col-sm-6 col-xs-12 tile-active">
                        <div class="tile tile-magenta">
                            <div class="img img-center">
                                <i class="fa fa-desktop"></i>
                            </div>
                            <p class="title text-center">Projects</p>
                        </div>

                        <div class="tile tile-blue">
                            <p class="title">Projects</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <div class="img img-bottom">
                                <i class="fa fa-desktop"></i>
                            </div>
                        </div>
                    </a>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <a href=" {{ url("/manager_dashboard/task/new") }} " class="tile tile-green">
                            <div class="img">
                                <i class="fa fa-copy"></i>
                            </div>
                            <div class="content">
                                <p class="big">+New</p>
                                <p class="title">Create Task</p>
                            </div>
                        </a>
                    </div>
                </div>
                @if ( Session::has('todoSuccess') )                
                {{-- <h1><i class="fa fa-list"></i>  </h1> --}}
                    <div class="alert alert-success alert-dismissible col-md-7">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> {{ Session::get('todoSuccess')}}
                    </div>
                @endif
                @if(count($errors))
                    <div class="alert alert-danger col-md-7">
                    <button class="close" data-dismiss="alert">×</button>
                    <h4>Error!</h4> 
                  @foreach($errors->all() as $error)
                 
                      <p>{{$error}}</p> 

                  @endforeach
                   </div>
                @endif
                
                <div class="row">
                    <div class="col-md-7">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-check"></i> Todo List <a class="new_todo" href="#" data-toggle="modal" data-target="#newToDo"><i class="fa fa-plus"></i> plus</a></h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="newToDo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">New Todo</h4>
                                      </div>
                                      <div class="modal-body">
                                        <form action=" {{ url("/manager_dashboard/todo/new") }} " method="POST" class="form-horizontal">
                                         {{ csrf_field() }}
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Title</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                  <input class="form-control" type="text" placeholder="title" name="title" required>
                                                </div>
                                              </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 col-lg-2 control-label">Priority</label>
                                              <div class="col-sm-9 col-lg-10 controls">
                                                 <label class="radio-inline">
                                                    <input type="radio" name="priority" value="0" required checked /> low
                                                 </label>
                                                  <label class="radio-inline">
                                                    <input type="radio" name="priority" value="1" /> Medium
                                                 </label>
                                                 <label class="radio-inline">
                                                    <input type="radio" name="priority" value="2" /> High
                                                 </label> 
                                              </div>
                                           </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Project</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                  <select class="form-control"  name="project_id" required >
                                                        <option> </option>
                                                        @foreach($projects as $project)
                                                            @if($project->status == 1)
                                                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Regular Hrs</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                  <input class="form-control" type="number" step="0.1" placeholder="00.00" name="regular_hours" required>
                                                </div>
                                              </div>
                                            
                                            <div class="form-group submit-btn">
                                                  <input type="submit" name="submit" class="btn btn-primary " value="Submit">
                                              </div>


                                        </form>
                                      </div>
<!--
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                      </div>
-->
                                    </div>
                                  </div>
                                </div>
                                
                            </div>
                            <div class="box-content">
                                <ul class="todo-list">
                                    <?php $todos = $todos->sortBy('priority');?>
                                    @foreach($todos as $todo)
                                        <li>
                                            <div class="todo-desc">
                                                <p><a href="#" data-toggle="modal" data-target="#todo_myModal{{$todo->id}}"> {{ $todo->title }} </a></p>
                                            </div>
                                            <div class="todo-actions">
                                                @if( $todo->priority == 0 )
                                                    <span class="label label-success">low</span>
                                                @elseif($todo->priority == 1)
                                                    <span class="label label-warning">Medium</span>
                                                @elseif($todo->priority == 2)
                                                    <span class="label label-important">High</span>
                                                @else
                                                    <span class="label label-primary">Done</span>
                                                @endif

                                                @if($todo->priority != 3)
                                                    <a class="show-tooltip" href="{{ url("/manager_dashboard/todo/done/$todo->id", $args = []) }}" title="It's done" data-original-title="It's done"><i class="fa fa-check"></i></a>
                                                @endif
                                                {{-- <a class="show-tooltip" href="#" title="" data-original-title="Remove"><i class="fa fa-times"></i></a> --}}
                                            </div>
                                        </li>
                                    @endforeach
                                    
                                </ul>

                                @foreach($todos as $todo)
                                <!-- Modal -->
                                    <div class="modal fade" id="todo_myModal{{$todo->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">ToDo info</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="todo-info">
                                                <p><span>Title:</span> {{$todo->title}} </p>
                                                {{-- <p><span>Priority:</span> low </p> --}}
                                                @if( $todo->priority == 0 )
                                                    <p><span class="label label-success">Priority:</span> low</p>
                                                @elseif($todo->priority == 1)
                                                    <p><span>Priority:</span> Medium</p>
                                                @elseif($todo->priority == 2)
                                                    <p><span>Priority:</span> High</p>
                                                @else
                                                    <p><span>Priority:</span> Done</p>
                                                @endif
                                                <p><span>project:</span> {{$todo->project->name}} </p>
                                                <p><span>Regular Hrs:</span> {{$todo->regular_hours}} </p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <!-- Projects Status-->
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-title">
                                    <h3><i class="fa fa-bar-chart-o"></i> Projects Status</h3>
                                    <div class="box-tool">
                                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div id="chart_div" style="width: 100%; height: 500px;"></div>
                                </div>
                            </div>
                        </div>
                    <!-- //Projects Status-->
                    </div>

                </div>

                                  <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        
@endsection

@section('managerCustomScript')
        
          <!--page specific plugin scripts-->
        <script src="{!! asset('assets/flot/jquery.flot.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.resize.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.pie.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.stack.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.crosshair.js') !!}"></script>
        <script src="{!! asset('assets/flot/jquery.flot.tooltip.min.js') !!}"></script>
        <script src="{!! asset('assets/sparkline/jquery.sparkline.min.js') !!}"></script>
        <script src="{!! asset('https://www.gstatic.com/charts/loader.js') !!}"></script>
        
        <script>
            $(document).ready(function () {
                $('#dashboardSide').addClass('active');
                $('body').addClass('ProjectsManagerInfoChart');
                var findAlbums = $('html').find('body');
                if (findAlbums.hasClass('ProjectsManagerInfoChart')) {
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawVisualization);

                      function drawVisualization() {

                        var temp = function () {
                            var datares = null;
                            $.ajax({
                                async: false,
                                gloabal: false,
                                type: "GET",
                                url: "manager_dashboard/API/getGraph",
                                dataType: "json",
                                success: function (data) {
                                    datares = data;
                                }
                            });
                            return datares;
                        }();
                        console.log(temp);
                        var dataArray = new Array();
                        var departments = new Array('Sub Department');
                        for (var i = 0; i < temp.sub_departments.length ; i++) {
                            departments.push(temp.sub_departments[i]);
                        }
                        // console.log(departments);
                        dataArray.push(departments);
                        for (var i = 0; i < temp.projects.length ; i++) {
                            var temp1 = new Array();
                            var totoal = parseInt(temp.projects[i].total_tasks);
                            temp1.push(temp.projects[i].project_name);
                            for (var j = 1; j < departments.length; j++) {
                                var dep = departments[j];
                                // console.log(totoal);
                                // console.log()
                                temp1.push(parseInt((parseInt(temp.projects[i][dep])/totoal)*100));
                            }
                            // console.log(temp1);
                            dataArray.push(temp1);
                        }
                        var data = google.visualization.arrayToDataTable(dataArray);
                          console.log(dataArray[0]);
                    if(dataArray.length <= 3) {
                              var options = {
                                  title : 'Departments Percentage for Projects',
                                  vAxis: {title: 'Sub Departments Percentage'},
                                  hAxis: {title: 'Projects'},
                                    bar: {groupWidth: "10%"},
                                  seriesType: 'bars'
                                };
                          } else {

                             var options = {
                                 title : 'Sub Departments Percentage for Projects',
                                 vAxis: {title: 'Sub Departments Percentage'},
                                 hAxis: {title: 'Projects'},
                                 seriesType: 'bars'
                           //      series: {5: {type: 'line'}}
                               };

                          }


                    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                  }
             
                }
             });
        </script>
        

@endsection
