@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')
  		
  {{-- 		<script src="{!! asset('https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        <script src="{!! asset('assets/bootstrap/js/bootstrap.min.js') !!}"></script>
 --}}
				<div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Create Task</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href=" {{ url("/manager_dashboard", $args = []) }} ">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Create Task</li>
                    </ul>
                </div>
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bars"></i> Create Task</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                 <form action=" {{ url("/manager_dashboard/task/store", $args = []) }} " method="POST" class="form-horizontal" autocomplete="off" enctype="multipart/form-data" >
                                 {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Task Title</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="Task Title" value="{{ old('title') }}" class="form-control" name="title" required>
                                       @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                       </div>
                                    </div>
                                     
                                    <div class="form-group">
                                      <label class="col-sm-3 col-lg-2 control-label">Project</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <select class="form-control chosen" data-placeholder="Choose a Project" name="selectProject" tabindex="1" id="selectProject" onchange="jsfunction()" required>
                                            <option> </option>
                                          
                                            @foreach($projects as $project )
                                              @if($project->status == 1)
                                            	  <option value="{{ $project->id }}" {{ (old('selectProject') == $project->id ? "selected":"") }}> {{ $project->name }} </option>
                                              @endif
                                            @endforeach
                                         </select>
                                      </div>
                                    </div>

  									                <div class="form-group{{ $errors->has('selectSubProject') ? ' has-error' : '' }}">
                                        <label class="col-sm-3 col-lg-2 control-label">Sub Project</label>
                                        <div class="col-sm-9 col-lg-10 controls" id="selectSubProject" >
                                           @if(old('selectSubProject'))
                                              <?php
                                                $department = Auth::user()->department;
                                                $subprojects = App\DepartmentProject::where([['project_id','=',old('selectProject')],['department_id','=',$department->id]])->first()->miniprojects;
                                              ?>
                                              @foreach($subprojects as $subpro)
                                                <label class="radio-inline">
                                                  <input type="radio" name="selectSubProject" {{ (old('selectSubProject') == $subpro->id ? "checked":"") }} value="{{ $subpro->id }}" > {{ $subpro->name }}
                                                </label>
                                              @endforeach
                                           @endif
                                        </div>
                                        @if ($errors->has('selectSubProject'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('selectSubProject') }}</strong>
                                          </span>
                                      @endif
                                    </div>
                                    

                                    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">To... </label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <select class="form-control"  name="user_id" id="toUser" required>
                                            <option value=""> </option>
                                            {{-- @foreach( $users as $user )
                                               <option value="{{$user->id}}" {{ (old('user_id') == $user->id ? "selected":"") }} >{{$user->first_name.' '.$user->last_name}}</option>
                                            @endforeach --}}
                                         </select>
                                      @if ($errors->has('user_id'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('user_id') }}</strong>
                                          </span>
                                      @endif
                                      </div>
                                    </div>
                                    
                                    <div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Priority</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <label class="radio-inline">
                                            <input type="radio" name="priority" value="0" required  {{ (old('priority') == 0 ? "checked":"") }}/> low
                                         </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="priority" value="1" {{ (old('priority') == 1 ? "checked":"") }} /> Medium
                                         </label>
                                         <label class="radio-inline">
                                            <input type="radio" name="priority" value="2" {{ (old('priority') == 2 ? "checked":"") }} /> High
                                         </label> 
                                      @if ($errors->has('priority'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('priority') }}</strong>
                                          </span>
                                      @endif
                                      </div>
                                    </div>
                                     
                                    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Start Date</label>
                                       <div class="col-sm-9 col-lg-10 controls">

                                               <div class="input-group date date-picker" data-date="Date()" data-date-format="dd-mm-yyyy" data-date-viewmode="days">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input class="form-control " value="{{ old('start_date') }}" id="dp1" size="16" type="text" name="start_date" >
                                                 </div>
                                       @if ($errors->has('start_date'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('start_date') }}</strong>
                                          </span>
                                      @endif
                                       </div>
                                    </div>
                                     
                                     <div class="form-group{{ $errors->has('regular_hours') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label">Regular Hrs</label>
                                       <div class="col-sm-3 col-lg-4 controls">
                                          <input type="number" placeholder="00" min="1" value="{{ old('regular_hours') }}" class="form-control" name="regular_hours" required>
                                       </div>
                                       <h4>Hours</h4>
                                       @if ($errors->has('regular_hours'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('regular_hours') }}</strong>
                                          </span>
                                      @endif
                                    </div>
                                     
                                     <div class="form-group{{ $errors->has('taskDocument') ? ' has-error' : '' }}">
                                      <label class="col-sm-3 col-lg-2 control-label">Add file</label>
                                      <div class="col-sm-9 col-lg-10 controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="file-input" value="{{ old('taskDocument') }}" name="taskDocument" />
                                            </span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                         </div>
                                      </div>
                                      @if ($errors->has('taskDocument'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('taskDocument') }}</strong>
                                          </span>
                                      @endif
                                   </div>
                                     
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                       <label class="col-sm-3 col-lg-2 control-label"> More info </label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <textarea class="form-control" rows="3" name="description" >{{ old('description') }}</textarea>
                                       </div>
                                       @if ($errors->has('description'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('description') }}</strong>
                                          </span>
                                      @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Send</button>
                                           <button type="button" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                     
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
                <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
                <script type="text/javascript" charset="utf-8" async defer>
                        $('#tasksSide').addClass('active');
                </script>
        
@endsection

@section('managerCustomScript')
    <script type="text/javascript" charset="utf-8" async defer>
            $('#tasksSide').addClass('active');
    </script>
    
    <script type="text/javascript" charset="utf-8" async defer>
      function jsfunction() {
        // body...
      var project = $('#selectProject').val();
      // alert(location);
      $('#selectSubProject').html('');

      var url = '../API/getMiniProjects/'+project;
      $.ajax({
          dataType:'json',
          type: 'GET',
          url: url,
          success: function(data) {

              if (data.length <= 0 ) {
                  $('#selectSubProject').append('<h4 style="color: red;"> you need to assign a sub projects to that project first </h4>');
              }else{
                for (var i = 0; i <= data.length - 1; i++) {
                  if (i == 0) {
                     $('#selectSubProject').append( '<label class="radio-inline">\
                                  <input type="radio" name="selectSubProject" onclick="getEmployeesBySub(\''+data[i]['name']+'\')" value="'+data[i]['id']+'" required />'+data[i]['name']+'\
                               </label>' );
                  }else{
                      $('#selectSubProject').append( '<label class="radio-inline">\
                                  <input type="radio" name="selectSubProject" onclick="getEmployeesBySub(\''+data[i]['name']+'\')"  value="'+data[i]['id']+'"  />'+data[i]['name']+'\
                               </label>' );
                  }
                }
              }
          }
      }); 
      }
    </script>
    {{-- <option value="{{$user->id}}" {{ (old('user_id') == $user->id ? "selected":"") }} >{{$user->first_name.' '.$user->last_name}}</option> --}}

    <script>
      function getEmployeesBySub(test) {
          var url = '../API/getEmployeeBySub/'+test+'/department/'+{{Auth::user()->department->id}};
          // alert(url);
          $.ajax({
              dataType:'json',
              type: 'GET',
              url: url,
              success: function(data) {
                $("#toUser").html('');
                $("#toUser").append('<option value=""></option>')
                for (var i = data.length - 1; i >= 0; i--) {
                  $("#toUser").append('<option value="'+data[i]['id']+'" >'+data[i]['first_name']+" "+data[i]['last_name']+'</option>')
                }
              }
          });
      }
    </script>
    <!--page specific plugin scripts-->
    <script src="{!! asset('assets/chosen-bootstrap/chosen.jquery.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-inputmask/bootstrap-inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-tags-input/jquery.tagsinput.min.js') !!}"></script>
    <script src="{!! asset('assets/jquery-pwstrength/jquery.pwstrength.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js') !!}"></script>
    <script src="{!! asset('assets/dropzone/downloads/dropzone.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
    <script src="{!! asset('assets/clockface/js/clockface.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/date.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-switch/static/js/bootstrap-switch.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}"></script> 
    <script src="{!! asset('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}"></script>
    <script src="{!! asset('assets/ckeditor/ckeditor.js') !!}"></script> 




@endsection