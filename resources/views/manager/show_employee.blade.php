@extends('layouts.manager')

@section('pageTitle')
    <title>Ivas System</title>
@endsection

@section('managerContent')
		<div id="main-content">

	         <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Emplyee Info</h1>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href=" {{ url("/admin_dashboard/", $args = []) }} ">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Emplyee Info</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="emplyee_info row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Profile Info</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content main_info">
                                <div class="row">
                                    <div class="col-md-3 profile_img">
                                        {{-- <img class="img-responsive img-thumbnail" src="img/demo/profile-picture.jpg" alt="profile picture" /> --}}
                                        @if($employee->profile_picture == '')
                                            {{ HTML::image("img/demo/thumbnail_default-profile.jpg", 'profile picture', ['class'=>'img-responsive img-thumbnail']) }}
                                        @else
                                            {{HTML::image( $employee->profile_picture , ' Panny\'s Avatar', ['class' => 'img-responsive img-thumbnail'])}}
                                        @endif
                                        {{-- {{ HTML::image($employee->profile_picture,'profile picture',['class' => 'img-responsive img-thumbnail'] ) }} --}}
                                        <a href="#"><i class="fa fa-wechat"></i></a>
                                        <br/><br/>
                                    </div>
                                    <div class="col-md-9 user-profile-info">
                                        {{-- <p><span>Username:</span> Abdelrahman</p> --}}
                                        <p><span>Email:</span> <a href="#"> {{ $employee->email }} </a></p>
                                        <p><span>First Name:</span> {{ $employee->first_name }} </p>
                                        <p><span>Last Name:</span> {{ $employee->last_name }} </p>
                                        <p><span>Birthday:</span> {{ $employee->birthday }}</p>
                                        <p><span>Date of Hire:</span> {{ $employee->hire_date }} </p>
                                        <p><span>Gender:</span> {{ $employee->gender }}</p>
                                        <p><span>Position:</span> {{ $employee->title }}</p>
                                        <p><span>Mobile Number:</span> {{ $employee->phone_number }}</p>
                                        <p><span>About:</span>{{ $employee->about }} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
                          <script src="{!! asset('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}"></script>
        <script>window.jQuery || document.write('<script src="{!! asset('assets/jquery/jquery-2.1.4.min.js') !!}"><\/script>')</script>
        <script type="text/javascript" charset="utf-8" async defer>
                $('#employeeSide').addClass('active');
        </script>
                
@endsection
