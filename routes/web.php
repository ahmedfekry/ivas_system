<?php
use App\Http\Middleware\Role;
use App\Http\Middleware\Admin;
use App\Http\Middleware\Manager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

Route::group(['middleware' => 'web'], function() {

	Route::get('/',  ['middleware' => 'guest',function () {
	    return view('auth.login');
	}]);

	Route::get('/user/{user}', 'UserController@show');
	Route::get('/user', 'UserController@new');

	Route::group(['middleware' => 'admin', 'prefix' => 'admin_dashboard'], function() {
		Route::get('/', 'AdminController@admin_dashboard');
		Route::get('/employee/new', 'AdminController@create');
		Route::post('/employee/store', 'AdminController@store');
		Route::get('/employees', 'AdminController@employees');
		Route::get('/profile', 'AdminController@profile');
		Route::post('/profile/edit', 'AdminController@edit_profile');
		Route::post('/employee/change_pass', 'AdminController@change_pass');
		Route::get('/employee/{employee}', 'AdminController@show_employee');
		Route::get('/employee/{employee}/delete', 'AdminController@delete_employee');
		Route::get('/messages', 'AdminController@messages');
		Route::get('/target', 'AdminController@target');

		Route::get('/project/new', 'ProjectController@create');
		Route::post('/project/store', 'ProjectController@store');
		Route::get('/projects', 'ProjectController@index');
		Route::get('/project/{project}', 'ProjectController@show');
		Route::get('/project-timeline/{project}', 'ProjectController@timeline');
		Route::post('/profile/update_password', 'AdminController@update_password');
		Route::post('/profile/image_upload', 'AdminController@image_upload');
		Route::get('/API/getSubDepartments/{department}', 'DepartmentController@getSubDepartment');
		Route::get('/API/getUsers/{department}', 'DepartmentController@getUsers');
		Route::get('/API/getAdmins/', 'DepartmentController@getAdmins');
		Route::get('/API/getDocument/{type}/{path}', 'ProjectController@getDocument');
		Route::get('/API/getGraph', 'ProjectController@graph');
		
		Route::get('/search/{string}', 'HomeController@search');
		Route::get('/vacation/new', 'VacationController@create');
		Route::get('/vacations', 'VacationController@index');
		Route::post('/vacation/store', 'VacationController@store');

		Route::get('/department/new', 'DepartmentController@create');
		Route::post('/department/store', 'DepartmentController@store');
		Route::get('/departments', 'DepartmentController@index');
		Route::get('/department/{id}', 'DepartmentController@show');

		Route::get('project/pause/{project}', 'ProjectController@pause');
		Route::get('project/resume/{project}', 'ProjectController@resume');
		Route::post('project/{project}/extend', 'ProjectController@extend');
		Route::get('project/{project}/user/{user}', 'ProjectController@userproject');

	});

	Route::group(['middleware' => 'employee', 'prefix' => 'employee_dashboard'], function() {
	    //
		Route::get('/', 'EmployeeController@employee_dashboard');
		Route::get('/profile', 'EmployeeController@profile');
		Route::post('/profile/edit', 'EmployeeController@edit_profile');
		Route::post('/profile/update_password', 'EmployeeController@update_password');
		Route::post('/profile/image_upload', 'EmployeeController@image_upload');
		
		Route::get('/department/team', 'DepartmentController@team');

		
		Route::get('/projects', 'ProjectController@index');
		Route::get('/project/{project}', 'ProjectController@show');

		
		Route::get('/tasks', 'TaskController@tasks');
		Route::get('/task/{task}/approve', 'TaskController@approve');
		Route::post('/task/{task}/finish', 'TaskController@finish');

		Route::get('/API/getDocument/{type}/{path}', 'ProjectController@getDocument');
		
		Route::post('/todo/new', 'ToDoController@store');
		Route::get('/todo/done/{todo}', 'ToDoController@done');

		Route::get('/search/{string}', 'HomeController@search');
		Route::get('/task/start_pause/{task}', 'TaskController@start_pause');
		Route::get('/task/end_pause/{task}', 'TaskController@end_pause');
		Route::get('project/{project}/user/{user}', 'ProjectController@userproject');
		
	});


	Route::group(['middleware' => 'manager','prefix' => 'manager_dashboard'], function (){
		# code...
		Route::get('/', 'ManagerController@manager_dashboard');
		Route::get('/project/new', 'ProjectController@create');
		Route::post('/project/store', 'ProjectController@store');
		Route::get('/projects', 'ProjectController@index');
		Route::get('/project/{project}', 'ProjectController@show');
		Route::get('/project-timeline/{project}', 'ProjectController@timeline');
		Route::post('/project/{project}/split', 'ProjectController@split');
		Route::get('/API/getMiniProjects/{project}', 'ProjectController@getMiniProjects');
		Route::get('/API/getDocument/{type}/{path}', 'ProjectController@getDocument');
		
		Route::get('/API/getEmployeeBySub/{sub}/department/{department}', 'UserController@getEmployeeBySub');
		
		Route::get('/tasks', 'TaskController@tasks');
		Route::get('/task/new', 'TaskController@create_task');
		Route::post('/task/store', 'TaskController@store_task');
		Route::get('/project/{project}/new_task', 'TaskController@new_task');
		
		Route::get('/department/team', 'DepartmentController@team');


		Route::get('/profile', 'ManagerController@profile');	
		Route::post('/profile/edit', 'ManagerController@edit_profile');
		Route::post('/profile/update_password', 'ManagerController@update_password');
		Route::post('/profile/image_upload', 'ManagerController@image_upload');
		Route::get('/employee/{employee}', 'AdminController@show_employee');
		
		Route::post('/todo/new', 'ToDoController@store');
		Route::get('/todo/done/{todo}', 'ToDoController@done');
	
		Route::get('/search/{string}', 'HomeController@search');

		Route::get('/API/getGraph', 'ProjectController@managerGraph');
		
		Route::get('project/pause/{project}', 'ProjectController@pause');
		Route::get('project/resume/{project}', 'ProjectController@resume');
		Route::post('project/{project}/extend', 'ProjectController@extend');
		Route::get('project/{project}/user/{user}', 'ProjectController@userproject');
		
	});




	Route::get('/departments', 'DepartmentController@index');
	Route::get('/departments/new', 'DepartmentController@create');
	// Route::get('/departments/{department}', 'DepartmentController@show');

	Auth::routes();

	Route::get('/home', 'HomeController@index');



    //
});
