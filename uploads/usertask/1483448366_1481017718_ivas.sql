-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2016 at 02:33 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ivas`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `description`) VALUES
(1, 'Digital', 'Digital'),
(2, 'IT', 'IT'),
(3, 'HR', 'HR');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `description`, `date`, `url`, `type`, `project_id`, `created_at`, `updated_at`) VALUES
(1, 'happy new project', 'this is a document for project: happy new project', '2016-11-28 10:06:26', 'uploads/project\\1480327586_file.docx', 'document', 1, '2016-11-28 08:06:26', '2016-11-28 08:06:26'),
(2, 'fucken new project', 'this is a document for project: fucken new project', '2016-11-28 10:53:20', 'uploads/project\\1480330400_file.docx', 'document', 2, '2016-11-28 08:53:20', '2016-11-28 08:53:20'),
(3, 'Ahmed', 'this is a document for project: Ahmed', '2016-11-28 14:29:49', 'uploads/project\\1480343389_file.docx', 'document', 3, '2016-11-28 12:29:49', '2016-11-28 12:29:49'),
(4, 'bew', 'this is a document for project: bew', '2016-11-28 14:31:18', 'uploads/project\\1480343478_file.docx', 'document', 4, '2016-11-28 12:31:18', '2016-11-28 12:31:18'),
(5, 'hiiiii', 'this is a document for project: hiiiii', '2016-11-28 15:09:53', 'uploads/project\\1480345793_file.docx', 'document', 5, '2016-11-28 13:09:53', '2016-11-28 13:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(11) NOT NULL,
  `timesheet_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` double NOT NULL,
  `end_time` double NOT NULL,
  `type` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(156, '2016_11_21_145722_create_department_table', 1),
(157, '2016_11_21_145722_create_document_table', 1),
(158, '2016_11_21_145722_create_leaves_table', 1),
(159, '2016_11_21_145722_create_overtime_table', 1),
(160, '2016_11_21_145722_create_project_table', 1),
(161, '2016_11_21_145722_create_report_table', 1),
(162, '2016_11_21_145722_create_subproject_table', 1),
(163, '2016_11_21_145722_create_task_table', 1),
(164, '2016_11_21_145722_create_timesegment_table', 1),
(165, '2016_11_21_145722_create_timesheet_table', 1),
(166, '2016_11_21_145722_create_userproject_table', 1),
(167, '2016_11_21_145722_create_users_table', 1),
(168, '2016_11_21_145722_create_usertask_table', 1),
(169, '2016_11_21_145724_add_foreign_keys_to_document_table', 1),
(170, '2016_11_21_145724_add_foreign_keys_to_leaves_table', 1),
(171, '2016_11_21_145724_add_foreign_keys_to_overtime_table', 1),
(172, '2016_11_21_145724_add_foreign_keys_to_report_table', 1),
(173, '2016_11_21_145724_add_foreign_keys_to_subproject_table', 1),
(174, '2016_11_21_145724_add_foreign_keys_to_task_table', 1),
(175, '2016_11_21_145724_add_foreign_keys_to_timesegment_table', 1),
(176, '2016_11_21_145724_add_foreign_keys_to_timesheet_table', 1),
(177, '2016_11_21_145724_add_foreign_keys_to_userproject_table', 1),
(178, '2016_11_21_145724_add_foreign_keys_to_users_table', 1),
(179, '2016_11_21_145724_add_foreign_keys_to_usertask_table', 1),
(180, '2016_11_21_145851_add_time_stamp_to_users', 1),
(181, '2016_11_21_150343_add_role_to_users', 1),
(182, '2016_11_22_115547_remove_columns_from_users', 1),
(183, '2016_11_22_143304_modify_profile_picture_in_user', 1),
(184, '2016_11_22_143534_modify_profile_picture_in_users', 1),
(185, '2016_11_22_145610_modify_salary_in_users', 1),
(186, '2016_11_22_160653_remove_file_path_from_projects', 1),
(187, '2016_11_22_161031_add_time_stamp_to_projects', 1),
(188, '2016_11_22_161337_remove_total_hours_from_projects', 1),
(189, '2016_11_22_180845_remove_start_date_projects', 1),
(190, '2016_11_24_074043_change_status_to_type_in_project', 1),
(191, '2016_11_24_075106_change_type_in_project', 1),
(192, '2016_11_24_124629_add_time_stamp_to_document', 1),
(193, '2016_11_24_134202_change_contract_period_in_project', 1),
(194, '2016_11_28_094116_create_table_mini_project', 1),
(195, '2016_11_28_095326_add_mini_project_id_to_tasks', 2),
(196, '2016_11_28_130518_add_time_stamp_to_miniproject', 3),
(197, '2016_11_28_150602_add_name_to_miniproject', 4),
(198, '2016_11_28_171125_drop_departments_projects_id_from_task', 5),
(200, '2016_11_28_172732_drop_end_date_and_approved_from_task', 6),
(201, '2016_11_28_180207_add_time_stamp_task', 7);

-- --------------------------------------------------------

--
-- Table structure for table `miniproject`
--

CREATE TABLE `miniproject` (
  `id` int(11) NOT NULL,
  `regular_days` int(11) NOT NULL,
  `departments_projects_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `miniproject`
--

INSERT INTO `miniproject` (`id`, `regular_days`, `departments_projects_id`, `created_at`, `updated_at`, `name`) VALUES
(1, 234, 4, '2016-11-28 11:08:20', '2016-11-28 11:08:20', 'website'),
(3, 12, 1, '2016-11-28 12:28:30', '2016-11-28 12:28:30', 'website'),
(4, 12, 1, '2016-11-28 12:29:15', '2016-11-28 12:29:15', 'ios'),
(5, 12, 5, '2016-11-28 12:30:01', '2016-11-28 12:30:01', 'website'),
(6, 123, 5, '2016-11-28 12:30:01', '2016-11-28 12:30:01', 'ios'),
(7, 123, 6, '2016-11-28 12:31:37', '2016-11-28 12:31:37', 'website'),
(8, 12312, 6, '2016-11-28 12:31:37', '2016-11-28 12:31:37', 'android'),
(9, 123, 6, '2016-11-28 12:31:37', '2016-11-28 12:31:37', 'ios'),
(10, 4, 7, '2016-11-28 13:10:18', '2016-11-28 13:10:18', 'website'),
(11, 4, 7, '2016-11-28 13:10:18', '2016-11-28 13:10:18', 'ios');

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `start_time` double NOT NULL,
  `end_time` double NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `time_segment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contract_date` date NOT NULL,
  `delivary_date` date NOT NULL,
  `contract_period` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `type`, `description`, `contract_date`, `delivary_date`, `contract_period`, `created_at`, `updated_at`) VALUES
(1, 'happy new project', 'Website / ', 'info', '2013-05-14', '2013-05-14', 12, '2016-11-28 08:06:26', '2016-11-28 08:06:26'),
(2, 'fucken new project', 'Website / App / ', 'sdf', '2013-05-21', '2010-08-25', 3, '2016-11-28 08:53:20', '2016-11-28 08:53:20'),
(3, 'Ahmed', 'App / ', 'asdasd', '2013-05-15', '2013-05-14', 12, '2016-11-28 12:29:49', '2016-11-28 12:29:49'),
(4, 'bew', 'Website / ', '123123123', '2013-05-08', '2010-05-12', 123, '2016-11-28 12:31:18', '2016-11-28 12:31:18'),
(5, 'hiiiii', 'Website / App / ', 'dsf', '2011-03-02', '2011-03-09', 3, '2016-11-28 13:09:53', '2016-11-28 13:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subproject`
--

CREATE TABLE `subproject` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subproject`
--

INSERT INTO `subproject` (`id`, `project_id`, `department_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 3, 1),
(6, 4, 1),
(7, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `regular_hours` int(11) DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `periority` int(11) NOT NULL,
  `mini_project_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `name`, `description`, `status`, `start_date`, `regular_hours`, `file_path`, `periority`, `mini_project_id`, `created_at`, `updated_at`) VALUES
(1, 'title', 'info', 0, '2016-11-22', 3, 'null', 0, 1, '2016-11-28 16:03:02', '2016-11-28 16:03:02'),
(2, 'title', 'info', 0, '2016-11-22', 3, 'null', 0, 1, '2016-11-28 16:04:25', '2016-11-28 16:04:25'),
(3, 'new task', 'infro', 0, '2016-11-23', 3, 'null', 0, 3, '2016-11-28 16:17:44', '2016-11-28 16:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `timesegment`
--

CREATE TABLE `timesegment` (
  `id` int(11) NOT NULL,
  `timesheet_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` double NOT NULL,
  `end_time` double NOT NULL,
  `total_hours` double NOT NULL,
  `deduction_hours` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timesheet`
--

CREATE TABLE `timesheet` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `number_of_hours` int(11) NOT NULL,
  `total_salary` double NOT NULL,
  `delivered` tinyint(1) NOT NULL,
  `accurate_num_of_hours` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userproject`
--

CREATE TABLE `userproject` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userproject`
--

INSERT INTO `userproject` (`id`, `project_id`, `user_id`) VALUES
(1, 1, 2),
(2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vacation` int(11) NOT NULL,
  `emergency` int(11) NOT NULL,
  `marriage` int(11) NOT NULL,
  `military_service` int(11) NOT NULL,
  `meternity` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `salary` double DEFAULT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `department_id`, `phone_number`, `email`, `first_name`, `last_name`, `password`, `vacation`, `emergency`, `marriage`, `military_service`, `meternity`, `title`, `gender`, `address`, `birthday`, `hire_date`, `about`, `salary`, `profile_picture`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 3, '011212', 'admin@ivas.com', 'Ahmed', 'Admin', '$2y$10$GTqCDRAhRL5N5qXSZHscC.DWooDWOo4X5HoIDO7YzJQ1kUhKhFSsy', 21, 21, 21, 21, 21, 'HR admin', 'male', 'address', '2016-11-09', '2016-11-10', 'about', 1212, 'uploads/user\\1480499051_cats-politics-TN.jpg', 'WP22oEewcQK64piA8JnfmXD0p9zDzV8htXWO39zYpitcxUmZmYFcwGtUvk9G', NULL, '2016-11-30 07:55:38', 'admin'),
(2, 1, '0102300123', 'manager@ivas.com', 'Ahmed', 'manager', '$2y$10$J/EAIJhhGhSO1gBv5PVOseuTC/YM4zy/qKxKnH.OT6UXo.Ix4IpCe', 21, 21, 21, 21, 21, 'Digital manager', 'male', 'address', '2016-11-16', '2016-11-16', 'about', 1231, 'uploads/user\\1480497097_Funny-Cat-GIFs.jpg', 'KFbZCPC5ePoyDKqceHZvzPxygUMfP8UKsfcY5L1P2d3L9Re3fi3Z2alr4NZF', NULL, '2016-11-30 07:43:30', 'manager'),
(3, 1, '011111111111', 'employee@ivas.com', 'employee', 'imp', '$2y$10$himCa4pxGwFiXGn3hksoh.XK9h18175yjxdDmx8/U7FLZqMWg7la6', 15, 6, 7, 14, 0, 'php developer', 'male', 'address', '2016-11-08', '2016-11-08', 'about', 123123, NULL, 'im36geuj3O1SUUkfyFKsjoSMssGXKjcpTvqp8YmSYdIg79aPLKXPKckg9IX1', '2016-11-30 07:55:32', '2016-11-30 11:12:56', 'employee');

-- --------------------------------------------------------

--
-- Table structure for table `usertask`
--

CREATE TABLE `usertask` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `accurate_hours` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usertask`
--

INSERT INTO `usertask` (`id`, `user_id`, `task_id`, `accurate_hours`) VALUES
(1, 2, 1, NULL),
(2, 2, 2, NULL),
(3, 2, 3, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_document__project_id` (`project_id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_leaves__timesheet_id` (`timesheet_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `miniproject`
--
ALTER TABLE `miniproject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_miniproject__departments_projects_id` (`departments_projects_id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_overtime__time_segment_id` (`time_segment_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_report__project_id` (`project_id`);

--
-- Indexes for table `subproject`
--
ALTER TABLE `subproject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_subproject__project_id` (`project_id`),
  ADD KEY `idx_subproject__department_id` (`department_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_task__mini_project_id` (`mini_project_id`);

--
-- Indexes for table `timesegment`
--
ALTER TABLE `timesegment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_timesegment__timesheet_id` (`timesheet_id`);

--
-- Indexes for table `timesheet`
--
ALTER TABLE `timesheet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_timesheet__user_id` (`user_id`);

--
-- Indexes for table `userproject`
--
ALTER TABLE `userproject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_userproject__project_id` (`project_id`),
  ADD KEY `idx_userproject__user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `idx_user__department_id` (`department_id`);

--
-- Indexes for table `usertask`
--
ALTER TABLE `usertask`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_usertask__user_id` (`user_id`),
  ADD KEY `idx_usertask__task_id` (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;
--
-- AUTO_INCREMENT for table `miniproject`
--
ALTER TABLE `miniproject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subproject`
--
ALTER TABLE `subproject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `timesegment`
--
ALTER TABLE `timesegment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `timesheet`
--
ALTER TABLE `timesheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userproject`
--
ALTER TABLE `userproject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usertask`
--
ALTER TABLE `usertask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `fk_document__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `leaves`
--
ALTER TABLE `leaves`
  ADD CONSTRAINT `fk_leaves__timesheet_id` FOREIGN KEY (`timesheet_id`) REFERENCES `timesheet` (`id`);

--
-- Constraints for table `miniproject`
--
ALTER TABLE `miniproject`
  ADD CONSTRAINT `miniproject_departments_projects_id_foreign` FOREIGN KEY (`departments_projects_id`) REFERENCES `subproject` (`id`);

--
-- Constraints for table `overtime`
--
ALTER TABLE `overtime`
  ADD CONSTRAINT `fk_overtime__time_segment_id` FOREIGN KEY (`time_segment_id`) REFERENCES `timesegment` (`id`);

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `fk_report__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `subproject`
--
ALTER TABLE `subproject`
  ADD CONSTRAINT `fk_subproject__department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `fk_subproject__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_mini_project_id_foreign` FOREIGN KEY (`mini_project_id`) REFERENCES `miniproject` (`id`);

--
-- Constraints for table `timesegment`
--
ALTER TABLE `timesegment`
  ADD CONSTRAINT `fk_timesegment__timesheet_id` FOREIGN KEY (`timesheet_id`) REFERENCES `timesheet` (`id`);

--
-- Constraints for table `timesheet`
--
ALTER TABLE `timesheet`
  ADD CONSTRAINT `fk_timesheet__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `userproject`
--
ALTER TABLE `userproject`
  ADD CONSTRAINT `fk_userproject__project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `fk_userproject__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_user__department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);

--
-- Constraints for table `usertask`
--
ALTER TABLE `usertask`
  ADD CONSTRAINT `fk_usertask__task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `fk_usertask__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
